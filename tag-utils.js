// ! When changing this, please make sure to reflect changes in backend/mainapp/static/mainapp/js/helper.js

export const USER_TAGS_CACHE_KEY = 'userTagsCache';
export const USER_LAST_TAG_MODIFIED_KEY = 'userLatestTagModified';

const setItemOnStorage = (itemKey, itemValue) => {
  try {
    window.localStorage.setItem(itemKey, JSON.stringify(itemValue));
  } catch (e) {}
};

const getItemFromStorage = (itemKey, defaultValue) => {
  let itemValue = defaultValue;
  try {
    const cachedItemValue = window.localStorage.getItem(itemKey);
    if (cachedItemValue) {
      itemValue = JSON.parse(cachedItemValue);
    }
  } catch (e) {
  } finally {
    return itemValue;
  }
};

const getTagsFromStorage = () => {
  return getItemFromStorage(USER_TAGS_CACHE_KEY, {});
};

const getLastTagModifiedFromStorage = () => {
  return getItemFromStorage(USER_LAST_TAG_MODIFIED_KEY, null);
};

const convertTagsListToHash = (tagsList) => {
  // Converts tags list that has original API response format into format for caching
  return tagsList.reduce((tagsHash, tagItem, index) => {
    return { ...tagsHash, [tagItem.value]: true };
  }, {});
};

export const convertTagsHashToList = (tagsHash) => {
  // Converts cached tags hash into format similar to original API response (for use with quill-mention)
  return Object.keys(tagsHash).map((tagName) => {
    return { value: tagName };
  });
};

const mergeStoredAndFetchedTags = (storedTags, fetchedTags) => {
  const fetchedTagsHash = convertTagsListToHash(fetchedTags);
  return { ...storedTags, ...fetchedTagsHash };
};

const updateCachedTagDetails = (updatedTags, latestModified) => {
  setItemOnStorage(USER_TAGS_CACHE_KEY, updatedTags);
  setItemOnStorage(USER_LAST_TAG_MODIFIED_KEY, latestModified);
};

const fetchUserTagsHash = () => {
  const storedTagsHash = getTagsFromStorage();
  const storedTagsLatestTime = getLastTagModifiedFromStorage();
  const requestParams = {
    headers: webHeaders,
  };
  return fetchUserTags(
    {
      url: '/api/v1/tag/',
      options: requestParams,
    },
    {
      storedTags: storedTagsHash,
      storedTagsLastModified: storedTagsLatestTime,
      updateCacheFunc: updateCachedTagDetails,
    },
  );
};

export const fetchUserTags = (requestDetails, cacheDetails) => {
  const { url, options } = requestDetails;
  const { storedTags, storedTagsLastModified, updateCacheFunc } = cacheDetails;
  let tagsApiUrl = url;
  if (storedTagsLastModified) {
    tagsApiUrl = `${tagsApiUrl}?since=${storedTagsLastModified}`;
  }
  return fetch(tagsApiUrl, options)
    .then((response) => response.json())
    .then((result) => {
      let { data, resource_version } = result;
      if (data.tags && data.resource_version) {
        resource_version = data.resource_version;
        data = data.tags;
      }
      if (data && data.length > 0) {
        // More recent tags are available, so merge with cached values
        const updatedTags = mergeStoredAndFetchedTags(storedTags, data);
        updateCacheFunc(updatedTags, resource_version);
        return updatedTags;
      } else {
        return storedTags;
      }
    });
};
