import { host, isDevMode, apiBase } from './globals.js';
import { helper } from './helper.js';

//--------- Variables ---------//
const settingsUrl = `${apiBase}/settings/`;

const hash = location.hash;

const options = document.querySelectorAll('[data-open-section]');
const sections = document.querySelectorAll('[data-detail-section]');

const settingsState = {};

let isError = false;
let isLoading = true;

//? Breakpoint for active option indicator is in css
const mobileBreakpoint = 1200;
let isMobile = window.innerWidth < mobileBreakpoint;
let isModalActivated = false;

//--------- Links in dev ---------//

if (isDevMode) {
  document.querySelectorAll('a[href^="https://histre.com/"]').forEach((link) => {
    const devLink = link.getAttribute('href').replace('https://histre.com', host);
    link.setAttribute('href', devLink);
  });
}

//--------- Navbar ---------//

document.getElementById('navbar-toggler').addEventListener('click', function () {
  if (this.getAttribute('aria-expanded') === 'false') {
    document.getElementById('navbar-collapse').classList.add('show');
    this.setAttribute('aria-expanded', true);
  } else {
    document.getElementById('navbar-collapse').classList.remove('show');
    this.setAttribute('aria-expanded', false);
  }
});

//--------- Main section ---------//

function setUpSettings() {
  fetch(settingsUrl, helper.setRequestParams())
    .then((response) => {
      return response.json();
    })
    .then((result) => {
      if (result.error) {
        if (helper.isAuthErrorStatus(result.errcode)) {
          showError('auth-error');
        } else {
          showError('server-error');
        }
      } else {
        if (typeof result.settings?.history !== 'undefined') {
          chrome.storage.local.set({ userLogHistory: result.settings.history }, () => {
            showMainContent();
            loadSettings();
          });
        } else {
          showError('server-error');
        }
      }
    })
    .catch(() => {
      showError('server-error');
    });
}

function finishLoading() {
  isLoading = false;
  document.getElementById('loading-spinner').remove('d-none');
}

function showError(type) {
  finishLoading();
  isError = true;

  switch (type) {
    case 'auth-error':
      document.getElementById('auth-error').classList.remove('d-none');
      break;
    case 'server-error':
      document.getElementById('server-error').classList.remove('d-none');
      break;
  }
}

function showMainContent() {
  finishLoading();

  document.getElementById('main-section').classList.remove('d-none');
  document.getElementById('details').classList.remove('d-none');

  if (isMobile) {
    document.getElementById('title-info').classList.remove('d-none');
  }
}

document.getElementById('reload-btn').addEventListener('click', () => {
  location.reload();
});

function loadSettings() {
  chrome.storage.local.get(
    [
      'userLogHistory',
      'featuresHistory',
      'featuresHighlights',
      'featuresEnhanceWebsites',
      'featuresInline',
      'inlineIsShown',
      'cmdPaletteIsShown',
    ],
    (result) => {
      let logBrowser = result.featuresHistory;
      if (typeof logBrowser === 'undefined') {
        logBrowser = true;
      }

      settingsState.enhanceWebsites = !!result.featuresEnhanceWebsites;
      settingsState.enableHighlights = !!result.featuresHighlights;
      settingsState.logAccount = !!result.userLogHistory;
      settingsState.logBrowser = !!logBrowser;
      settingsState.featuresInline = !!result.featuresInline;
      settingsState.enableInline = !!result.inlineIsShown;
      settingsState.enableCmdPalette = !!result.cmdPaletteIsShown;
      renderSettings();
    },
  );
}

function renderSettings() {
  document.getElementById('enhance-web-sites-switch').checked = settingsState.enhanceWebsites;
  document.getElementById('highlights-switch').checked = settingsState.enableHighlights;
  document.getElementById('history-switch').checked = settingsState.logBrowser;
  document.getElementById('inline-switch').checked = settingsState.enableInline;
  document.getElementById('cmd-palette-switch').checked = settingsState.enableCmdPalette;

  if (!settingsState.logAccount && settingsState.logBrowser) {
    document.getElementById('history-error-message').classList.remove('d-none');
  }
}

function updateStorage(key, val, cb) {
  if (!cb) {
    cb = loadSettings;
  }
  const kv = {};
  kv[key] = val;
  chrome.storage.local.set(kv, cb);
}

function updateEnhanceWebsites(enable) {
  if (enable) {
    chrome.permissions.request(
      {
        origins: ['*://*/*'],
        permissions: ['bookmarks', 'history'],
      },
      function (enabled) {
        if (enabled) {
          updateStorage('featuresEnhanceWebsites', true);

          chrome.storage.local.get(['enablePermissions'], (result) => {
            if (!result.enablePermissions) {
              chrome.storage.local.set({ enablePermissions: true });
            }
          });
        } else {
          loadSettings();
        }
      },
    );
  } else {
    updateStorage('featuresEnhanceWebsites', false);
  }
}

function updateHighlights(enable) {
  if (enable) {
    chrome.permissions.request(
      {
        origins: ['*://*/*'],
      },
      (enabled) => {
        if (enabled) {
          updateStorage('featuresHighlights', true);
        } else {
          loadSettings();
        }
      },
    );
  } else {
    updateStorage('featuresHighlights', false);
  }
}

function updateInline(enable) {
  const updatedValues = {};

  if (enable) {
    chrome.permissions.request(
      {
        origins: ['*://*/*'],
      },
      (enabled) => {
        if (enabled) {
          if (settingsState.enableInline === false) {
            updatedValues.featuresInline = true;
            updatedValues.cmdPaletteIsShown = true;
          }
          updatedValues.inlineIsShown = true;
          chrome.storage.local.set(updatedValues, loadSettings);
        } else {
          loadSettings();
        }
      },
    );
  } else {
    if (settingsState.enableCmdPalette === false) {
      updatedValues.featuresInline = false;
    }
    updatedValues.inlineIsShown = false;
    chrome.storage.local.set(updatedValues, loadSettings);
  }
}

function updateCmdPalette(enable) {
  const updatedValues = {};

  if (enable) {
    chrome.permissions.request(
      {
        origins: ['*://*/*'],
      },
      (enabled) => {
        if (enabled) {
          if (settingsState.enableInline === false) {
            updatedValues.featuresInline = true;
          }
          updatedValues.cmdPaletteIsShown = true;
          chrome.storage.local.set(updatedValues, loadSettings);
        } else {
          loadSettings();
        }
      },
    );
  } else {
    if (settingsState.enableInline === false) {
      updatedValues.featuresInline = false;
    }
    updatedValues.cmdPaletteIsShown = false;
    chrome.storage.local.set(updatedValues, loadSettings);
  }
}

document.getElementById('enhance-web-sites-switch').addEventListener('click', function () {
  updateEnhanceWebsites(this.checked);
});

document.getElementById('highlights-switch').addEventListener('click', function () {
  updateHighlights(this.checked);
});

document.getElementById('history-switch').addEventListener('click', function () {
  updateStorage('featuresHistory', this.checked);
});

document.getElementById('inline-switch').addEventListener('click', function () {
  updateInline(this.checked);
});

document.getElementById('cmd-palette-switch').addEventListener('click', function () {
  updateCmdPalette(this.checked);
});

function showHistoryLoadingMessage() {
  document.getElementById('history-error-message').classList.add('d-none');
  document.getElementById('history-failed-message').classList.add('d-none');
  document.getElementById('history-loading-message').classList.remove('d-none');
}

function showHistoryFailedMessage() {
  document.getElementById('history-loading-message').classList.add('d-none');
  document.getElementById('history-failed-message').classList.remove('d-none');
}

function showHistorySuccessMessage() {
  document.getElementById('history-loading-message').classList.add('d-none');
  document.getElementById('history-success-message').classList.remove('d-none');
}

document.querySelectorAll('[data-enable-account-history]').forEach((button) => {
  button.addEventListener('click', () => {
    const data = { history: true };
    showHistoryLoadingMessage();

    fetch(settingsUrl, helper.setRequestParams('POST', data))
      .then((response) => {
        return response.json();
      })
      .then((result) => {
        const {
          settings: { history },
          error,
          errcode,
        } = result;

        if (error) {
          if (helper.isAuthErrorStatus(errcode)) {
            helper.handleAuthErrorsOnce(() => {
              showHistoryFailedMessage();
              return;
            });
          } else {
            showHistoryFailedMessage();
          }
        } else {
          updateStorage('userLogHistory', history);

          showHistorySuccessMessage();
        }
      })
      .catch(() => {
        showHistoryFailedMessage();
      });
  });
});

//--------- Detail section ---------//

options.forEach((option) => {
  option.addEventListener('click', function (e) {
    sections.forEach((section) => {
      if (section.dataset.detailSection === this.dataset.openSection) {
        section.classList.remove('d-none');
      } else {
        section.classList.add('d-none');
      }
    });

    options.forEach((option) => {
      if (option.dataset.openSection === this.dataset.openSection) {
        option.classList.add('active');
      } else {
        option.classList.remove('active');
      }
    });

    if (hash) {
      location.hash = '#' + this.dataset.openSection;
    }

    if (isMobile) {
      MicroModal.show('detail-modal');
      document.getElementById('detail-modal-content').scrollTop = 0;
    }
  });
});

function activateModal() {
  const details = document.getElementById('details');

  document.getElementById('detail-modal-content').appendChild(details);

  details.classList.remove('card');
  details.firstElementChild.classList.remove('card-body');

  document.querySelectorAll('[data-micromodal-close]').forEach((button) => {
    button.parentElement.classList.remove('d-none');
  });

  if (!isError && !isLoading) {
    document.getElementById('title-info').classList.remove('d-none');
  }

  if (!isModalActivated) {
    MicroModal.init({ awaitCloseAnimation: true });
    isModalActivated = true;
  }
}

function deactivateModal() {
  const details = document.getElementById('details');

  document.getElementById('main').appendChild(details);

  details.classList.add('card');
  details.firstElementChild.classList.add('card-body');

  document.querySelectorAll('[data-micromodal-close]').forEach((button) => {
    button.parentElement.classList.add('d-none');
  });

  document.getElementById('title-info').classList.add('d-none');

  if (document.getElementById('detail-modal').classList.contains('is-open')) {
    MicroModal.close('detail-modal');
  }
}

window.addEventListener('resize', () => {
  const mobile = window.innerWidth < mobileBreakpoint;
  if (isMobile === mobile) {
    return;
  }

  isMobile = mobile;
  if (mobile) {
    activateModal();
  } else {
    deactivateModal();
  }
});

//--------- Deep link handling ---------//

if (hash) {
  document.querySelector(`[data-open-section="${hash.substring(1)}"]`)?.click();
}

//--------- Function calls ---------//

setUpSettings();

if (isMobile) {
  activateModal();
}
