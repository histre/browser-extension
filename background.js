import { isDevMode, apiBase } from '/globals.js';
import { helper } from '/helper.js';
import { db } from '/background-globals.js';
import '/background-highlight.js';
import {
  runBookmarkImport,
  bookmarksOnCreatedHandler,
  bookmarksOnMovedHandler,
  bookmarksOnDeletedHandler,
} from '/background-bookmarks.js';
import '/background-inline.js';
import '/background-hnvotes.js';
import '/background-misc.js';
import { runStorageMigrations } from '/migrations.js';

const apiUrl = `${apiBase}/visit/`;
const historyUrl = `${apiBase}/history.handle.json`;
const settingsUrl = `${apiBase}/settings/`;
const pageUrl = `${apiBase}/page/`;

const POST_PERIOD = 1000 * (isDevMode ? 5 : 30);
const payloadBatchSize = 500;

// Function of isPosting is to prevent db changes while one request is being made,
// mainly to prevent submitting same history items twice
let isPosting = false;

// History Threads can be a global variable, because service worker won't be closed until submitting is processed
let historyThreads = 0;

function getLocalStorage(keys = null) {
  return new Promise((resolve) => {
    chrome.storage.local.get(keys, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}

function setLocalStorage(keys = null) {
  return new Promise((resolve) => {
    chrome.storage.local.set(keys, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}

// Data is processed in helper get function
function fetchSettings() {
  helper.doGet(settingsUrl, {});
}

async function isHistoryEnabled() {
  const { userLogHistory, featuresHistory } = await chrome.storage.local.get([
    'userLogHistory',
    'featuresHistory',
  ]);

  return userLogHistory && featuresHistory;
}

function firstTab(tabs, caller) {
  if (
    typeof tabs === 'undefined' ||
    typeof tabs.length === 'undefined' ||
    typeof tabs[0] === 'undefined'
  ) {
    helper.llog(`${caller} : tab is not available`);
    return null;
  }
  return tabs[0];
}

function cleanFavicon(favicon) {
  if (favicon && (favicon.indexOf('http') !== 0 || favicon.length > 255)) {
    return null;
  }
  return favicon;
}

async function resetStorage() {
  persist({ forReset: true });
  (await db).clear('navfrom');
  (await db).clear('note');
  chrome.storage.local.set({ historyLastActiveTabId: -1 });
}

// ---- Handlers ----

function onInstalledHandler(details) {
  if (details.reason === 'install') {
    chrome.tabs.create({
      url: 'onboarding.html',
    });
  }

  if (details.reason === 'update') {
    runBookmarkImport();

    runStorageMigrations(details.previousVersion, refreshFromStorage);
  }

  resetStorage();
}

function onStartupHandler() {
  resetStorage();
}

async function tabUpdatedHandler(tabId, changes, tab) {
  if (!(await isHistoryEnabled())) return;

  if (changes.url) {
    //url of the tab changing for some reason
    if (tab.active) {
      //start new click if its active tab and we are not already tracking it
      const tx = (await db).transaction('history', 'readonly');
      const store = tx.store;
      const keys = await store.getAllKeys();
      if (keys.length > 0) {
        const lastItemKey = keys[keys.length - 1];
        const lastItem = await store.get(lastItemKey);
        if (lastItem.tabId === tabId && lastItem.url === changes.url) {
          return;
        }
      }
      await tx.done;

      // ? Maybe await this?
      startClock();
    }
  }
  if (changes.status != 'complete') {
    //do updates only if page already loaded
    return;
  }

  const tx = (await db).transaction(['history', 'navfrom'], 'readwrite');
  const historyStore = tx.objectStore('history');
  const keys = await historyStore.getAllKeys();
  for (let i = keys.length - 1; i >= 0; i--) {
    const item = await historyStore.get(keys[i]);
    if (item.tabId === tab.id) {
      if (tab.favIconUrl) {
        item.favicon = cleanFavicon(tab.favIconUrl);
      }
      if (tab.title) {
        item.title = tab.title;
      }
      item.navfrom = (await tx.objectStore('navfrom').get(tab.id))?.navfrom;
      await historyStore.put(item);
      break;
    }
  }
  await tx.done;

  const { historyLastPost } = await getLocalStorage(['historyLastPost']);
  if (typeof historyLastPost === 'undefined' || Date.now() - historyLastPost > POST_PERIOD) {
    // post only after a tab completes loading
    await setLocalStorage({ historyLastPost: Date.now() });
    persist();
  }
}

async function tabCreatedHandler(tab) {
  if (!(await isHistoryEnabled())) return;

  //left click that opens new tab, Ctrl/CMD-T
  if (tab.openerTabId) {
    //trying to get refefrrer for new tab.
    chrome.tabs.get(tab.openerTabId, async function (oldtab) {
      if (oldtab) {
        if (oldtab.url.length > helper.MAX_URL_LEN) {
          const url = helper.reduceURLSize(tab.url);
          if (!url) {
            return;
          }
          oldtab.url = url;
        }
        (await db).put('navfrom', { tabId: tab.id, navfrom: oldtab.url });
      }
    });
  }
}

async function tabRemovedHandler(tabId) {
  if (!(await isHistoryEnabled())) return;

  (await db).delete('navfrom', tabId);
}

async function onActivatedHandler(activeInfo) {
  if (!(await isHistoryEnabled())) return;

  chrome.storage.local.set({ historyLastActiveTabId: activeInfo.tabId });
  helper.llog('tab activated');
  startClock();
}

async function focusChangedHandler(window) {
  if (!(await isHistoryEnabled())) return;

  if (window == chrome.windows.WINDOW_ID_NONE) {
    helper.llog('lost focus');
    stopClock();
  } else {
    helper.llog('got focus');
    startClock();
  }
}

async function onCommittedHandler(e) {
  if (!(await isHistoryEnabled())) return;

  //checking if navigation happened in addressbar
  for (var i = 0; i < e.transitionQualifiers.length; i++) {
    if (e.transitionQualifiers[i] == 'from_address_bar') {
      //user navigated from addressbar, clear navfrom for this tab. it's 100% active tab
      const tx = (await db).transaction(['history', 'navfrom'], 'readwrite');
      await tx.objectStore('navfrom').delete(e.tabId);
      const historyStore = tx.objectStore('history');
      const keys = historyStore.getAllKeys();
      for (let i = keys.length; i >= 0; i--) {
        //also clear it in last clock record, it's already there. should be last one if we did not switched tab fast.
        const item = await historyStore.get(keys[i]);
        if (item.tabId === e.tabId && item.url === e.url) {
          item.navfrom = null;
          await historyStore.put(item);
          break;
        }
      }
      await tx.done;
    }
  }
}

async function onBeforeNavigateHandler(e) {
  if (!(await isHistoryEnabled())) return;

  const lastTabId = (await chrome.storage.local.get('historyLastActiveTabId'))
    ?.historyLastActiveTabId;

  if (typeof lastTabId === 'undefined') return;

  //new url, trying to get url of last open tab - it's current tab or tab that launched navigation in new tab
  if (e.frameId != 0 || lastTabId == -1) return;
  chrome.tabs.get(lastTabId, async (tab) => {
    if (tab) {
      if (tab.url.length > helper.MAX_URL_LEN) {
        const url = helper.reduceURLSize(tab.url);
        if (!url) {
          return;
        }
        tab.url = url;
      }
      (await db).put('navfrom', { tabId: e.tabId, navfrom: tab.url });
    }
  });
}

function startClock() {
  chrome.tabs.query({ currentWindow: true, active: true }, async (tabs) => {
    const tx = (await db).transaction(['history', 'navfrom'], 'readwrite');
    await stopClock(tx);

    const tab = firstTab(tabs, 'startClock');
    if (tab === null || !tab.url.startsWith('http')) {
      await tx.done;
      return;
    }

    if (tab.url.length > helper.MAX_URL_LEN) {
      const url = helper.reduceURLSize(tab.url);
      if (!url) {
        return;
      }
      tab.url = url;
    }

    const navfrom = (await tx.objectStore('navfrom').get(tab.id))?.navfrom;
    await tx.objectStore('history').add({
      url: tab.url,
      tabId: tab.id,
      start: Date.now(),
      end: null,
      title: tab.title,
      navfrom: navfrom,
      favicon: cleanFavicon(tab.favIconUrl),
    });
    await tx.done;

    helper.llog(`startClock : ${tab.url}`);

    if (chrome.runtime.lastError) {
      helper.llog(`startClock Error : ${chrome.runtime.lastError}`);
    }
  });
}

async function stopClock(transaction) {
  const tx = transaction ?? (await db).transaction('history', 'readwrite');
  const store = tx.objectStore('history');
  const keys = await store.getAllKeys();

  if (keys.length > 0) {
    const lastItemKey = keys[keys.length - 1];
    const lastItem = await store.get(lastItemKey);
    if (!lastItem.end) {
      lastItem.end = Date.now();
      await store.put(lastItem);
      helper.llog(`stopClock : ${lastItem.url}, time: ${(lastItem.end - lastItem.start) / 1000}s`);
    } else {
      helper.llog('cant stop clock coz none running');
    }
  } else {
    helper.llog('cant stop clock coz none running');
  }
  if (!transaction) {
    await tx.done;
  }
}

async function persist(options = {}) {
  const enable = options.forReset || (await isHistoryEnabled()) || options.fromRecursion;
  if (!enable) {
    helper.llog('in persist, but history not enabled');
    return;
  }

  if (isPosting) {
    helper.llog('in persist, but history is posting');
    return;
  }
  isPosting = true;

  try {
    const tx = (await db).transaction('history', 'readwrite');
    const store = tx.store;
    let keys = await store.getAllKeys();
    let isSplit = false;

    if (keys.length <= 0) {
      helper.llog('nothing to persist');
      isPosting = false;
      return;
    }
    if (!(await store.get(keys[keys.length - 1])).end) {
      // If the end is not set when resetting, it means that the browser was closed before it was set,
      // so we remove it from db to prevent sending incorrect active time
      if (options.forReset) {
        await store.delete(keys[keys.length - 1]);
      }
      keys.pop();
    }
    if (keys.length <= 0) {
      helper.llog('nothing to persist');
      isPosting = false;
      return;
    }

    if (keys.length > payloadBatchSize) {
      keys = keys.slice(0, payloadBatchSize);
      isSplit = true;
    }

    const payload = [];

    for (let i = 0; i < keys.length; i++) {
      const item = await store.get(keys[i]);
      delete item.id;
      if (
        item.url === 'about:blank' ||
        item.url.indexOf('https://www.google.com/_/chrome/newtab') > -1 ||
        item.url.indexOf('https://notifications.google.com') > -1
      ) {
        continue;
      }

      if (item.url.length > helper.MAX_URL_LEN) {
        const url = helper.reduceURLSize(item.url);
        if (!url) {
          continue;
        }
        item.url = url;
      }
      if (item.navfrom?.length > helper.MAX_URL_LEN) {
        const url = helper.reduceURLSize(item.navfrom);
        if (!url) {
          delete item.navfrom;
        } else {
          item.navfrom = url;
        }
      }

      if (isDevMode && !item.end) {
        const allHistory = await store.getAll();
        if (!allHistory[allHistory.length - 1].end) {
          allHistory.pop();
        }
        console.error(
          'History log time end not set ocurred in payload : ',
          JSON.stringify(allHistory),
        );
      }

      payload.push(item);
    }
    await tx.done;

    helper.llog('persist:', payload);
    helper.doPost(
      apiUrl,
      payload,
      async (json) => {
        if (json.error === false) {
          helper.llog('persisted ok');
          const tx = (await db).transaction('history', 'readwrite');
          const store = tx.store;
          for (let key of keys) {
            await store.delete(key);
          }
          await tx.done;
          helper.unsetIconWarning();
          isPosting = false;

          if (isSplit) {
            persist({ fromRecursion: true });
          }
        } else {
          const { status } = json;
          if (status === helper.HttpCodes.unauthorized || status === helper.HttpCodes.forbidden) {
            helper.llog('not authenticated.');
            helper.setIconWarning();
          }
          isPosting = false;
        }
      },
      (e) => {
        helper.llog('history persisting failed :', e);
        isPosting = false;
      },
    );
  } catch (e) {
    helper.llog('history persisting failed :', e);
    isPosting = false;
  }
}

function postHistory() {
  chrome.history.search({ text: '' }, function callback(history) {
    let data = [];
    for (var i = 0; i < history.length; ++i) {
      let url = history[i].url;
      let title = history[i].title;
      let lastVisitTime = history[i].lastVisitTime;
      var processVisits = function (url, title, lastVisitTime) {
        return function (visitItems) {
          for (var j = 0; j < visitItems.length; j++) {
            data.push({
              id: visitItems[j].id,
              url: url,
              title: title,
              datetime: lastVisitTime,
              referrerId: visitItems[j].referringVisitId,
            });
          }
          historyThreads--;
          if (historyThreads == 0) {
            helper.llog(data);
            helper.doPost(historyUrl, data, function (result) {
              helper.llog('History data sent' + result);
            });
          }
        };
      };
      historyThreads++;
      chrome.history.getVisits({ url: url }, processVisits(url, title, lastVisitTime));
    }
  });
}

function addBookmarksListeners() {
  chrome.permissions.contains(
    {
      permissions: ['bookmarks'],
    },
    function (result) {
      if (result) {
        chrome.bookmarks.onCreated.addListener(bookmarksOnCreatedHandler);
        chrome.bookmarks.onMoved.addListener(bookmarksOnMovedHandler);
        chrome.bookmarks.onRemoved.addListener(bookmarksOnDeletedHandler);
      } else {
        // we don't have the permission to do that
      }
    },
  );
}

function onMessageHandler(request, sender, sendResponse) {
  try {
    if (sender.tab) {
      if (request.type == 'open_tabs') {
        chrome.windows.create({ url: request.urls }, (w) => {
          helper.llog(w);
          sendResponse(true);
        });
      }
    }

    if (request.action === 'sendPageSource') {
      helper.doPost(pageUrl, request.payload, (result) => {
        if (result.error !== true) {
          helper.llog('Successfully sent page source of', request.payload.url);
        } else {
          helper.llog('Error occurred while trying to send page source of', request.payload.url);
        }
      });
    }
  } catch (e) {
    helper.llog(e);
  }
}

// listen to changes from popup
chrome.storage.onChanged.addListener((changes) => {
  if (changes.enablePermissions) {
    if (changes.enablePermissions.newValue) {
      runBookmarkImport();
      addBookmarksListeners();

      chrome.storage.local.get(['postedHistory'], (result) => {
        if (!result.postedHistory) {
          chrome.permissions.contains({ permissions: ['history'] }, (enabled) => {
            if (enabled) {
              postHistory();
              chrome.storage.local.set({ postedHistory: true });
            } else {
              // we don't have the permission to do that
            }
          });
        }
      });
    }
  }

  if (changes.userLogHistory || changes.featuresHistory) {
    chrome.storage.local.get(['userLogHistory', 'featuresHistory'], (result) => {
      // If user hasn't set this, default to true
      if (typeof result.userLogHistory === 'undefined') {
        chrome.storage.local.set({ userLogHistory: true });
      }
      if (typeof result.featuresHistory === 'undefined') {
        chrome.storage.local.set({ featuresHistory: true });
      }
    });
  }
});

function refreshFromStorage() {
  chrome.storage.local.get(
    ['userLogHistory', 'featuresHistory', 'enablePermissions', 'postedHistory'],
    async (result) => {
      // If user hasn't set this, default to true
      if (typeof result.userLogHistory === 'undefined') {
        await chrome.storage.local.set({ userLogHistory: true });
      }
      if (typeof result.featuresHistory === 'undefined') {
        await chrome.storage.local.set({ featuresHistory: true });
      }

      if (result.enablePermissions && !result.postedHistory) {
        chrome.permissions.contains(
          {
            permissions: ['history'],
          },
          function (result) {
            if (result) {
              postHistory();
              chrome.storage.local.set({ postedHistory: true }, () => {});
            } else {
              // we don't have the permission to do that
            }
          },
        );
      }
    },
  );
}

function initialize() {
  fetchSettings();
  addBookmarksListeners();
  refreshFromStorage();
}
initialize();

chrome.tabs.onUpdated.addListener(tabUpdatedHandler);
chrome.tabs.onActivated.addListener(onActivatedHandler);
chrome.tabs.onCreated.addListener(tabCreatedHandler);
chrome.tabs.onRemoved.addListener(tabRemovedHandler);
chrome.windows.onFocusChanged.addListener(focusChangedHandler);
chrome.webNavigation.onCommitted.addListener(onCommittedHandler);
chrome.webNavigation.onBeforeNavigate.addListener(onBeforeNavigateHandler);
chrome.runtime.onInstalled.addListener(onInstalledHandler);
chrome.runtime.onStartup.addListener(onStartupHandler);
chrome.runtime.onMessage.addListener(onMessageHandler);
