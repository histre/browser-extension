import { apiBase } from '/globals.js';
import { helper } from '/helper.js';

const hnvotesUrl = `${apiBase}/hnvotes/`;

function addHNContentScript(tab) {
  chrome.permissions.contains(
    {
      origins: ['https://news.ycombinator.com/'],
    },
    function (hasPermission) {
      if (hasPermission) {
        chrome.scripting.executeScript(
          {
            files: ['hnvotes.js'],
            target: { tabId: tab.id },
          },
          function () {
            const requestUrl = `${hnvotesUrl}?url=${encodeURIComponent(tab.url)}`;
            helper.doGet(requestUrl, {}, (data) => {
              chrome.tabs.sendMessage(
                tab.id,
                {
                  action: 'show_upvoted',
                  data: data.data,
                },
                (response) => {
                  if (chrome.runtime.lastError) {
                    // no op
                  } else {
                    // no op
                  }
                },
              );
            });
          },
        );
        if (chrome.runtime.lastError) {
        }
      } else {
        // we don't have the permission to do that
      }
    },
  );
}

function tabUpdatedHandler(tabId, changes, tab) {
  if (tab && tab.url && tab.url.indexOf('http') === 0 && changes.status === 'complete') {
    if (tab.url.indexOf('news.ycombinator.com') != -1) {
      addHNContentScript(tab);
    }
  }
}

chrome.tabs.onUpdated.addListener(tabUpdatedHandler);
