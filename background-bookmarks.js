import { apiBase, isFirefox } from '/globals.js';
import { helper } from '/helper.js';

const bookmarksUrl = `${apiBase}/bookmarks/`;

let builtInBookmarksFolderNames = ['bookmarks bar', 'other bookmarks'];
if (isFirefox) {
  builtInBookmarksFolderNames = [
    'bookmarks menu',
    'other bookmarks',
    'bookmarks toolbar',
    'mozilla firefox',
    'recent tags',
  ];
}

function validateBookmarkFolderName(folderName) {
  if (!folderName || folderName.length === 0) {
    // folder name is empty
    return false;
  }
  // Check if folderName is a built-in foldern
  const fldrName = folderName.toLowerCase();
  return !builtInBookmarksFolderNames.includes(fldrName);
}

function handleUnexpectedBookmarkImportErrors(errorObj) {
  // Run when the error causes bulk process to stop,
  // ensures that 'error' state is set
  chrome.storage.local.set({ postedBookmarksV2: 'error' }, () => {
    helper.llog(`Unexpected error occurred during bulk bookmark save: ${errorObj.message}`);
  });
}

function processBookmarkNode(bookmarkNode, parentFolders) {
  if (!parentFolders) {
    parentFolders = [];
  }
  const data = [];
  if (bookmarkNode.url) {
    // Include only notes that are bookmarks not folders
    const bookmarkItem = {
      title: bookmarkNode.title,
      url: bookmarkNode.url,
      datetime: bookmarkNode.dateAdded,
    };
    if (parentFolders.length > 0) {
      bookmarkItem['folders'] = parentFolders;
    }
    data.push(bookmarkItem);
  }
  if (bookmarkNode.children) {
    const parents = parentFolders.map((folderName) => folderName);
    if (validateBookmarkFolderName(bookmarkNode.title)) {
      parents.push(bookmarkNode.title);
    }
    for (let i = 0; i < bookmarkNode.children.length; i++) {
      const newitems = processBookmarkNode(bookmarkNode.children[i], parents);
      if (newitems && newitems.length) {
        data.push(...newitems);
      }
    }
  }
  return data;
}

function doPostBookmarksData(data) {
  const payload = Array.isArray(data) ? { data } : { data: [data] };
  helper.doPost(bookmarksUrl, payload, function (result) {
    helper.llog('bookmarks create sent. result:', result);
    const bookmarkUrls = payload.data.map((bookmark) => helper.removeURLFragment(bookmark.url));
    chrome.tabs.query({ url: bookmarkUrls }, (tabs) => {
      if (tabs.length > 0) {
        chrome.storage.local.get('featuresInline', (result) => {
          tabs.forEach((tab) => {
            if (result.featuresInline) {
              chrome.tabs.sendMessage(tab.id, { action: 'setNoteIsPresent', url: tab.url });
            }
            helper.setNoteIsPresentIcon(tab.id, tab.url);
          });
        });
      }
    });
  });
}

function doPostBulkBookmarksData(bookmarksData, errorCount) {
  errorCount = errorCount || 0;
  if (!bookmarksData.length) {
    const postedBookmarksStatus = errorCount == 0 ? 'finished' : 'error';
    chrome.storage.local.set({ postedBookmarksV2: postedBookmarksStatus }, () => {
      const msg =
        postedBookmarksStatus === 'finished'
          ? 'Successfully saved all bookmarks.'
          : 'Some errors encountered when saving bookmarks';
      helper.llog(msg);
    });
    return;
  }
  const batchSize = 500;
  const payload = { data: bookmarksData.splice(0, batchSize) };
  const bookmarksBulkImportUrl = `${bookmarksUrl}?bulk=true`;
  helper.doPost(
    bookmarksBulkImportUrl,
    payload,
    function (result) {
      const { error, status } = result;
      if (error) {
        helper.llog(`Error on bulk bookmark save: ${status}`);
        errorCount += 1;
      } else {
        helper.llog(`Saved current batch of ${payload.data.length} bookmarks`);
      }
      doPostBulkBookmarksData(bookmarksData, errorCount);
    },
    handleUnexpectedBookmarkImportErrors,
  );
}

function postBookmarks() {
  chrome.bookmarks.getTree(function (bookmarkTreeNodes) {
    let data = [];
    for (let i = 0; i < bookmarkTreeNodes.length; i++) {
      const newitems = processBookmarkNode(bookmarkTreeNodes[i]);
      if (newitems && newitems.length) {
        data.push(...newitems);
      }
    }
    helper.llog(data);
    doPostBulkBookmarksData(data);
  });
}

function postBookmarkItemWithFolderInfo(bookmarkItemData, parentId) {
  chrome.bookmarks.get(parentId, function (results) {
    if (results.length) {
      const parent = results[0];
      bookmarkItemData.folders = bookmarkItemData.folders || [];
      if (validateBookmarkFolderName(parent.title)) {
        bookmarkItemData.folders.push(parent.title);
      }

      if (parent.parentId) {
        postBookmarkItemWithFolderInfo(bookmarkItemData, parent.parentId);
      } else {
        doPostBookmarksData(bookmarkItemData);
      }
    }
  });
}

function getBookmark(id) {
  return new Promise((resolve) => {
    chrome.bookmarks.get(id, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}

async function getBookmarkUrl(id) {
  const results = await getBookmark(id);

  if (results.length) {
    return results[0].url;
  } else {
    return null;
  }
}

async function getBookmarkFolders(bookmarkParentId) {
  const folders = [];
  let parentId = bookmarkParentId;

  while (parentId) {
    const results = await getBookmark(parentId);
    if (results.length) {
      const parent = results[0];
      if (validateBookmarkFolderName(parent.title)) {
        folders.push(parent.title);
      }

      parentId = parent.parentId;
    } else {
      parentId = null;
    }
  }

  return folders;
}

export function bookmarksOnCreatedHandler(id, bookmark) {
  helper.llog('in bookmarksOnCreatedHandler:', id, bookmark);

  if (!bookmark.url) {
    // object is a folder
    return;
  }

  const data = {
    url: bookmark.url,
    title: bookmark.title,
    datetime: bookmark.dateAdded,
  };

  if (bookmark.parentId) {
    // Handle case when created bookmark is added to a folder
    postBookmarkItemWithFolderInfo(data, bookmark.parentId);
  } else {
    doPostBookmarksData(data);
  }
}

export async function bookmarksOnMovedHandler(id, moveInfo) {
  helper.llog('in bookmarksOnMovedHandler:', id, moveInfo);

  const bookmarkUrl = await getBookmarkUrl(id);
  if (!bookmarkUrl) {
    // object is a folder
    return;
  }
  const oldFolders = await getBookmarkFolders(moveInfo.oldParentId);
  const newFolders = await getBookmarkFolders(moveInfo.parentId);

  const payload = {
    url: bookmarkUrl,
    old_folders: oldFolders,
    new_folders: newFolders,
  };

  helper.doPatch(bookmarksUrl, payload, function (result) {
    helper.llog('bookmarks update sent. result:', result);
  });
}

export async function bookmarksOnDeletedHandler(id, bookmark) {
  helper.llog('in bookmarksOnDeletedHandler:', id, bookmark);

  if (!bookmark.url) {
    // object is a folder
    return;
  }

  const payload = {
    url: removeInfo.node.url,
  };

  helper.doDelete(bookmarksUrl, payload, function (result) {
    helper.llog('bookmarks delete sent. result:', result);
  });
}

function importAllBookmarks() {
  chrome.permissions.contains(
    {
      permissions: ['bookmarks'],
    },
    function (result) {
      if (result) {
        chrome.storage.local.set({ postedBookmarksV2: 'started' }, () => {
          postBookmarks();
        });
      } else {
        // we don't have the permission to do that
      }
    },
  );
}

export function runBookmarkImport() {
  chrome.storage.local.get(['enablePermissions', 'postedBookmarksV2'], (result) => {
    const { enablePermissions, postedBookmarksV2 } = result;
    if (enablePermissions && (!postedBookmarksV2 || postedBookmarksV2 === 'error')) {
      importAllBookmarks();
    }
  });
}
