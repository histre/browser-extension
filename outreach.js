(() => {
  const histreDOMHelper = window.histreDOMHelper;
  const { $e, $t, noEvents } = histreDOMHelper;
  const prefix = (classNames) => histreDOMHelper.prefix('outreach', classNames);
  const host = window.histreDOMHelper.getHost();

  let templates;
  let defaultTemplateIndex = 0;

  let cachedFullName;
  let cachedUrl;

  const $css = $e('link', {
    rel: 'stylesheet',
    type: 'text/css',
    href: chrome.runtime.getURL('outreach.css'),
  });

  //----- Config -----//
  // Properties that are different from site to site are meant to be stored (and overwritten)
  // in this config object, rather than throughout the code, to improve its readability.

  // Default values: LinkedIn
  const config = {
    queries: {
      editor: ['[contenteditable="true"][role="textbox"]', 'textarea.ember-text-area'],
      editorWrap: ['.msg-form__msg-content-container--scrollable', '.artdeco-modal__content'],
      msgWrap: '.msg-convo-wrapper',
      name: [
        '.profile-card-one-to-one__profile-link',
        '.msg-compose__profile-link',
        '.msg-connections-typeahead .artdeco-pill .artdeco-pill__text',
      ],
      profileLink: ['.profile-card-one-to-one__profile-link', '[data-test-app-aware-link]'],
      sendMsgBtn: ['button[aria-label="Send now"]', '.artdeco-button--primary'],
    },
    insertHTML: true,
    setOutreachWrap($editor, $wrap) {
      const $editorWrap = getEditorWrap($editor);
      if (!$editorWrap) return;
      $editorWrap.querySelector('[data-histre-outreach-wrap]')?.remove();

      if ($editorWrap.classList.contains('artdeco-modal__content')) {
        $wrap.classList.add(prefix('wrap--connect-message-modal'));
      }

      $editorWrap.appendChild($wrap);
    },
  };

  // Overwrites for LinkedIn Sales Navigator
  if (location.pathname.startsWith('/sales/')) {
    config.queries = {
      editor: 'textarea',
      editorWrap: ['.connect-cta-form__content-container', '.compose-form__container section'],
      msgWrap: ['.connect-cta-form__content-container', '.message-overlay'],
      name: ['.artdeco-entity-lockup__title', '[data-anonymize="person-name"]'],
    };
    config.insertHTML = false;
    config.setOutreachWrap = function ($editor, $wrap) {
      const $editorWrap = getEditorWrap($editor);
      if (!$editorWrap) return;
      $editorWrap.querySelector('[data-histre-outreach-wrap]')?.remove();

      if ($editorWrap.classList.contains('connect-cta-form__content-container')) {
        $wrap.classList.add(prefix('wrap--sales-message-modal'));
      }

      if ($editor.nextElementSibling?.classList?.contains('compose-signature')) {
        $wrap.classList.add(prefix('wrap--sales-message-popup'));
        $editor.nextElementSibling.after($wrap);
      } else {
        $editor.after($wrap);
      }
    };
  }

  // Addition for /search/ name caching when clicking on "Connect" button
  document.addEventListener('click', (e) => {
    if (location.pathname.startsWith('/search/')) {
      if (e.target?.closest('button')?.innerText === 'Connect') {
        const $wrap = e.target.closest('.entity-result');
        const fullName = $wrap
          ?.querySelector('.entity-result__title-text')
          ?.innerText.split('\n')[0];

        if (fullName?.split(' ')?.length === 2) {
          cachedFullName = fullName;
        } else {
          cachedFullName = '';
        }

        const $profileLink = $wrap?.querySelector('.entity-result__title-text .app-aware-link');

        if ($profileLink) {
          let href = $profileLink?.getAttribute('href');
          let url;

          if (href) {
            url = processProfileUrl(href);
          }

          // Check for encoded profile url
          if (url && checkIfProfileUrlIsEncoded(url)) {
            url = '';
          }

          if (url) {
            cachedUrl = url;
          } else {
            cachedUrl = '';
          }
        }
      }
    } else {
      // Reset cached attributes to decrease a chance of wrong one being used
      cachedFullName = '';
      cachedUrl = '';
    }
  });

  //----- Messages -----//

  function onMessageHandler(message) {
    if (message.action === 'setupTemplates') {
      templates = message.templates;

      if (templates.length <= 0) {
        return;
      }
      chrome.storage.local.get('outreachDefaultTemplate', (result) => {
        if (result.outreachDefaultTemplate) {
          defaultTemplateIndex = templates.findIndex(
            (template) => template.template_id === result.outreachDefaultTemplate,
          );
          if (defaultTemplateIndex < 0) {
            defaultTemplateIndex = 0;
            chrome.storage.local.set({ outreachDefaultTemplate: templates[0].template_id });
          }
        } else {
          defaultTemplateIndex = 0;
          chrome.storage.local.set({ outreachDefaultTemplate: templates[0].template_id });
        }

        templates = templates.map((template) => {
          template.elements = template.text.split('\n').map((paragraph) => {
            const $elem = document.createElement('p');
            if (!paragraph) {
              $elem.appendChild(document.createElement('br'));
            } else {
              $elem.innerText = paragraph;
            }
            return $elem;
          });
          return template;
        });

        // Re-init existing UI
        document.querySelectorAll('[data-histre-has-outreach]').forEach(($editor) => {
          addOutreachUI($editor);
        });
      });
    }
  }

  //----- UI elements -----//

  function createUI($editor) {
    let selectedTemplateIndex = defaultTemplateIndex;
    let isTemplateUsed = false;

    const $logo = $e('div', { class: prefix('logo-wrap') }, { click: openWebappEdit }, [
      $e('img', {
        class: prefix('logo'),
        src: chrome.runtime.getURL('assets/outreach/logo.svg'),
        alt: 'histre logo',
      }),
    ]);
    const $ctaBtn = $e('div', { class: prefix('cta-btn') }, { click: openWebappEdit }, [
      $t('Set Up'),
    ]);
    const $button = $e(
      'div',
      { class: prefix('btn'), 'data-histre-outreach-btn': '' },
      { click: injectTemplate },
      [$t(templates[selectedTemplateIndex]?.title ?? 'template')],
    );
    const $dropdownToggle = $e(
      'div',
      { class: prefix('dropdown-toggle') },
      { click: toggleDropdown },
      [$e('div', { class: prefix('dropdown-icon') })],
    );
    const $edit = $e('div', { class: prefix('edit') }, { click: openWebappEdit }, [
      $e('div', {
        class: prefix('edit-icon'),
        title: 'Open template editor',
        'aria-label': 'Open template editor',
      }),
    ]);
    const $dropdownMenu = $e('div', {
      class: prefix('dropdown-menu'),
      'data-histre-outreach-menu': '',
      'data-histre-element': '',
    });
    const $wrap = $e(
      'div',
      { class: prefix('wrap'), 'data-histre-outreach-wrap': '', 'data-histre-element': '' },
      noEvents,
      [$logo],
    );

    function injectTemplate() {
      $editor.innerText = '';
      const $msgWrap = getMsgWrap($editor);
      const name = getNameWithFallbacks($msgWrap);

      const [firstName, lastName] = name ? name.split(' ') : [];

      if (config.insertHTML && $editor.nodeName !== 'TEXTAREA') {
        for (let i = 0; i < templates[selectedTemplateIndex].elements.length; i++) {
          const $elem = templates[selectedTemplateIndex].elements[i].cloneNode(true);
          if ($elem.innerText.includes('{first_name}')) {
            $elem.innerText = $elem.innerText.replace('{first_name}', firstName ?? '');
          }
          if (lastName && $elem.innerText.includes('{last_name}')) {
            $elem.innerText = $elem.innerText.replace('{last_name}', lastName ?? '');
          }
          $editor.appendChild($elem);
        }
      } else {
        $editor.value = templates[selectedTemplateIndex].text
          .replace('{first_name}', firstName ?? '')
          .replace('{last_name}', lastName ?? '');
      }

      // Dispatch input event to trigger update events of element
      $editor.dispatchEvent(new Event('input', { bubbles: true }));

      isTemplateUsed = true;

      chrome.runtime.sendMessage({ action: 'logOutreachTemplateUsed' });
    }

    function toggleDropdown() {
      function hideDropdown() {
        $dropdownMenu.classList.remove(prefix('active'));
        $dropdownToggle.classList.remove(prefix('active'));
        document.removeEventListener('click', hideDropdown);
      }

      if ($dropdownMenu.classList.contains(prefix('active'))) {
        return;
      }
      const { top, left } = $button.getBoundingClientRect();

      $dropdownMenu.classList.add(prefix('active'));
      $dropdownMenu.style.top = `${top - $dropdownMenu.clientHeight - 5}px`;
      $dropdownMenu.style.left = `${left}px`;
      $dropdownToggle.classList.add(prefix('active'));
      // Timeout is there because, there's a problem where event below
      // would be called with the same click that triggers this function
      setTimeout(() => {
        document.addEventListener('click', hideDropdown);
      }, 1);
    }

    function createTemplateList() {
      $dropdownMenu.innerText = '';
      if (templates.length <= 0) {
        return;
      }

      templates.forEach((template, index) => {
        const $listItem = $e(
          'div',
          { class: prefix('dropdown-item') },
          { click: selectTemplateAndInject },
          [$t(template.title)],
        );

        function selectTemplateAndInject() {
          selectedTemplateIndex = index;
          $button.innerText = template.title;
          injectTemplate();
          defaultTemplateIndex = selectedTemplateIndex;
          chrome.storage.local.set({ outreachDefaultTemplate: template.template_id });
        }

        $dropdownMenu.appendChild($listItem);
      });
    }

    function getTemplateCollectionId() {
      return templates[selectedTemplateIndex].book_id;
    }

    function getIsTemplateUsed() {
      return isTemplateUsed;
    }

    function setIsTemplateUsed(value) {
      if (typeof value !== 'boolean') return;
      isTemplateUsed = value;
    }

    if (templates.length > 0) {
      $wrap.append($button, $dropdownToggle, $edit);
      createTemplateList();
    } else {
      $wrap.append($ctaBtn, $edit);
    }

    return { $wrap, $dropdownMenu, getIsTemplateUsed, setIsTemplateUsed, getTemplateCollectionId };
  }

  function getEditorWrap($editor) {
    return getClosestOfAnArrayOfQueries($editor, config.queries.editorWrap);
  }

  function getMsgWrap($editor) {
    return getClosestOfAnArrayOfQueries($editor, config.queries.msgWrap);
  }

  function setUpMsgSentListener($editor, outreachUI) {
    const { getIsTemplateUsed, setIsTemplateUsed, getTemplateCollectionId } = outreachUI;

    const $form = $editor.closest('form');

    // We will reinit the submit event also when we reinit the UI (we need to reinit UI,
    // instead of just skipping it, because editor context will be lost, so templates don't work).
    // While it's not ideal, it's fine to have multiple submit listeners on a same element,
    // because only one with isTemplateUsed are going to be submitted
    if ($form) {
      $form.addEventListener('submit', (e) => {
        if (getIsTemplateUsed() === false) {
          return;
        }
        setIsTemplateUsed(false);

        const $msgWrap = getMsgWrap($editor);
        let url;

        if ($msgWrap) {
          const $url = getElementOfAnArrayOfQueries($msgWrap, config.queries.profileLink);
          let href = $url?.getAttribute('href');
          if (href) {
            url = processProfileUrl(href);
          }
        }

        if (url && checkIfProfileUrlIsEncoded(url)) {
          return;
        }

        if (!url) {
          if (location.pathname.startsWith('/in/')) {
            url = location.href;
          } else if (cachedUrl) {
            url = cachedUrl;
          }
        }

        if (url) {
          const bookId = getTemplateCollectionId();
          const name = getNameWithFallbacks($msgWrap);

          saveLinkedInProfile({ url, bookId, name });
        }
      });
    } else {
      const $modalWrap = getClosestOfAnArrayOfQueries($editor, ['.artdeco-modal', '.send-invite']);
      if ($modalWrap) {
        const $sendBtn = getElementOfAnArrayOfQueries($modalWrap, config.queries.sendMsgBtn);

        $sendBtn.setAttribute('data-histre-outreach-msg-send', '');
        $sendBtn.addEventListener('click', (e) => {
          if (getIsTemplateUsed() === false) {
            return;
          }
          setIsTemplateUsed(false);

          if (cachedUrl) {
            const bookId = getTemplateCollectionId();

            saveLinkedInProfile({ url: cachedUrl, bookId, name: cachedFullName });
          }
        });
      }
    }
  }

  function addOutreachUI($editor) {
    const outreachUI = createUI($editor);

    config.setOutreachWrap($editor, outreachUI.$wrap);
    if (!location.pathname.startsWith('/sales/')) {
      setUpMsgSentListener($editor, outreachUI);
    }
    document.body.appendChild(outreachUI.$dropdownMenu);
  }

  //----- Helper functions -----//

  function saveLinkedInProfile(data = {}, cb) {
    const { url, bookId, name } = data;
    if (!url || !bookId) return;

    chrome.runtime.sendMessage({ action: 'getLinkedInAttrs', url: url }, (result) => {
      let title = '';

      if (name) {
        title = name;
      } else {
        if (result.data.first_name) {
          title = result.data.first_name;
        }

        if (result.data.first_name && result.data.last_name) {
          title += ' ';
        }

        if (result.data.last_name) {
          title += result.data.last_name;
        }
      }
      if (title) {
        title += ' | LinkedIn';
      }

      const payload = { url: url, book_ids: [bookId] };

      if (result.error === false && typeof result.data === 'object') {
        payload.extracted_attrs = result.data;
      }

      if (title) {
        payload.title = title;
      }

      chrome.runtime.sendMessage(
        {
          action: 'appendToNote',
          data: { note: payload, attrpriority: 'old' },
          forOutreach: true,
        },
        (result) => {
          if (typeof cb === 'function') cb();
        },
      );
    });
  }

  function getClosestOfAnArrayOfQueries($ref, queries) {
    if (!Array.isArray(queries) && typeof queries === 'string') {
      return $ref.closest(queries);
    }
    let $el;
    for (const query of queries) {
      $el = $ref.closest(query);
      if ($el) break;
    }
    return $el;
  }

  function getElementOfAnArrayOfQueries($ref, queries) {
    if (!Array.isArray(queries) && typeof queries === 'string') {
      return $ref.querySelector(queries);
    }
    let $el;
    for (const query of queries) {
      $el = $ref.querySelector(query);
      if ($el) break;
    }
    return $el;
  }

  function getNameWithFallbacks($msgWrap) {
    let name = '';

    if (!$msgWrap) {
      const $name = getElementOfAnArrayOfQueries($msgWrap, config.queries.name);
      name = $name?.innerText;
    } else {
      if (location.pathname.startsWith('/in/')) {
        const $name = document.querySelector('h1');
        name = $name?.innerText;
      } else if (cachedFullName) {
        name = cachedFullName;
      }
    }

    return name;
  }

  function processProfileUrl(url) {
    if (!url) return '';

    let processedUrl = url;

    if (!url.startsWith('https://www.linkedin.com/')) {
      processedUrl = 'https://www.linkedin.com' + url;
    }

    if (url.includes('?')) {
      const fullUrl = new URL(processedUrl);
      processedUrl = fullUrl.origin + fullUrl.pathname + '/';
    }

    return processedUrl;
  }

  function checkIfProfileUrlIsEncoded(url) {
    if (!url) return;

    const fullUrl = new URL(url);
    const splitPathname = fullUrl.pathname.split('/');
    let isEncoded = false;

    for (const fragment of splitPathname) {
      if (fragment.length === 39) {
        isEncoded = true;
      }
    }

    return isEncoded;
  }

  function openWebappEdit() {
    window.open(host + '/integrations/outreach/');
  }

  //----- Setup -----//

  histreDOMHelper.loadFont();

  setInterval(() => {
    if (!templates) {
      return;
    }
    const $editors = document.querySelectorAll(config.queries.editor);
    $editors.forEach(($editor) => {
      if ($editor && !$editor.hasAttribute('data-histre-has-outreach')) {
        $editor.setAttribute('data-histre-has-outreach', '');
        addOutreachUI($editor);
      }
    });
  }, 1000);

  (document.head || document.documentElement).appendChild($css);
  chrome.runtime.onMessage.addListener(onMessageHandler);

  return true;
})();
