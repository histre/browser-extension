(function () {
  const histreDOMHelper = window.histreDOMHelper;
  const { $e, $t, noEvents } = histreDOMHelper;
  const prefix = (classNames) => histreDOMHelper.prefix('highlight', classNames);
  const host = histreDOMHelper.getHost();
  const highlightLinkBase = host + '/highlights/';

  const minHighlightLength = 4;

  const options = {
    wildcards: 'enabled',
    acrossElements: true,
    separateWordSearch: false,
    ignoreJoiners: true,
    ignorePunctuation: true,
  };

  const hostsWithMenuBelow = {
    'coda.io': true,
    'medium.com': true,
    'slack.com': true,
  };
  const domainName = location.hostname.split(/\./).slice(-2).join('.');
  const isMenuBelow = domainName in hostsWithMenuBelow;

  //----- UI elements -----//

  const $arrow = $e('span', { class: prefix('arrow') });
  const $arrowWrap = $e('div', { class: prefix('arrow-wrap') }, noEvents, [$arrow]);

  // Create menu

  const $info = $e(
    'div',
    { class: prefix('icon icon--info'), title: 'Info', 'aria-label': 'Info' },
    { click: openHistre },
  );

  const $createMenuContent = $e('div', { class: prefix('menu-content') }, noEvents, [
    ...getColorElements('create'),
    $info,
  ]);
  const $createMenu = $e('div', { class: prefix('menu') }, noEvents, [
    $createMenuContent,
    $arrowWrap,
  ]);

  // Edit menu

  const $noteText = $e('div', { class: prefix('note-text') }, { click: openNoteEditor });
  const $noteInput = $e(
    'textarea',
    {
      class: prefix('note-input'),
      type: 'text',
      placeholder: 'Type notes and press Enter to save...',
      autocomplete: 'off',
      rows: '1',
    },
    {
      keydown: handleNoteInputKeydown,
      input: resizeNoteInput,
    },
  );
  const $createNote = $e(
    'div',
    {
      class: prefix('icon icon--note'),
      title: 'Add note to highlight',
      'aria-label': 'Add note to highlight',
    },
    { click: openNoteEditor },
  );
  const $copy = $e(
    'div',
    {
      class: prefix('icon icon--copy'),
      title: 'Copy highlight link to clipboard',
      'aria-label': 'Copy highlight link to clipboard',
    },
    { click: copyHighlightLinkToClipboard },
  );
  const $delete = $e(
    'div',
    {
      class: prefix('icon icon--delete'),
      title: 'Delete highlight',
      'aria-label': 'Delete highlight',
    },
    { click: deleteHighlight },
  );
  // Event listeners must be manually added to cloned nodes
  const $infoClone = $info.cloneNode();
  $infoClone.addEventListener('click', openHistre);

  const $editMenuContent = $e('div', { class: prefix('menu-content') }, noEvents, [
    ...getColorElements('edit'),
    $noteText,
    $noteInput,
    $createNote,
    $copy,
    $delete,
    $infoClone,
  ]);
  const $editMenu = $e('div', { class: prefix('menu') }, noEvents, [
    $editMenuContent,
    $arrowWrap.cloneNode(true),
  ]);

  // Wrap

  const $wrap = $e(
    'div',
    {
      id: prefix('wrap'),
      class: prefix('wrap'),
      style: 'display: none;', // prevent some strange interactions until styles are loaded
    },
    noEvents,
    [$createMenu, $editMenu],
  );

  if (isMenuBelow) {
    $wrap.classList.add(prefix('wrap--bottom'));
  }

  const $css = $e('link', {
    rel: 'stylesheet',
    type: 'text/css',
    href: chrome.runtime.getURL('highlight.css'),
  });

  //----- UI functionality -----//

  function positionUI(coordinates, $menu) {
    const top = isMenuBelow
      ? coordinates.bottom + 8 + window.scrollY
      : coordinates.top - $menu.getBoundingClientRect().height - 8 + window.scrollY;
    const left = coordinates.left + coordinates.width / 2 - $menu.getBoundingClientRect().width / 2;

    if ($menu === $createMenu) {
      $createMenu.style.top = top + 'px';
      $createMenu.style.left = left + 'px';
      // Set up edit menu coordinates because it's shown right after highlight is created
      $editMenu.style.top = top + 'px';
      $editMenu.style.left = left - 24 + 'px';
    } else if ($menu === $editMenu) {
      $editMenu.style.top = top + 'px';
      $editMenu.style.left = left + 'px';
    }
  }

  function openEditMenu(coordinates, noteText) {
    if ($copy.classList.contains(prefix('icon--check'))) {
      $copy.classList.remove(prefix('icon--check'));
      $copy.classList.add(prefix('icon--copy'));
      $copy.setAttribute('title', 'Copy highlight link to clipboard');
      $copy.setAttribute('aria-label', 'Copy highlight link to clipboard');
    }
    $noteInput.classList.remove(prefix('note-input--active'));
    $noteInput.value = '';

    if (noteText) {
      $noteText.innerText = noteText;
      $noteText.classList.add(prefix('note-text--active'));
    } else {
      $noteText.innerText = '';
      $noteText.classList.remove(prefix('note-text--active'));
    }

    openUI(coordinates, $editMenu);
  }

  function openUI(coordinates, $menu) {
    $menu.classList.add(prefix('menu--active'));
    if (coordinates) {
      positionUI(coordinates, $menu);
    }
  }

  function closeUI() {
    if ($createMenu.classList.contains(prefix('menu--active'))) {
      $createMenu.classList.remove(prefix('menu--active'));
    } else if ($editMenu.classList.contains(prefix('menu--active'))) {
      $editMenu.classList.remove(prefix('menu--active'));
    }
  }

  function nukeSelf() {
    window.histreLoadedHighlight = false;
    clearOld();
    clearAllMarks();
    chrome.runtime.onMessage.removeListener(onMessageHandler);
  }

  function clearOld(cb) {
    while (document.getElementById(prefix('wrap'))) {
      document.getElementById(prefix('wrap')).remove();
    }

    while (document.querySelector(`link[href="${chrome.runtime.getURL('highlight.css')}"]`)) {
      document.querySelector(`link[href="${chrome.runtime.getURL('highlight.css')}"]`).remove();
    }

    document.body.removeEventListener('mouseup', handleMouseUp);
    window.removeEventListener('mousedown', handleMouseDown);

    if (cb) {
      cb();
    }
  }

  function openHistre() {
    window.open(`${host}/features/highlights/`);
  }

  //----- Highlighting -----//

  function getColorElements(type = 'create') {
    const colors = ['yellow', 'orange', 'green', 'blue', 'purple', 'red'];
    const $colors = [];

    function highlight(color) {
      const sel = window.getSelection();

      if (sel && sel.rangeCount > 0) {
        const range = sel.getRangeAt(0);
        const text = range.toString().trim();
        window.getSelection().removeAllRanges();
        if (text.length < minHighlightLength) {
          return;
        }

        getUserInfo(({ externalId }) => {
          const highlightId = sha1(externalId + location.href + text).substring(0, 8);
          $editMenu.dataset.histreHighlightId = highlightId;
          $editMenu.dataset.histreHighlightColor = color;
          renderHighlights([[text, color, highlightId]], true);
        });

        closeUI();

        // Persist highlight on the backend
        chrome.runtime.sendMessage(
          {
            action: 'saveHighlight',
            highlight: {
              color: color,
              text: text,
              url: location.href,
              title: document.title,
              tweet: false,
            },
          },
          function (result) {
            if (chrome.runtime.lastError) {
              console.log(JSON.stringify(chrome.runtime.lastError));
            }
            if (result && result.usermsg) {
              // TODO: Replace below with logic to show error message to user
              console.log(result.usermsg);
            }
          },
        );
      }
    }

    function edit(color) {
      const highlightId = $editMenu.dataset.histreHighlightId;
      const marks = [...document.getElementsByClassName(prefix(`id-${highlightId}`))];
      const markColorClass = [...marks[0].classList].filter((className) =>
        className.includes(prefix('color-')),
      )[0];
      marks.forEach((mark) => {
        mark.classList.replace(markColorClass, prefix(`color-${color}`));
      });

      closeUI();

      // Persist highlight on the backend
      chrome.runtime.sendMessage({
        action: 'updateHighlight',
        highlight: {
          id: highlightId,
          color: color,
        },
      });
    }

    colors.forEach((color) => {
      $colors.push(
        $e(
          'div',
          { class: prefix(`btn btn--${color}`) },
          {
            click:
              type === 'create'
                ? () => {
                    highlight(color);
                  }
                : () => {
                    edit(color);
                  },
          },
        ),
      );
    });

    return $colors;
  }

  function clearAllMarks() {
    const context = document.querySelector('body');
    const instance = new Mark(context);
    instance.unmark();
  }

  function renderHighlights(highlights, isNew = false) {
    const context = document.querySelector('body');
    const instance = new Mark(context);

    highlights.sort(function (a, b) {
      // prevent highlights of substrings being hidden by superstring highlight
      return b[0].length - a[0].length;
    });

    for (let i = 0; i < highlights.length; i++) {
      const [txt, color, highlightId, note] = highlights[i];
      if (typeof color === 'undefined') {
        color = 'yellow';
      }

      if (isNew) {
        options.done = () => {
          openEditMenu();
        };
      } else {
        options.done = () => {};
      }

      if (note) {
        options.each = (mark) => {
          mark.setAttribute('data-histre-highlight-note', note);
        };
      } else {
        options.each = () => {};
      }

      instance.mark(txt, {
        ...options,
        className: prefix(`color-${color} id-${highlightId}`),
      });
    }
  }

  function openNoteEditor() {
    if ($noteText.innerText) {
      $noteInput.value = $noteText.innerText;
      $noteText.classList.remove(prefix('note-text--active'));
    } else {
      $noteInput.value = '';
    }
    $noteInput.classList.add(prefix('note-input--active'));
    resizeNoteInput();
    $noteInput.focus();
  }

  function copyHighlightLinkToClipboard() {
    const highlightId = $editMenu.dataset.histreHighlightId;
    getUserInfo(({ username }) => {
      navigator.clipboard.writeText(highlightLinkBase + username + '/' + highlightId + '/');
      this.classList.remove(prefix('icon--copy'));
      this.classList.add(prefix('icon--check'));
      this.setAttribute('title', 'Highlight link copied to clipboard');
      this.setAttribute('aria-label', 'Highlight link copied to clipboard');
    });
  }

  function deleteHighlight() {
    const highlightId = $editMenu.dataset.histreHighlightId;
    const context = document.querySelector('body');
    const instance = new Mark(context);
    closeUI();
    instance.unmark({
      className: prefix(`id-${highlightId}`),
    });
    chrome.runtime.sendMessage({
      action: 'deleteHighlight',
      highlight: {
        id: highlightId,
      },
    });
  }

  //----- Messages -----//

  function onMessageHandler(message) {
    if (message.action === 'showHighlights' && message.data) {
      const h = message.data.map((x) => {
        return [x.text, x.color || 'blue', x.highlight_id, x.note];
      });
      renderHighlights(h);
      return;
    }

    if (message.action === 'disableHighlights') {
      nukeSelf();
      return;
    }
  }

  //----- Event listeners -----//

  function handleMouseUp(e) {
    if (!window.getSelection()?.toString() && e.target?.nodeName?.toLowerCase() === 'mark') {
      const markCoordinates = e.target.getBoundingClientRect();
      // Set highlight reference for editing
      const markClasses = [...e.target.classList];
      $editMenu.dataset.histreHighlightId = markClasses
        .filter((className) => className.includes(prefix('id-')))
        .map((className) => className.replace(prefix('id-'), ''))[0];

      $editMenu.dataset.histreHighlightColor = markClasses
        .filter((className) => className.includes(prefix('color-')))
        .map((className) => className.replace(prefix('color-'), ''))[0];

      const noteText = e.target.getAttribute('data-histre-highlight-note');

      openEditMenu(markCoordinates, noteText);
    } else if (
      window.getSelection()?.toString().trim().length >= minHighlightLength &&
      e.target?.tagName !== 'BUTTON' &&
      (e.target?.nodeName?.toLowerCase() === 'mark' ||
        (!(e.target?.className instanceof SVGAnimatedString) &&
          !e.target?.className?.includes?.('histre-highlight')))
    ) {
      const selectedText = window.getSelection();
      if (selectedText && selectedText.rangeCount > 0) {
        const textCoordinates = selectedText.getRangeAt(0).getBoundingClientRect();
        if (textCoordinates.x === 0 && textCoordinates.y === 0) {
          // Handle a case when user selects text from highlight note input,
          // but releases a mouse outside of input
          return;
        }

        openUI(textCoordinates, $createMenu);
      }
    }
  }

  function handleMouseDown(e) {
    if (e.target?.nodeName?.toLowerCase() === 'mark' || e.target === $noteInput) {
      // no op
      // if the target is mark, don't do e.preventDefault() from below
      // this is so users can select the highlighted text
      //
      // we want to do same for highlight note input, to allow selecting text from it
    } else if (e.target?.className?.includes?.('histre-highlight')) {
      // clicking on the highlight ui shouldn't remove the selection before we can grab it
      e.preventDefault();
    } else {
      closeUI();
    }
  }

  function handleSelectionChange() {
    if ($editMenu.classList.contains(prefix('menu--active'))) {
      // prevent this from firing when edit menu is active, so that doesn't interfere
      // with highlight note input
      return;
    }
    // handle the case where user clicks on selected text to unselect it
    // we need this because, when the user clicks on the current selection,
    // the selection is cleared AFTER the mouseup event
    if (!window.getSelection()?.toString()) {
      closeUI();
    }
  }

  function handleNoteInputKeydown(e) {
    if (e.key === 'Enter') {
      this.disabled = true;
      const highlightId = $editMenu.dataset.histreHighlightId;
      const color = $editMenu.dataset.histreHighlightColor;
      const note = this.value;
      const marks = [...document.getElementsByClassName(prefix(`id-${highlightId}`))];
      marks.forEach((mark) => {
        if (note) {
          mark.setAttribute('data-histre-highlight-note', note);
        } else {
          mark.removeAttribute('data-histre-highlight-note');
        }
      });
      chrome.runtime.sendMessage(
        {
          action: 'updateHighlight',
          highlight: {
            id: highlightId,
            color,
            note,
          },
        },
        (result) => {
          this.disabled = false;
          if (result?.error === false) {
            $noteText.innerText = note;
            this.classList.remove(prefix('note-input--active'));
            if (note) {
              $noteText.classList.add(prefix('note-text--active'));
            } else {
              $noteText.classList.remove(prefix('note-text--active'));
            }
          }
          if (chrome.runtime.lastError) {
            console.log(JSON.stringify(chrome.runtime.lastError));
          }
        },
      );
    } else if (e.key === 'Escape') {
      this.classList.remove(prefix('note-input--active'));
      if ($noteText.innerText) {
        $noteText.classList.add(prefix('note-text--active'));
      }
    }
  }

  //----- Unsorted functions -----//

  function getUserInfo(cb) {
    chrome.storage.local.get(['userUsername', 'userExternalId'], (items) => {
      if (items.userUsername && items.userExternalId) {
        return cb({
          username: items.userUsername,
          externalId: items.userExternalId,
        });
      } else {
        chrome.runtime.sendMessage({ action: 'setUserInfo', dest: 'highlights' }, {}, (result) => {
          if (result.isSet) {
            getUserInfo(cb);
          } else {
            //TODO Implement some sort of error message or figure out how to disable menu on user signout
            return;
          }
        });
      }
    });
  }

  function resizeNoteInput() {
    $noteInput.style.height = '0px';
    $noteInput.style.height = $noteInput.scrollHeight + 2 + 'px';
  }

  //----- Initial setup -----//

  histreDOMHelper.loadFont();

  clearOld(() => {
    (document.head || document.documentElement).appendChild($css);
    document.body.appendChild($wrap);
    document.body.addEventListener('mouseup', handleMouseUp);
    window.addEventListener('mousedown', handleMouseDown);

    // handle the case where user clicks on selected text to unselect it
    document.onselectionchange = handleSelectionChange;
  });

  chrome.runtime.onMessage.addListener(onMessageHandler);

  return true;
})();
