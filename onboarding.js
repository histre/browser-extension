window.addEventListener('hashchange', function () {
  goToSection(location.hash);
});

function goToSection(hash) {
  if (hash && hash.includes('#')) {
    hash = hash.substring(1);
    document.querySelectorAll('section').forEach((section) => {
      if (section.id === hash) {
        section.classList.remove('d-none');
      } else {
        section.classList.add('d-none');
      }
    });
  } else {
    document.querySelectorAll('section').forEach((section) => {
      if (section.id === 'home') {
        section.classList.remove('d-none');
      } else {
        section.classList.add('d-none');
      }
    });
  }
}

document.getElementById('btn-allow-permissions').addEventListener('click', () => {
  const features = new Set();
  const permissions = new Set();
  let origins = new Set();

  document.querySelectorAll('#permissions input[type="checkbox"]').forEach((checkbox) => {
    if (checkbox.checked) {
      switch (checkbox.id) {
        case 'enhanceWebSitesSwitch':
          features.add('enhanceWebSites');
          permissions.add('bookmarks');
          permissions.add('history');
          origins.add('*://*/*');
          break;
        case 'highlightsSwitch':
          features.add('highlights');
          origins.add('*://*/*');
          break;
        case 'historySwitch':
          features.add('history');
          origins.add('*://*/*');
          break;
        case 'inlineSwitch':
          features.add('inline');
          origins.add('*://*/*');
          break;
      }
    }
  });

  chrome.permissions.request(
    {
      origins: [...origins],
      permissions: [...permissions],
    },
    (granted) => {
      if (granted) {
        chrome.storage.local.set({
          enablePermissions: features.has('enhanceWebSites'),
          featuresEnhanceWebsites: features.has('enhanceWebSites'),
          featuresHighlights: features.has('highlights'),
          featuresHistory: features.has('history'),
          featuresInline: features.has('inline'),
          inlineIsShown: features.has('inline'),
          cmdPaletteIsShown: features.has('inline'),
        });

        location.hash = '#finish';
      }
    },
  );
});

goToSection(location.hash);
