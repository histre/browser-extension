(function () {
  const histreDOMHelper = window.histreDOMHelper;
  const { $e, $t, noEvents } = histreDOMHelper;
  const prefix = (classNames) => histreDOMHelper.prefix('ask-ai', classNames);
  const host = window.histreDOMHelper.getHost();

  const utils = new window.histreUtils('ask-ai');
  const askAI = utils.createAskAI({ closeStandaloneUI: closeUI });

  function nukeSelf() {
    window.histreLoadedAskAI = false;
    clearOld();
    chrome.runtime.onMessage.removeListener(onMessageHandler);
  }

  function clearOld(cb) {
    while (document.getElementById(prefix('wrap'))) {
      document.getElementById(prefix('wrap')).remove();
    }

    while (document.querySelector(`link[href="${chrome.runtime.getURL('ask-ai.css')}"]`)) {
      document.querySelector(`link[href="${chrome.runtime.getURL('ask-ai.css')}"]`).remove();
    }

    if (cb) {
      cb();
    }
  }

  function closeUI() {
    nukeSelf();
  }

  function onMessageHandler(message, sender, sendResponse) {
    if (message.action === 'focusAskAIInput') {
      askAI.focusInput();
    }

    if (message.action === 'setAskAINoteModified') {
      $wrap.dataset.histreNoteModified = message.noteModified;
    }

    if (message.action === 'loadAskAIPrompts') {
      askAI.loadPrompts(message.data);
    }
  }

  function openHistre() {
    window.open(host);
  }

  const $closeUIButton = $e(
    'button',
    {
      class: prefix('control-icon control-icon--close'),
      title: 'Close',
      'aria-label': 'Close',
    },
    { click: closeUI },
  );
  const $controlButtonsWrap = $e('div', { class: prefix('control-btns-wrap') }, noEvents, [
    $closeUIButton,
  ]);

  const $logoImg = $e('img', {
    src: chrome.runtime.getURL('assets/ask-ai/logo.png'),
    alt: 'Histre logo',
  });
  const $logoTxt = $e('div', {}, noEvents, [$t('histre')]);
  const $logo = $e(
    'div',
    {
      class: prefix('logo'),
      title: 'Open Histre',
      'aria-label': 'Open Histre',
    },
    { click: openHistre },
    [$logoImg, $logoTxt],
  );

  const $header = $e('div', { class: prefix('header') }, noEvents, [$logo, $controlButtonsWrap]);

  const $body = $e('div', { class: prefix('body') }, noEvents, [askAI.element]);

  const $wrap = $e(
    'div',
    {
      id: prefix('wrap'),
      class: prefix('wrap'),
      style: 'display: none;', // prevent some strange interactions until styles are loaded
      'data-histre-element': '',
    },
    noEvents,
    [$header, $body],
  );

  const $css = $e('link', {
    rel: 'stylesheet',
    type: 'text/css',
    href: chrome.runtime.getURL('ask-ai.css'),
  });

  histreDOMHelper.loadFont();

  clearOld(() => {
    chrome.storage.local.get('askAIModel', (result) => {
      if (result.askAIModel) {
        askAI.setAskAIModel(result.askAIModel);
      }
      (document.head || document.documentElement).appendChild($css);
      document.body.appendChild($wrap);
    });
  });

  chrome.runtime.onMessage.addListener(onMessageHandler);

  return true;
})();
