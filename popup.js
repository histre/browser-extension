import { host, isDevMode, apiBase, isFirefox } from './globals.js';
import { helper } from '/helper.js';
import {
  USER_TAGS_CACHE_KEY,
  USER_LAST_TAG_MODIFIED_KEY,
  convertTagsHashToList,
  fetchUserTags,
} from '/tag-utils.js';

const tagUrl = `${apiBase}/tag/`;
const tabSaveUrl = `${apiBase}/collections/save_tabs_or_windows/`;
const loginPageUrl = `${host}/accounts/login/`;
const contentAddressableApiUrl = `${apiBase}/content_addressable_mapping/`;

const utilsRefs = {
  quill: null,
  saveNote: null,
  saveNoteDebounced: null,
  tagList: null,
};
const utils = new window.histreUtils('popup', utilsRefs);
const dataTable = utils.createExtractedDataTable();

let pageUrl = null;
let pageTitle = null;
let currentTabId = null;
let noteLog = '';
let quill;
let enableSettings = true;
let disableNav = false;

let contentAddressableMapping = null;
let isAddressableUrl = false;

let recentBooks = null;

let userIsPremium = false;

if (isDevMode) {
  document.querySelectorAll('a[href^="https://histre.com').forEach((link) => {
    const path = new URL(link.getAttribute('href')).pathname;
    const devLink = new URL(path, host);

    link.setAttribute('href', devLink);
  });
}

DOMPurify.addHook('afterSanitizeAttributes', (node) => {
  // allow target="_blank"
  if ('target' in node) {
    node.setAttribute('target', '_blank');
    node.setAttribute('rel', 'noopener noreferrer');
  }
});

const handleAuthError = () => {
  helper.handleAuthErrorsOnce(() => {
    // Callback to run if login page cannot be shown for the meantime
    showAuthError();
  });
};

const showNoteSetupErrorMsg = (errcode, errmsg) => {
  let reason = 'An error occurred';
  if (errmsg === helper.errmsgs.urlMissing) {
    reason = 'Page URL is missing';
  } else if (errmsg === helper.errmsgs.urlTooLong) {
    reason = `URL over ${helper.MAX_URL_LEN} chars`;
  }
  showErrorMessage(`Cannot take notes on this page: ${reason}.`);
};

function sendRuntimeMessage(message) {
  return new Promise((resolve) => {
    chrome.runtime.sendMessage(message, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}

const debounce = (func, wait, immediate) => {
  // https://davidwalsh.name/javascript-debounce-function
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

const saveNote = async ({ note, readLater, title } = {}, cb) => {
  if ($('#save-close').is(':disabled')) {
    return;
  }

  if (typeof note !== 'string') {
    note = DOMPurify.sanitize(quill.root.innerHTML);
  }

  const accessLevel = $('#note-access-level input:checked').val();
  const bookIds = recentBooks?.getBookIdsForAddNote() || null;
  const deselectedBookIds = recentBooks?.getDeselectedBookIds() || null;
  const newBookTitle = recentBooks?.getNewBookTitle() || null;
  const noteId = document.getElementById('notes').dataset.noteId || null;
  const noteModified = document.getElementById('notes').dataset.noteModified || null;

  displaySpinner();

  try {
    let newBookId;
    if (newBookTitle) {
      const result = await sendRuntimeMessage({
        action: 'createBook',
        data: { title: newBookTitle },
        url: decodeURIComponent(pageUrl),
      });
      if (!result.error) {
        newBookId = result.data.book_id;
        bookIds.push(newBookId);
        recentBooks.enableNewBook(newBookId);
      } else {
        recentBooks.enableNewBookActions();
        throw new Error(result.errorMessage ?? 'Error creating collection');
      }
    }

    const saveNoteRequestPayload = {
      url: pageUrl,
      title: pageTitle,
      note: note,
      book_ids: bookIds,
      access: accessLevel,
    };

    const tableValues = dataTable.getTableData();

    if (tableValues) {
      saveNoteRequestPayload['extracted_attrs'] = tableValues;
    }
    if (noteModified) {
      saveNoteRequestPayload.note_modified = noteModified;
    }
    if (readLater) {
      saveNoteRequestPayload.read_later = true;
    }
    if (title) {
      saveNoteRequestPayload.user_title = title;
    }

    const result = await sendRuntimeMessage({
      action: 'saveNote',
      data: {
        note: saveNoteRequestPayload,
        removeNoteFromBooks: deselectedBookIds,
        tabId: currentTabId,
        newBook: {
          id: newBookId,
          title: newBookTitle,
        },
      },
      url: decodeURIComponent(pageUrl),
    });
    const { error, errmsg, errcode, status } = result;

    if (error) {
      if (helper.isAuthErrorStatus(errcode)) {
        handleAuthError();
        return;
      } else {
        throw new Error(result.errorMessage ?? 'An error occurred while saving note');
      }
    } else {
      if (result.newNote) {
        await setupNote(result.newNote);
        displayCheckmark();
        return;
      } else {
        const noteId = result.data.item_id;

        setNoteIsPresentIcon(noteId);
        document.getElementById('notes').dataset.noteId = noteId;
        document.getElementById('notes').dataset.noteModified = result.data.note_modified;
        noteLog = note;

        const $noteTitleText = document.querySelector('#note-title-text');

        if (!$noteTitleText.hasAttribute('data-is-set')) {
          $noteTitleText.textContent = result.note.title;
          $noteTitleText.setAttribute('data-is-set', '');
        }

        if (title) {
          $noteTitleText.textContent = title;
          $noteTitleText.setAttribute('data-is-set', '');
        }

        if (result.data.item_id) {
          if (saveNoteRequestPayload.access === 'public') {
            setPublishIcon();
          } else {
            removePublishIcon();
          }

          if (saveNoteRequestPayload.access === 'private') {
            setSavePrivatelyIcon();
          } else {
            removeSavePrivatelyIcon();
          }
        }

        if (readLater && result.data.read_later_book_id) {
          const readLaterBookId = result.data.read_later_book_id;
          const $readLater = document.querySelector('#read-later');
          $readLater.setAttribute('data-read-later-book-id', result.data.read_later_book_id);
          $readLater.setAttribute('data-read-later-active', '');
          recentBooks?.checkRecentBooks({
            bookIds: [readLaterBookId],
            itemId: noteId,
          });

          chrome.runtime.sendMessage({
            action: 'setSidebarReadLater',
            dest: 'sidePanel',
            bookId: readLaterBookId,
            url: decodeURIComponent(pageUrl),
          });

          chrome.storage.local.get('featuresInline', (result) => {
            if (result.featuresInline) {
              chrome.tabs.query({ url: helper.removeURLFragment(pageUrl, true) }, (tabs) => {
                tabs.forEach((tab) => {
                  chrome.tabs.sendMessage(tab.id, {
                    action: 'setInlineReadLater',
                    bookId: result.data.read_later_book_id,
                    noteId: result.data.item_id,
                    url: tab.url,
                  });
                });
              });
            }
          });

          bookIds.push(result.data.read_later_book_id);
        }
      }
    }

    if (deselectedBookIds?.length > 0 && (noteId || result?.data?.item_id)) {
      const reqPayload = {
        book_ids: deselectedBookIds,
        url_item_item_id: noteId || result?.data?.item_id,
      };
      const response = await sendRuntimeMessage({
        action: 'removeNoteFromBooks',
        data: {
          data: reqPayload,
          url: decodeURIComponent(pageUrl),
          tabId: currentTabId,
        },
      });
      if (response?.error !== false) {
        recentBooks?.reCheckBooks(deselectedBookIds);
        throw new Error(response.errorMessage ?? 'Unable to remove note from collection');
      }
    }

    updateSidebarUI({
      note,
      bookIds,
      accessLevel,
      title: title,
      noteId: noteId || result?.data?.item_id,
      noteModified: result?.data?.note_modified,
      attrs: tableValues,
      newBook: {
        id: newBookId,
        title: newBookTitle,
      },
    });
    displayCheckmark();
  } catch (e) {
    displayAlert();
    if (e.message) {
      setMsg(e.message, 'errmsg');
    } else {
      setMsg('An error occurred', 'errmsg');
    }
  }

  if (typeof cb === 'function') {
    cb();
  }
};
utilsRefs.saveNote = saveNote;

const saveNoteDebounced = debounce((ifChanged = false) => {
  const note = DOMPurify.sanitize(quill.root.innerHTML);

  if (ifChanged) {
    if (note !== noteLog) {
      saveNote({ note });
    }
  } else {
    saveNote({ note });
  }
}, 800);
utilsRefs.saveNoteDebounced = saveNoteDebounced;

async function deleteNote({ fromModal = false }) {
  if (fromModal) {
    document.querySelector('#delete-note-confirm-cancel').disabled = true;
    document.querySelector('#delete-note-confirm-alert').classList.add('hide');
    document.querySelector('#delete-note-confirm-error-msg').textContent = '';
    document.querySelector('#delete-note-confirm-spinner').classList.remove('hide');
  } else {
    hideAlert();
    hideCheckmark();
    setMsg('');
  }

  const noteId = document.getElementById('notes').dataset.noteId;

  if (!noteId) {
    if (fromModal) {
      document.querySelector('#delete-note-confirm-spinner').classList.add('hide');
      document.querySelector('#delete-note-confirm-alert').classList.remove('hide');
      document.querySelector('#delete-note-confirm-error-msg').textContent =
        'An error occurred, please close popup and try again';
      document.querySelector('#delete-note-confirm-delete').disabled = false;
      document.querySelector('#delete-note-confirm-cancel').disabled = false;
    } else {
      document.querySelector('#delete-note-spinner').classList.add('hide');
      document.querySelector('#notes').classList.remove('hide');
      displayAlert();
      setMsg('An error occurred, please close popup and try again', 'errmsg');
    }
    return;
  }

  const result = await sendRuntimeMessage({
    action: 'deleteNote',
    data: { payload: { url: decodeURIComponent(pageUrl) }, tabId: currentTabId },
  });

  if (result.error === false) {
    helper.setDefaultIcon(currentTabId, decodeURIComponent(pageUrl), () => {
      chrome.tabs.sendMessage(
        currentTabId,
        { action: 'resetInline', url: decodeURIComponent(pageUrl) },
        () => {
          chrome.runtime.sendMessage(
            { action: 'deleteNote', dest: 'sidePanel', url: decodeURIComponent(pageUrl) },
            () => {
              window.close();
            },
          );
        },
      );
    });
  } else {
    if (fromModal) {
      document.querySelector('#delete-note-confirm-spinner').classList.add('hide');
      document.querySelector('#delete-note-confirm-alert').classList.remove('hide');
      document.querySelector('#delete-note-confirm-error-msg').textContent =
        'An error occurred while deleting a note, please try again';
      document.querySelector('#delete-note-confirm-delete').disabled = false;
      document.querySelector('#delete-note-confirm-cancel').disabled = false;
    } else {
      document.querySelector('#delete-note-spinner').classList.add('hide');
      document.querySelector('#notes').classList.remove('hide');
      displayAlert();
      setMsg('An error occurred while deleting a note, please try again', 'errmsg');
    }
  }
}

function setNoteIsPresentIcon(noteId) {
  chrome.storage.local.get(['featuresInline'], (result) => {
    chrome.tabs.query({ url: helper.removeURLFragment(pageUrl, true) }, (tabs) => {
      tabs.forEach((tab) => {
        helper.setNoteIsPresentIcon(tab.id, tab.url);

        if (result.featuresInline === true) {
          chrome.tabs.sendMessage(tab.id, {
            action: 'setNoteIsPresent',
            noteId,
            url: tab.url,
          });
        }
      });
    });
  });
}

function updateSidebarUI(data) {
  chrome.runtime.sendMessage({
    action: 'updateNote',
    dest: 'sidePanel',
    data,
    url: decodeURIComponent(pageUrl),
  });
}

const fadingMsg = (msg) => {
  document.getElementById('notes-msg').textContent = msg;
  setTimeout(() => {
    document.getElementById('notes-msg').textContent = '';
  }, 2000);
};

const setMsg = (msg, classNames, isHTML = false) => {
  const msgWrap = document.getElementById('notes-msg');
  if (msg === '') {
    msgWrap.style.display = 'none';
    return;
  }
  msgWrap.style.display = 'block';
  msgWrap.textContent = '';

  const span = document.createElement('span');
  if (isHTML) {
    span.innerHTML = DOMPurify.sanitize(msg);
  } else {
    span.textContent = msg;
  }
  if (classNames) {
    span.setAttribute('class', classNames);
  }
  msgWrap.appendChild(span);
};

function displaySpinner() {
  hideAlert();
  hideCheckmark();
  $('#popup-wrap .saving-spinner').removeClass('hide');
}

function hideSpinner() {
  $('#popup-wrap .saving-spinner').addClass('hide');
}

function displayCheckmark() {
  // Clear any error messages on success
  setMsg('');

  hideSpinner();
  $('#popup-wrap .saving-info--checkmark').removeClass('hide');
}

function hideCheckmark() {
  $('#popup-wrap .saving-info--checkmark').addClass('hide');
}

function displayAlert() {
  hideSpinner();
  $('#popup-wrap .saving-info--error').removeClass('hide');
}

function hideAlert() {
  $('#popup-wrap .saving-info--error').addClass('hide');
}

function hideLoadingUI() {
  $('#popup-wrap .loading-spinner').addClass('hide');
  $('#popup-wrap').removeClass('popup-placeholder');
  if ($('#notes').css('display') === 'block') {
    quill.focus();
  } else if ($('#savetabs').css('display') === 'block') {
    $('#savetabs-title').select();
  }
}

async function showAuthError() {
  disableNav = true;

  const hasPermission = await chrome.permissions.contains({
    origins: [`${host.replace(/:(\d+)/, '')}/` /* remove port number */],
  });
  if (!hasPermission) {
    $('#popup-wrap .loading-spinner').addClass('hide');
    $('#permission-error').removeClass('hide');
    return;
  }

  $('#popup-wrap .loading-spinner').addClass('hide');
  $('#auth-error').removeClass('hide');
}

document.querySelector('#permission-btn').addEventListener('click', () => {
  chrome.permissions.request({
    origins: [`${host.replace(/:(\d+)/, '')}/` /* remove port number */],
  });

  // It works without timeout, but it's here just in case
  setTimeout(() => {
    window.close();
  }, 100);
});
function showErrorMessage(msg) {
  // Check if the user is authencticated and set history setting
  fetch(`${apiBase}/settings/`, helper.setRequestParams())
    .then((response) => {
      return response.json();
    })
    .then((result) => {
      if (result.error) {
        helper.setIconWarning();

        const { error, errmsg, errcode } = result;
        if (helper.isAuthErrorStatus(errcode)) {
          handleAuthError();
        } else {
          showNoteSetupErrorMsg(errcode, errmsg);
        }
      } else {
        $('#popup-wrap .loading-spinner').addClass('hide');
        $('#popup-wrap').removeClass('popup-placeholder');
        $('#notes').addClass('note-placeholder');
        $('#note-error').text(msg).removeClass('hide');

        if (typeof result.settings?.history !== 'undefined') {
          local_save_log_account(result.settings.history);
        } else {
          enableSettings = false;
        }
      }
    })
    .catch(() => {
      showAuthError();
    });
}

const saveClose = () => {
  const note = DOMPurify.sanitize(quill.root.innerHTML);
  saveNote({ note }, window.close);
};

const openlogin = () => {
  chrome.tabs.create({ url: loginPageUrl });
};

const runNoteSetup = async (currentTabId) => {
  setMsg('Loading notes... Please wait...');

  chrome.tabs.get(currentTabId, (tab) => {
    if (tab) {
      chrome.runtime.sendMessage({ action: 'getNote', tab, isAddressableUrl }, (result) => {
        setupNote(result);
      });
    } else {
      showNoteSetupErrorMsg();
    }
  });
};

async function setupNote(result) {
  if (result.error) {
    helper.setIconWarning();
    const { error, errmsg, errcode } = result;
    if (helper.isAuthErrorStatus(errcode)) {
      handleAuthError();
    } else {
      showNoteSetupErrorMsg(errcode, errmsg);
    }
  } else {
    $('#save-close').prop('disabled', false);
    helper.unsetIconWarning();
    if (typeof result.settings?.history !== 'undefined') {
      local_save_log_account(result.settings.history);
    } else {
      enableSettings = false;
    }

    if (typeof result.settings?.premium_user !== 'undefined') {
      userIsPremium = result.settings.premium_user;
    }

    let dataExtractionResult = null;
    if (!result.data.attrs) {
      dataExtractionResult = result.dataExtraction ?? result.data.dataExtraction;
    } else {
      dataExtractionResult = {
        data: result.data.attrs,
      };
    }

    const $accessLevels = document.getElementById('note-access-level');
    if (!$accessLevels.hasChildNodes()) {
      if (result.settings?.access_levels) {
        const accessLevels = utils.createAccessLevels(
          result.settings.access_levels,
          saveNote,
          quill,
        );
        $accessLevels.appendChild(accessLevels);
      } else {
        const accessLevels = utils.createAccessLevels([['private', 'Private']], saveNote, quill);
        $accessLevels.appendChild(accessLevels);
      }
    }

    try {
      if (result.data.access) {
        document.querySelector(
          `#note-access-level input[value=${result.data.access}]`,
        ).checked = true;
      } else if (result.settings?.access_default) {
        document.querySelector(
          `#note-access-level input[value=${result.settings.access_default}]`,
        ).checked = true;
      } else {
        document.querySelector('#note-access-level input[value=private]').checked = true;
      }
    } catch {
      document.querySelector('#note-access-level input[value=private]').checked = true;
    }

    if (result.data.item_id) {
      document.getElementById('notes').dataset.noteId = result.data.item_id;

      if (result.data.access === 'public') {
        setPublishIcon();
      } else {
        removePublishIcon();
      }

      if (result.data.access === 'private') {
        setSavePrivatelyIcon();
      } else {
        removeSavePrivatelyIcon();
      }
    }

    const $noteTitleText = document.querySelector('#note-title-text');

    if (result.data.title) {
      $noteTitleText.textContent = result.data.title;
      $noteTitleText.setAttribute('data-is-set', '');
    } else {
      $noteTitleText.textContent = 'Add Note Title';
      $noteTitleText.removeAttribute('data-is-set');
    }

    setMsg('');
    quill.enable();
    quill.setText('');
    if (result.data && result.data.note > '') {
      // ref for using quill.clipboard.convert: https://stackoverflow.com/a/61482805
      const noteDelta = quill.clipboard.convert(DOMPurify.sanitize(result.data.note || ''));
      quill.setContents(noteDelta, 'api');
      noteLog = result.data.note;
    }
    quill.focus();
    quill.setSelection(quill.getLength());

    const $suggestedTagsWrap = document.getElementById('suggested-tags-wrap');
    if (!$suggestedTagsWrap.hasChildNodes()) {
      if (result.data.suggested_tags && result.data.suggested_tags.length > 0) {
        $suggestedTagsWrap.appendChild(
          utils.createSuggestedTags(result.data.suggested_tags, quill),
        );
      } else {
        $suggestedTagsWrap.appendChild(utils.createSuggestedTags(['toread', 'work'], quill));
      }
    }
    $suggestedTagsWrap.classList.remove('hide');

    if (result.data.recent_books && result.data.recent_books.length > 0) {
      if (recentBooks) {
        recentBooks.renderRecentBooks(result.data.recent_books);
      } else {
        recentBooks = utils.createRecentBooks(
          result.data.recent_books,
          saveNote,
          saveNoteDebounced,
          quill,
        );
        document.getElementById('recent-books').appendChild(recentBooks.element);
      }
    }

    if (dataExtractionResult) {
      renderExtractedData(dataExtractionResult);
    } else {
      renderExtractedData({ data: null });
    }

    if (result.data.note_modified) {
      document.getElementById('notes').dataset.noteModified = result.data.note_modified;
    } else {
      // Create a note if it doesn't exist
      saveNote();
    }

    if (result.data.read_later_book_id) {
      const $readLater = document.querySelector('#read-later');
      $readLater.setAttribute('data-read-later-book-id', result.data.read_later_book_id);
      $readLater.setAttribute('data-read-later-active', '');
      $readLater.querySelector('[data-read-later-icon]').classList.add('hide');
      $readLater.querySelector('[data-read-later-active-icon]').classList.remove('hide');
    }

    hideLoadingUI();

    chrome.storage.local.get(['featuresEnhanceWebsites'], (res) => {
      if (res.featuresEnhanceWebsites === true) {
        if (result.data.note_taken_from && result.data.note_taken_from.length > '') {
          // Tell user that note comes from another URL
          setMsg(
            `You made this note on a <a href="${result.data.note_taken_from}" target=”_blank” rel="noreferrer">different page</a> with the same content`,
            'addressable-msg',
            true,
          );
        }
      } else if (typeof res.featuresEnhanceWebsites === 'undefined') {
        setMsg('Enable "Enhance Websites" in settings to extract metadata');
      }
    });
  }
}

const handleResponse = (resp, errorMsg) => {
  if (!resp) {
    return;
  }

  if (resp.ok) {
    return resp.json();
  } else {
    throw new Error(errorMsg);
  }
};

const checkContentAddressableMapping = (url) =>
  contentAddressableMapping && contentAddressableMapping[url];

const updateContentAddressableMappingAndSetupNote = (
  currentUrl,
  currentPageContent,
  currentTabId,
) => {
  const reqPayload = {
    url: currentUrl,
    page_content: currentPageContent,
  };
  fetch(contentAddressableApiUrl, helper.setRequestParams('POST', reqPayload))
    .then((response) => {
      return handleResponse(
        response,
        'Error setting up notes. Please try closing the popup, refreshing the page, and re-opening the popup.',
      );
    })
    .then((result) => {
      if (result && result.data) {
        contentAddressableMapping = result.data;
      }
      isAddressableUrl = checkContentAddressableMapping(currentUrl);
      return runNoteSetup(currentTabId);
    })
    .catch((error) => {
      showErrorMessage(error.message);
    });
};

const setupContentAddressableNote = (currentTab) => {
  const currentUrl = currentTab.url;
  const currentTabId = currentTab.id;

  fetch(contentAddressableApiUrl, helper.setRequestParams())
    .then((response) => {
      return handleResponse(
        response,
        'Error setting up notes. Please try closing the popup, refreshing the page, and re-opening the popup.',
      );
    })
    .then((result) => {
      if (result.data) {
        contentAddressableMapping = result.data;
      }

      if (checkContentAddressableMapping(currentUrl)) {
        isAddressableUrl = true;
        return runNoteSetup(currentTabId);
      }

      // Insert current URL and content into content addressable table on server
      chrome.permissions.contains(
        {
          origins: ['*://*/*'],
        },
        (hasPermission) => {
          if (hasPermission) {
            chrome.tabs.sendMessage(currentTabId, { addressable: currentUrl }, (msg) => {
              let pageContent = null;
              if (msg !== undefined) {
                pageContent = msg.result;
              }
              return updateContentAddressableMappingAndSetupNote(
                currentUrl,
                pageContent,
                currentTabId,
              );
            });
          } else {
            return runNoteSetup(currentTabId);
          }
        },
      );
    })
    .catch((error) => {
      showErrorMessage(error.message);
    });
};

// Data extraction -- start
const runDataExtraction = async (sourceUrl, tabId) => {
  return new Promise((resolve) => {
    chrome.permissions.contains({ origins: ['*://*/*'] }, (enabled) => {
      if (chrome.runtime.lastError) {
      }

      if (enabled) {
        chrome.scripting.executeScript(
          { files: ['extractable.js'], target: { tabId } },
          (result) => {
            if (chrome.runtime.lastError) {
            }

            chrome.tabs.sendMessage(tabId, { extractable: sourceUrl }, (result) => {
              if (chrome.runtime.lastError) {
              }

              resolve(result?.result);
            });
          },
        );
      } else {
        resolve(null);
      }
    });
  });
};

const renderExtractedData = (extractableResult = {}) => {
  const { data, errorMsg, isExtractableSite } = extractableResult;

  if (isExtractableSite) {
    $('#re-extract-data').removeClass('hide');
  }

  if (errorMsg) {
    // Currently on page with extractable data but none were found
    setMsg(errorMsg);
  } else {
    dataTable.renderExtractedData(data);
  }
};

$('#re-extract-data').click(async () => {
  const dataExtractionResult = await runDataExtraction(decodeURIComponent(pageUrl), currentTabId);

  if (dataExtractionResult) {
    renderExtractedData(dataExtractionResult);
  } else {
    renderExtractedData({ data: null });
  }
});
// Data extraction -- end

const $savePrivately = document.querySelector('#save-privately');
const $savePrivatelyIcon = $savePrivately.querySelector('[data-save-privately-icon]');
const $savePrivatelyActiveIcon = $savePrivately.querySelector('[data-save-privately-active-icon]');

function setSavePrivatelyIcon() {
  removePublishIcon();
  $savePrivately.setAttribute('data-save-privately-active', '');
  $savePrivatelyIcon.classList.add('hide');
  $savePrivatelyActiveIcon.classList.remove('hide');
  document.querySelector('#note-access-level input[value=private]').checked = true;
}

function removeSavePrivatelyIcon() {
  $savePrivately.removeAttribute('data-save-privately-active');
  $savePrivatelyIcon.classList.remove('hide');
  $savePrivatelyActiveIcon.classList.add('hide');
}

$savePrivately.addEventListener('click', async (e) => {
  if (!$savePrivately.hasAttribute('data-save-privately-active')) {
    setSavePrivatelyIcon();
    displaySpinner();

    const result = await chrome.runtime.sendMessage({
      action: 'saveNote',
      data: { note: { access: 'public', url: pageUrl } },
    });

    if (result.error === false) {
      displayCheckmark();
    } else {
      displayAlert();
      setMsg(result.errorMessage || 'An error occurred', 'errmsg');
    }
  } else {
    const noteId = document.getElementById('notes').dataset.noteId;
    if (!noteId) return;

    chrome.storage.local.get('userUsername', ({ userUsername }) => {
      chrome.tabs.create({ url: `${host}/notes/${userUsername}/${noteId}/` });
    });
  }
});

const $publish = document.querySelector('#publish-note');
const $publishIcon = $publish.querySelector('[data-publish-icon]');
const $publishActiveIcon = $publish.querySelector('[data-publish-active-icon]');

function setPublishIcon() {
  removeSavePrivatelyIcon();
  $publish.setAttribute('data-publish-active', '');
  $publishIcon.classList.add('hide');
  $publishActiveIcon.classList.remove('hide');
  document.querySelector('#note-access-level input[value=public]').checked = true;
}

function removePublishIcon() {
  $publish.removeAttribute('data-publish-active');
  $publishIcon.classList.remove('hide');
  $publishActiveIcon.classList.add('hide');
}

$publish.addEventListener('click', async (e) => {
  if (!$publish.hasAttribute('data-publish-active')) {
    setPublishIcon();
    displaySpinner();

    const result = await chrome.runtime.sendMessage({
      action: 'saveNote',
      data: { note: { access: 'public', url: pageUrl } },
    });

    if (result.error === false) {
      displayCheckmark();
    } else {
      displayAlert();
      setMsg(result.errorMessage || 'An error occurred', 'errmsg');
    }
  } else {
    const noteId = document.getElementById('notes').dataset.noteId;
    if (!noteId) return;

    chrome.storage.local.get('userUsername', ({ userUsername }) => {
      chrome.tabs.create({ url: `${host}/notes/${userUsername}/${noteId}/` });
    });
  }
});

$('#read-later').click(function () {
  if (!this.hasAttribute('data-read-later-active')) {
    this.querySelector('[data-read-later-icon]').classList.add('hide');
    this.querySelector('[data-read-later-active-icon]').classList.remove('hide');
    saveNote({ readLater: true });
  } else {
    const bookId = this.getAttribute('data-read-later-book-id');
    const noteId = document.getElementById('notes').dataset.noteId;

    if (!bookId || !noteId) {
      return;
    }

    displaySpinner();
    this.querySelector('[data-read-later-icon]').classList.remove('hide');
    this.querySelector('[data-read-later-active-icon]').classList.add('hide');
    chrome.runtime.sendMessage(
      {
        action: 'removeNoteFromBooks',
        data: {
          data: {
            book_ids: [bookId],
            url_item_item_id: noteId,
          },
          url: decodeURIComponent(pageUrl),
          tabId: currentTabId,
          forReadLater: true,
        },
      },
      (response) => {
        if (response.error === false) {
          displayCheckmark();
          this.removeAttribute('data-read-later-active');
          recentBooks?.uncheckRecentBooks([bookId]);

          chrome.runtime.sendMessage({
            action: 'uncheckBooks',
            dest: 'sidePanel',
            bookIds: [bookId],
            url: decodeURIComponent(pageUrl),
          });

          chrome.runtime.sendMessage({
            action: 'removeReadLaterActive',
            dest: 'sidePanel',
            url: tab.url,
          });
        } else {
          displayAlert();
          setMsg(response.errorMessage || 'An error occurred', 'errmsg');
          this.querySelector('[data-read-later-icon]').classList.add('hide');
          this.querySelector('[data-read-later-active-icon]').classList.remove('hide');
        }
      },
    );
  }
});

document.addEventListener('DOMContentLoaded', function () {
  if (isDevMode) {
    document.getElementById('notes-debug').textContent = 'local';
  }

  // Setup data extraction table
  const $dataTableWrap = document.getElementById('extracted-data-table-wrap');
  $dataTableWrap.append(...dataTable.elements);

  document.getElementById('save-close').addEventListener('click', () => {
    const note = DOMPurify.sanitize(quill.root.innerHTML);
    saveNote({ note }, window.close);
  });

  $('body').on('keydown', (e) => {
    // Prevents ESC key from closing popup when pressed inside search/create box
    if (e.key === 'Escape') {
      e.preventDefault();
      if (e.target.hasAttribute('data-histre-book-input')) {
        return;
      } else {
        window.close();
      }
    }
  });

  chrome.tabs.query({ active: true, lastFocusedWindow: true }, (tabs) => {
    if (
      !tabs ||
      typeof tabs[0] === 'undefined' ||
      !tabs[0] ||
      !tabs[0].url ||
      tabs[0].url.indexOf('http') !== 0
    ) {
      showErrorMessage('No web page found on this tab');

      // Break note setup
      return;
    }

    const currentUrl = tabs[0].url;
    currentTabId = tabs[0].id;

    pageUrl = encodeURIComponent(tabs[0].url);
    pageTitle = tabs[0].title;

    if (currentUrl.startsWith('https://www.youtube.com/watch?')) {
      chrome.permissions.contains({ origins: ['https://www.youtube.com/'] }, (enabled) => {
        const $editorWrap = document.querySelector('#editor-container');
        if ($editorWrap.querySelector('.yt-timestamp')) {
          return;
        }

        if (enabled) {
          chrome.scripting.executeScript(
            { files: ['youtube-player-control.js'], target: { tabId: currentTabId } },
            () => {
              const ytTimestamp = utils.createYTTimestamp(currentTabId, currentUrl);
              $editorWrap.appendChild(ytTimestamp.element);
              $editorWrap
                .querySelector('#editor')
                .addEventListener('click', ytTimestamp.jumpToYoutubeTimestamp);
            },
          );
        } else {
          const $ytEnable = document.createElement('div');
          $ytEnable.setAttribute('class', 'yt-timestamp');
          $ytEnable.innerHTML = '<i class="fe fe-youtube"></i> Save current time';
          $ytEnable.addEventListener('click', function () {
            chrome.permissions.request({ origins: ['https://www.youtube.com/'] });
          });
          $editorWrap.appendChild($ytEnable);
          $editorWrap.querySelector('#editor').addEventListener('click', () => {
            chrome.permissions.request({ origins: ['https://www.youtube.com/'] });
          });
        }
      });
    }

    if (helper.isContentAddressable(tabs[0].url)) {
      setupContentAddressableNote(tabs[0]);
    } else {
      runNoteSetup(currentTabId);
    }
  });

  const bindings = {
    // "Cmd/Ctrl Enter" to save and close
    custom: {
      key: 13,
      shortKey: true,
      handler: function (range, context) {
        saveClose();
      },
    },
  };

  const mention = utils.getMentionConfig();

  quill = new Quill('#editor', {
    theme: 'snow',
    modules: {
      toolbar: false,
      keyboard: {
        bindings,
      },
      mention,
      magicUrl: true,
    },
  });
  utilsRefs.quill = quill;
  quill.disable();
  quill.on('text-change', function (delta, oldDelta, source) {
    if (source == 'api') {
      // pass
    } else if (source == 'user') {
      saveNoteDebounced(true);
    }
  });

  $('.nav-item').on('click', function (event) {
    event.preventDefault();

    if (disableNav) {
      return;
    }

    const el = $(this);
    const target = `#${el.data('contents-id')}`;

    if (target === '#settings') {
      if (!enableSettings) {
        return;
      }

      if (chrome.runtime.openOptionsPage) {
        chrome.runtime.openOptionsPage();
      } else {
        window.open(chrome.runtime.getURL('options.html'));
      }

      return;
    }

    if (target === '#search') {
      window.open(`${host}/search/`);
      return;
    }

    $('.nav-item .nav-link').removeClass('active');
    el.find('.nav-link').addClass('active');
    $('.tab-contents').hide();
    $(target).show();

    if (target === '#savetabs') {
      $('#savetabs-title').val(`Tabs at ${helper.friendlyTime()}`).select();
    } else if (target === '#notes') {
      quill.focus();
    }
  });

  const $noteTitle = document.querySelector('#note-title');
  const $noteTitleInput = document.querySelector('#note-title-input');
  const $noteTitleInputGroup = document.querySelector('#note-title-input-group');
  const $noteTitleCancelBtn = document.querySelector('#note-title-cancel-btn');
  const $noteTitleSaveBtn = document.querySelector('#note-title-save-btn');

  $noteTitle.addEventListener('click', () => {
    $noteTitle.classList.add('hide');
    $noteTitleInput.value = document.querySelector('#note-title-text').textContent;
    $noteTitleInputGroup.classList.remove('hide');
    $noteTitleInput.focus();
  });

  $noteTitleCancelBtn.addEventListener('click', () => {
    $noteTitleInputGroup.classList.add('hide');
    $noteTitle.classList.remove('hide');
    $noteTitleInput.classList.remove('input-error');
  });

  $noteTitleSaveBtn.addEventListener('click', async () => {
    if (!$noteTitleInput.value) {
      $noteTitleInput.classList.add('input-error');
      $noteTitleInput.focus();
      return;
    }

    $noteTitleInput.classList.remove('input-error');

    $noteTitleSaveBtn.disabled = true;
    $noteTitleCancelBtn.disabled = true;
    $noteTitleInput.disabled = true;

    await saveNote({ title: $noteTitleInput.value });

    $noteTitleInputGroup.classList.add('hide');
    $noteTitle.classList.remove('hide');
    $noteTitleSaveBtn.disabled = false;
    $noteTitleCancelBtn.disabled = false;
    $noteTitleInput.disabled = false;
  });

  $noteTitleInput.addEventListener('keydown', (e) => {
    $noteTitleInput.classList.remove('input-error');

    if (e.key === 'Enter') {
      $noteTitleSaveBtn.click();
    }
  });

  function checkIfNoteIsEmpty() {
    let isEmpty = true;

    if (quill.getText() !== '\n') {
      isEmpty = false;
    }

    if (document.querySelector('#extracted-data-table')?.textContent !== '') {
      isEmpty = false;
    }

    if (document.querySelector('#recent-books .fe-check-circle')) {
      isEmpty = false;
    }

    if (
      !document
        .querySelector('#read-later [data-read-later-active-icon]')
        ?.classList.contains('hide')
    ) {
      isEmpty = false;
    }

    return isEmpty;
  }

  $('#delete-note-btn').click((e) => {
    disableNav = true;

    if (checkIfNoteIsEmpty()) {
      document.querySelector('#notes').classList.add('hide');
      document.querySelector('#delete-note-spinner').classList.remove('hide');
      deleteNote({ fromModal: false });
    } else {
      document.querySelector('#notes').classList.add('hide');
      document.querySelector('#delete-note-confirm-alert').classList.add('hide');
      document.querySelector('#delete-note-confirm-error-msg').textContent = '';
      document.querySelector('#delete-note-confirm-spinner').classList.add('hide');
      document.querySelector('#delete-note-confirm').classList.remove('hide');
    }
  });

  $('#delete-note-confirm-cancel').click(() => {
    disableNav = false;
    $('#delete-note-confirm').addClass('hide');
    $('#notes').removeClass('hide');
    quill.focus();
  });

  $('#delete-note-confirm-delete').click(async function () {
    this.disabled = true;
    deleteNote({ fromModal: true });
  });

  $('#ask-ai-trigger-btn').click(async () => {
    if (userIsPremium !== false) {
      if (isFirefox) {
        chrome.permissions.contains({ origins: ['*://*/*'] }, (enabled) => {
          if (!enabled) {
            setMsg(
              'Please close the popup, and allow permissions request to use this functionality.',
            );
          }
        });
      }
      chrome.permissions.request({ origins: ['*://*/*'] }, (granted) => {
        if (granted) {
          const noteModified = document.getElementById('notes').dataset.noteModified || null;
          // Because there's no event that can be triggered when popup is closed, the best solution is
          // to create a connection to a runtime, and listen for onDisconnect event. Ref:
          // https://stackoverflow.com/questions/15798516/is-there-an-event-for-when-a-chrome-extension-popup-is-closed
          let connectionName = `popupAskAISwitch_${currentTabId}`;
          if (noteModified) {
            connectionName = `${connectionName}_${noteModified}`;
          }
          chrome.runtime.connect({ name: connectionName });
          window.close();
        }
      });
    } else {
      $('#notes').addClass('hide');
      $('#ask-ai-cta').removeClass('hide');
    }
  });

  $('#ask-ai-tab-close').click(() => {
    $('#ask-ai-cta').addClass('hide');
    $('#notes').removeClass('hide');
    quill.focus();
  });

  $('#ask-ai-cta').append(...utils.createAskAICta());

  document.getElementById('do-save-tabs-thiswin').addEventListener('click', (e) => {
    saveOpenTabs(true);
  });
  document.getElementById('do-save-tabs-allwin').addEventListener('click', (e) => {
    saveOpenTabs(false);
  });

  $('#savetabs-title').keydown((e) => {
    if (e.ctrlKey && e.key === 'Enter') {
      saveOpenTabs(true);
    }
  });

  document.getElementById('editor').addEventListener('click', (e) => {
    quill.focus();
  });
});

function local_save_log_account(value) {
  chrome.storage.local.set({ userLogHistory: value });
}

const handleUserTagUpdate = (storedTags, storedTagsLastModified, updateCacheFunc) => {
  fetchUserTags(
    {
      url: tagUrl,
      options: helper.setRequestParams(),
    },
    {
      storedTags,
      storedTagsLastModified,
      updateCacheFunc,
    },
  )
    .then((result) => {
      utilsRefs.tagList = convertTagsHashToList(result);
    })
    .catch((error) => {
      setMsg('Error fetching tags');
    });
};

const cacheUpdatedTags = (updatedTags, latestModified) => {
  chrome.storage.local.set({
    [USER_TAGS_CACHE_KEY]: updatedTags,
    [USER_LAST_TAG_MODIFIED_KEY]: latestModified,
  });
};

function updateTagList() {
  chrome.storage.local.get([USER_TAGS_CACHE_KEY, USER_LAST_TAG_MODIFIED_KEY], (result) => {
    const storedTags = result[USER_TAGS_CACHE_KEY] || {};
    const storedTagsLastModified = result[USER_LAST_TAG_MODIFIED_KEY] || null;
    handleUserTagUpdate(storedTags, storedTagsLastModified, cacheUpdatedTags);
  });
}

document.querySelectorAll('[data-vote]').forEach((voteBtn) => {
  voteBtn.addEventListener('click', async function () {
    const payload = {
      url: decodeURIComponent(pageUrl),
      title: pageTitle,
    };

    const voteUI = document.querySelector('#vote-ui');

    if (this.dataset.vote === 'up') {
      payload.vote = 1;
      this.classList.add('hide');
      voteUI.querySelector('.thumbs-up-active').classList.remove('hide');
    } else {
      payload.vote = 0;
      this.classList.add('hide');
      document.querySelector('#vote-ui .thumbs-up').classList.remove('hide');
    }

    displaySpinner();

    try {
      const result = await sendRuntimeMessage({
        action: 'saveVote',
        data: payload,
      });

      if (result.error) {
        displayAlert();
      } else {
        displayCheckmark();
      }
    } catch (error) {
      displayAlert();
    }
  });
});

updateTagList();

function onError(error) {
  helper.llog('onError', error);
}

const $saveTabsSaveThisWindowBtn = document.querySelector('#do-save-tabs-thiswin');
const $saveTabsSaveAllWindowsBtn = document.querySelector('#do-save-tabs-allwin');
const $saveTabsTitleInput = document.querySelector('#savetabs-title');
const $saveTabsSavingSpinner = document.querySelector('#savetabs-saving-spinner');
const $saveTabsSavingCheckmark = document.querySelector('#savetabs-saving-checkmark');
const $saveTabsSavingAlert = document.querySelector('#savetabs-saving-alert');
const $saveTabsErrorMsg = document.querySelector('#save-tabs-msg');

function startSaveTabsLoadingUI() {
  $saveTabsSaveThisWindowBtn.disabled = true;
  $saveTabsSaveAllWindowsBtn.disabled = true;
  $saveTabsTitleInput.disabled = true;
  $saveTabsSavingCheckmark.classList.add('hide');
  $saveTabsSavingAlert.classList.add('hide');
  $saveTabsSavingSpinner.classList.remove('hide');
  $saveTabsErrorMsg.innerText = '';
  $saveTabsErrorMsg.classList.add('hide');
}

function displaySaveTabsCheckmark() {
  $saveTabsSavingSpinner.classList.add('hide');
  $saveTabsSavingCheckmark.classList.remove('hide');
  $saveTabsSavingAlert.classList.add('hide');
  $saveTabsSaveThisWindowBtn.disabled = false;
  $saveTabsSaveAllWindowsBtn.disabled = false;
  $saveTabsTitleInput.disabled = false;
}

function displaySaveTabsError(msg) {
  $saveTabsSavingSpinner.classList.add('hide');
  $saveTabsSavingAlert.classList.remove('hide');
  $saveTabsSaveThisWindowBtn.disabled = false;
  $saveTabsSaveAllWindowsBtn.disabled = false;
  $saveTabsTitleInput.disabled = false;

  if (msg) {
    $saveTabsErrorMsg.innerText = msg;
    $saveTabsErrorMsg.classList.remove('hide');
  }
}

function saveOpenTabs(current) {
  startSaveTabsLoadingUI();

  let tabsInfo = {};
  const queryOptions = { url: ['http://*/*', 'https://*/*'] };
  const bookTitle = $('#savetabs-title').val().trim();

  if (current) {
    queryOptions.currentWindow = true;
  }
  chrome.tabs.query(queryOptions, (tabs, onError) => {
    tabs.forEach((tab) => {
      const { windowId } = tab;
      if (typeof tabsInfo[windowId] !== 'object') {
        tabsInfo[windowId] = [];
      }
      tabsInfo[windowId].push({ url: tab.url, title: tab.title });
    });
    helper.llog(tabsInfo);

    var data = {
      items: tabsInfo,
      friendly_time: helper.friendlyTime(),
      title: bookTitle,
    };

    fetch(tabSaveUrl, helper.setRequestParams('POST', data))
      .then((response) => {
        return response.json();
      })
      .then((result) => {
        const {
          data: { url },
          error,
          errcode,
        } = result;
        if (error) {
          if (helper.isAuthErrorStatus(errcode)) {
            helper.handleAuthErrorsOnce(() => {
              displaySaveTabsError('Not authenticated');
              return;
            });
          } else {
            throw new Error('Error saving tabs');
          }
        } else {
          displaySaveTabsCheckmark();

          chrome.storage.local.get(['featuresInline'], (result) => {
            if (result.featuresInline === true) {
              tabs.forEach((tab) => {
                chrome.tabs.sendMessage(tab.id, { action: 'setNoteIsPresent', url: tab.url });
                helper.setNoteIsPresentIcon(tab.id, tab.url);
              });
            }
          });
          return url;
        }
      })
      .then((url) => {
        if (url) {
          const creating = chrome.tabs.create({ url: url }, (entry) => {
            if (chrome.runtime.lastError) {
              setMsg(`Saved tabs, but unable to open that page: ${url}`, 'errmsg');
            } else {
              window.close();
            }
          });
        }
      })
      .catch((error) => {
        displaySaveTabsError('Error saving tabs. Please email support@histre.com for help.');
      });
  });
}
