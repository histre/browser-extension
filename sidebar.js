import { apiBase } from '/globals.js';
import { helper } from '/helper.js';
import {
  USER_TAGS_CACHE_KEY,
  USER_LAST_TAG_MODIFIED_KEY,
  convertTagsHashToList,
  fetchUserTags,
} from '/tag-utils.js';

const tagUrl = `${apiBase}/tag/`;

const histreDOMHelper = window.histreDOMHelper;
const { $e, $t, noEvents } = histreDOMHelper;
const prefix = (classNames) => histreDOMHelper.prefix('sidebar', classNames);
const host = window.histreDOMHelper.getHost();

const utilsRefs = {
  quill: null,
  saveNote,
  saveNoteDebounced,
  tagList: null,
};
const utils = new window.histreUtils('sidebar', utilsRefs);
const dataTable = utils.createExtractedDataTable();
const votes = utils.createVoteUI({ displaySpinner });
const askAI = utils.createAskAI({ showMainContent });
let youtubeTimestamp = null;

const authErrorMessage = 'Unable to authenticate with histre.com';
const permissionErrorMessage = 'Host permissions missing';

const sidebarWidth = 400;

let pageUrl;
let pageTitle;

let quill;

let noteLog = '';
let noteAutosaveTimeout;

let recentBooks = null;

let tagListUpdateTimeout;
const tagListUpdateDelay = 1000;

let userIsPremium = false;

//----- DOMPurify setup -----//

DOMPurify.addHook('afterSanitizeAttributes', (node) => {
  // allow target="_blank"
  if ('target' in node) {
    node.setAttribute('target', '_blank');
    node.setAttribute('rel', 'noopener noreferrer');
  }
});

//----- Messages -----//

function onMessageHandler(message, sender, sendResponse) {
  if (message.dest === 'sidePanel') {
    if (message.action === 'loading') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        startLoadingUI();
      })();
      return;
    }

    if (message.action === 'displayCheck') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        finishLoadingUI('', 'check');
      })();
      return;
    }

    if (message.action === 'displayError') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        finishLoadingUI(message.message || 'An error occurred', 'error');
      })();
      return;
    }

    if (message.action === 'displaySetupError') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        finishLoadingUI(message.message || 'An error occurred', 'setupError');
      })();
      return;
    }

    if (message.action === 'updateVoteSidebar') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        votes.updateVote(message.vote);
      })();
      return;
    }

    if (message.action === 'setupNote') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        const { result, tab } = message;

        if (tab.url.indexOf('http') !== 0) {
          showSetupErrorMsg('No web page found on this tab');
          return;
        }

        if (!result.error) {
          const { data, settings, tags, dataExtraction } = result;

          if (data.item_id) {
            $wrap.dataset.histreNoteId = data.item_id;
          }

          if (data.note_modified) {
            $wrap.dataset.histreNoteModified = data.note_modified;
          }

          if (data.title) {
            $noteTitleText.textContent = data.title;
            $noteTitleText.setAttribute('data-is-set', '');
          } else {
            $noteTitleText.textContent = 'Add Note Title';
            $noteTitleText.removeAttribute('data-is-set');
          }

          if (result.data.read_later_book_id) {
            setReadLaterActive();
            $readLater.setAttribute(
              'data-histre-read-later-book-id',
              result.data.read_later_book_id,
            );
          } else {
            removeReadLaterActive();
          }

          if (typeof settings.premium_user !== 'undefined') {
            userIsPremium = settings.premium_user;
          }

          votes.updateVote(data.vote);

          quill.setText('');
          if (data.note > '') {
            // ref for using quill.clipboard.convert: https://stackoverflow.com/a/61482805
            const noteDelta = quill.clipboard.convert(DOMPurify.sanitize(data.note));
            quill.setContents(noteDelta, 'api');
          }
          noteLog = data.note;

          if (!$noteAccessLevels.hasChildNodes()) {
            if (settings?.access_levels) {
              $noteAccessLevels.appendChild(
                utils.createAccessLevels(result.settings.access_levels, saveNote, quill),
              );
            } else {
              $noteAccessLevels.appendChild(
                utils.createAccessLevels([['private', 'Private']], saveNote, quill),
              );
            }
          }
          try {
            if (data.access) {
              $noteAccessLevels.querySelector(`input[value=${data.access}]`).checked = true;
            } else if (settings?.access_default) {
              $noteAccessLevels.querySelector(
                `input[value=${settings.access_default}]`,
              ).checked = true;
            } else {
              $noteAccessLevels.querySelector('input[value=private]').checked = true;
            }
          } catch {
            $noteAccessLevels.querySelector('input[value=private]').checked = true;
          }

          if (data.item_id && data.access === 'public') {
            setPublishIcon();
          } else {
            removePublishIcon();
          }

          if (!$suggestedTagsWrap.hasChildNodes()) {
            if (data.suggested_tags && data.suggested_tags.length > 0) {
              $suggestedTagsWrap.appendChild(utils.createSuggestedTags(data.suggested_tags, quill));
            } else {
              $suggestedTagsWrap.appendChild(utils.createSuggestedTags(['toread', 'work'], quill));
            }
          }

          if (result.data.recent_books && result.data.recent_books.length > 0) {
            if (recentBooks) {
              recentBooks.renderRecentBooks(result.data.recent_books);
            } else {
              recentBooks = utils.createRecentBooks(
                result.data.recent_books,
                saveNote,
                saveNoteDebounced,
                quill,
              );
              $recentBooksWrap.appendChild(recentBooks.element);
            }
          }

          if (dataExtraction) {
            renderExtractedData(dataExtraction);
          } else {
            renderExtractedData({ data: null });
          }

          finishLoadingUI();
        } else {
          finishLoadingUI(result.errmsg || 'An error occured', 'error');
        }

        pageUrl = encodeURIComponent(tab.url);
        pageTitle = tab.title;

        if (tab.url.startsWith('https://www.youtube.com/watch?')) {
          setupYoutubeWatch(tab);
        } else {
          removeYoutubeWatch();
        }
      })();
      return;
    }

    if (message.action === 'updateNote') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        const { data } = message;

        if (data.noteId) {
          $wrap.dataset.histreNoteId = data.noteId;
        }

        if (data.noteModified) {
          $wrap.dataset.histreNoteModified = data.noteModified;
        }

        if (data.title) {
          $noteTitleText.textContent = data.title;
        }

        quill.setText('');
        if (data.note > '') {
          const noteDelta = quill.clipboard.convert(DOMPurify.sanitize(data.note));
          quill.setContents(noteDelta, 'api');
        }
        noteLog = data.note;

        if (data.accessLevel) {
          $noteAccessLevels.querySelector(`input[value=${data.accessLevel}]`).checked = true;
        }

        if (data.bookIds || (data.newBook?.id && data.newBook?.title)) {
          recentBooks?.updateRecentBooks({
            checkedIds: data.bookIds,
            noteId: data.noteId,
            newBook: data.newBook,
          });
        }

        if (data.attrs) {
          renderExtractedData({ data: data.attrs });
        } else {
          renderExtractedData({ data: null });
        }

        if (data.readLaterBookId) {
          setReadLaterActive();
          $readLater.setAttribute('data-histre-read-later-book-id', data.readLaterBookId);
          recentBooks?.checkRecentBooks({ bookIds: [data.readLaterBookId], itemId: data.noteId });
        }
      })();
      return;
    }

    if (message.action === 'deleteNote') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        $wrap.dataset.histreNoteId = '';
        $wrap.dataset.histreNoteModified = '';

        quill.setText('');
        noteLog = '';

        chrome.storage.local.get('userAccessDefault', (result) => {
          if (result.userAccessDefault) {
            $noteAccessLevels.querySelector(
              `input[value=${result.userAccessDefault}]`,
            ).checked = true;
          }
        });

        recentBooks?.uncheckRecentBooks();

        renderExtractedData({ data: null });

        votes.updateVote(0);

        removeReadLaterActive();
      })();
      return;
    }

    if (message.action === 'appendTextToNote') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        const { data } = message;

        if (data.noteId) {
          $wrap.dataset.histreNoteId = data.noteId;
        }

        if (data.noteModified) {
          $wrap.dataset.histreNoteModified = data.noteModified;
        }

        if (data.title) {
          $noteTitleText.textContent = data.title;
        }

        if (data.note > '') {
          if (quill.getText() === '\n') {
            quill.setText(data.note);
          } else {
            quill.insertText(quill.getLength(), `\n\n${data.note}`);
          }
        }
        noteLog = quill.root.innerHTML;
      })();
      return;
    }

    if (message.action === 'noteSaved') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        noteLog = message.note;
        $wrap.dataset.histreNoteId = message.noteId;
        $wrap.dataset.histreNoteModified = message.noteModified;

        if (message.readLaterBook) {
          $readLater.setAttribute('data-histre-read-later-book-id', message.readLaterBook);
          recentBooks?.checkRecentBooks({
            bookIds: [message.readLaterBook],
            itemId: message.noteId,
          });
        }

        if (!$noteTitleText.hasAttribute('data-is-set')) {
          $noteTitleText.textContent = message.note.title;
          $noteTitleText.setAttribute('data-is-set', '');
        }

        if (message.note.user_title) {
          $noteTitleText.textContent = message.note.user_title;
          $noteTitleText.setAttribute('data-is-set', '');
        }
      })();
      return;
    }

    if (message.action === 'getUpdatedSidebarNote') {
      getUpdatedSidebarNote();
      return;
    }

    if (message.action === 'reCheckBooks') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        recentBooks?.reCheckBooks(message.bookIds);
      })();
      return;
    }

    if (message.action === 'uncheckBooks') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        recentBooks?.uncheckRecentBooks(message.bookIds);
      })();
      return;
    }

    if (message.action === 'enableNewBookActions') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        recentBooks?.enableNewBookActions();
      })();
      return;
    }

    if (message.action === 'enableNewBook') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        if (message.newBookId) {
          recentBooks?.enableNewBook(message.newBookId);
        }
      })();
      return;
    }

    if (message.action === 'closeRecentBooks') {
      recentBooks?.hideRecentBooks();
      return;
    }

    if (message.action === 'showRecentBooks') {
      recentBooks?.showRecentBooks();
      return;
    }

    if (message.action === 'focusEditor') {
      // It's meant to close "Ask AI" UI first if it's open, and then focusEditor() function
      // will be called in the chain.
      askAI.hideAskAI();
      return;
    }

    if (message.action === 'renderExtractedData') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        renderExtractedData(message.dataExtraction);
      })();
      return;
    }

    if (message.action === 'setAccessLevel') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        setAccessLevel(message.accessLevel);
      })();
      return;
    }

    if (message.action === 'setSidebarReadLater') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        setReadLaterActive();
        $readLater.setAttribute('data-histre-read-later-book-id', message.bookId);
      })();
      return;
    }

    if (message.action === 'removeReadLaterActive') {
      (async () => {
        if (!(await checkUrl(message.url))) return;

        removeReadLaterActive();
      })();
      return;
    }
  }
}

async function tabActivatedHandler(activeInfo) {
  const tab = await chrome.tabs.get(activeInfo.tabId);

  if (tab.url.indexOf('http') !== 0) {
    showSetupErrorMsg('No web page found on this tab');
    return;
  }

  getUpdatedSidebarNote();
}

function getUpdatedSidebarNote() {
  startLoadingUI();

  clearTimeout(tagListUpdateTimeout);
  tagListUpdateTimeout = setTimeout(() => {
    updateTagList();
  }, tagListUpdateDelay);

  chrome.runtime.sendMessage({ action: 'getSidebarNote', dest: 'sidebar' });
}

function onStorageChangedHandler(changes) {
  if (changes.userIsPremium) {
    userIsPremium = changes.userIsPremium.newValue;
  }
}

//----- Save note -----//

async function saveNote({ note, readLater, title } = {}) {
  if (typeof note !== 'string') {
    note = DOMPurify.sanitize(quill.root.innerHTML);
  }

  const accessLevel = $noteAccessLevels.querySelector('input:checked').value;
  const bookIds = recentBooks?.getBookIdsForAddNote() || null;
  const deselectedBookIds = recentBooks?.getDeselectedBookIds() || null;
  const newBookTitle = recentBooks?.getNewBookTitle() || null;
  const noteId = $wrap.dataset.histreNoteId || null;
  const noteModified = $wrap.dataset.histreNoteModified || null;
  const message = { action: 'saveSidebarNote', dest: 'sidebar' };

  // Get title again, because sometimes tab title is set before it's loaded
  const tab = (await chrome.tabs.query({ active: true, lastFocusedWindow: true }))[0];
  if (tab?.title) {
    pageTitle = tab.title;
  }

  const saveNoteRequestPayload = {
    url: pageUrl,
    title: pageTitle,
    note: note,
    book_ids: bookIds,
    access: accessLevel,
    note_modified: noteModified,
  };

  const tableValues = dataTable.getTableData();

  if (tableValues) {
    saveNoteRequestPayload['extracted_attrs'] = tableValues;
  }
  if (readLater) {
    saveNoteRequestPayload.read_later = true;
  }
  if (title) {
    saveNoteRequestPayload.user_title = title;
  }

  message.data = saveNoteRequestPayload;

  if (deselectedBookIds?.length !== 0) {
    message.removeNoteFromBooks = deselectedBookIds;
  }

  if (newBookTitle) {
    message.createBook = { title: newBookTitle };
  }

  message.noteId = noteId;

  displaySpinner();

  await chrome.runtime.sendMessage(message);
}

function saveNoteDebounced(ifChanged = false) {
  clearTimeout(noteAutosaveTimeout);

  noteAutosaveTimeout = setTimeout(() => {
    const note = DOMPurify.sanitize(quill.root.innerHTML);

    if (ifChanged) {
      if (note !== noteLog) {
        saveNote({ note });
      }
    } else {
      saveNote({ note });
    }
  }, 800);
}

//----- Status messages -----//

function displaySpinner() {
  $checkmark.style.display = 'none';
  $alert.style.display = 'none';
  $spinner.style.display = 'inline-block';
}

function displayCheck() {
  $spinner.style.display = 'none';
  $checkmark.style.display = 'block';
}

function displayAlert() {
  $spinner.style.display = 'none';
  $alert.style.display = 'block';
}

function startLoadingUI() {
  askAI.hideAndResetAskAI();
  $wrap.classList.remove(prefix('hide'));
  $authError.classList.add('hide');
  $permissionError.classList.add('hide');
  $body.classList.add(prefix('hide'));
  $footer.classList.add(prefix('hide'));
  $setupErrorMsg.classList.add(prefix('hide'));
  $setupErrorMsgText.textContent = '';
  $loadingUI.classList.remove(prefix('hide'));
  $statusMessage.style.display = 'none';
  $statusMessage.textContent = '';
  removeReadLaterActive();
}

function finishLoadingUI(message = '', type = '', isFaded = false) {
  $statusMessage.textContent = message;
  $statusMessage.style.display = message ? 'block' : 'none';

  $loadingUI.classList.add(prefix('hide'));

  if (type === 'setupError') {
    chrome.permissions.contains(
      {
        origins: [`${host.replace(/:(\d+)/, '')}/` /* remove port number */],
      },
      (hasPermission) => {
        if (!hasPermission) {
          showSetupErrorMsg(permissionErrorMessage);
          return;
        }
        showSetupErrorMsg(message);
      },
    );
    return;
  }

  $body.classList.remove(prefix('hide'));
  $footer.classList.remove(prefix('hide'));
  if (type === 'error') {
    $statusMessage.classList.add(prefix('status-msg--error'));
    displayAlert();
  } else if (type === 'check') {
    $statusMessage.classList.remove(prefix('status-msg--error'));
    displayCheck();
  } else {
    $statusMessage.classList.remove(prefix('status-msg--error'));
    $spinner.style.display = 'none';
    $checkmark.style.display = 'none';
    $alert.style.display = 'none';
  }

  if (isFaded) {
    setTimeout(() => {
      $statusMessage.style.display = 'none';
      $statusMessage.textContent = '';
      $statusMessage.classList.remove(prefix('status-msg--error'));
      $checkmark.style.display = 'none';
      $alert.style.display = 'none';
    }, 2000);
  }
}

function showSetupErrorMsg(msg) {
  $body.classList.add(prefix('hide'));
  $footer.classList.add(prefix('hide'));
  $loadingUI.classList.add(prefix('hide'));

  if (msg === authErrorMessage) {
    $wrap.classList.add(prefix('hide'));
    $authError.classList.remove('hide');
    return;
  }

  if (msg === permissionErrorMessage) {
    $wrap.classList.add(prefix('hide'));
    $permissionError.classList.remove('hide');
    return;
  }

  $authError.classList.add('hide');
  $permissionError.classList.add('hide');

  $setupErrorMsg.classList.remove(prefix('hide'));
  $setupErrorMsgText.textContent = msg || 'An error occurred';
}

//----- Data extraction -----//

function reExtractData() {
  chrome.runtime.sendMessage({ action: 'reExtractData', dest: 'sidebar' });
}

function renderExtractedData(extractableResult = {}) {
  const { data, errorMsg, isExtractableSite } = extractableResult;

  if (isExtractableSite) {
    $reExtractDataBtn.style.display = 'flex';
  }

  if (errorMsg) {
    // Currently on page with extractable data but none were found
    dataTable.renderExtractedData(null);
  } else {
    dataTable.renderExtractedData(data);
  }
}

//----- Save privately -----//

function handleSavePrivatelyBtn() {
  if (!this.classList.contains(prefix('save-privately--saved'))) {
    setSavePrivatelyIcon();
    displaySpinner();
    chrome.runtime.sendMessage({
      action: 'saveNote',
      data: { note: { access: 'private', url: pageUrl } },
    });
  } else {
    const noteId = $wrap.getAttribute('data-histre-note-id');
    if (!noteId) return;

    chrome.storage.local.get('userUsername', ({ userUsername }) => {
      chrome.tabs.create({ url: `${host}/notes/${userUsername}/${noteId}/` });
    });
  }
}

function setSavePrivatelyIcon() {
  $savePrivately.classList.add(prefix('save-privately--saved'));
  $noteAccessLevels.querySelector(`input[value=private]`).checked = true;
  removePublishIcon();
}

function removeSavePrivatelyIcon() {
  $savePrivately.classList.remove(prefix('save-privately--saved'));
}

//----- Publish note -----//

function handlePublishBtn() {
  if (this.hasAttribute('disabled')) {
    return;
  }

  if (!this.classList.contains(prefix('publish--published'))) {
    setPublishIcon();
    displaySpinner();
    chrome.runtime.sendMessage({
      action: 'saveNote',
      data: { note: { access: 'public', url: pageUrl } },
    });
  } else {
    const noteId = $wrap.getAttribute('data-histre-note-id');
    if (!noteId) return;

    chrome.storage.local.get('userUsername', ({ userUsername }) => {
      chrome.tabs.create({ url: `${host}/notes/${userUsername}/${noteId}/` });
    });
  }
}

function setPublishIcon() {
  removeSavePrivatelyIcon();
  $publish.classList.add(prefix('publish--published'));
  $noteAccessLevels.querySelector(`input[value=public]`).checked = true;
}

function removePublishIcon() {
  $publish.classList.remove(prefix('publish--published'));
}

//----- Read later -----//

function toggleReadLater() {
  if (this.hasAttribute('disabled')) {
    return;
  }

  if (!this.classList.contains(prefix('read-later--saved'))) {
    setReadLaterActive();
    saveNote({ readLater: true });
  } else {
    removeNoteFromReadLater();
  }
}

function removeNoteFromReadLater(cb) {
  const bookId = $readLater.getAttribute('data-histre-read-later-book-id');
  const noteId = $wrap.dataset.histreNoteId || null;

  if (!bookId || !noteId) {
    if (typeof cb === 'function') cb({ error: true });
    return;
  }

  displaySpinner();
  removeReadLaterActive();
  chrome.runtime.sendMessage(
    {
      action: 'removeNoteFromBooks',
      data: {
        data: {
          book_ids: [bookId],
          url_item_item_id: noteId,
        },
        forReadLater: true,
      },
    },
    (response) => {
      if (response.error === false) {
        displayCheck();
        recentBooks?.uncheckRecentBooks([bookId]);
      } else {
        setReadLaterActive();
        finishLoadingUI(response.errorMessage || 'An error occurred', 'error');
      }

      if (typeof cb === 'function') cb(response);
    },
  );
}

function setReadLaterActive() {
  $readLater.classList.add(prefix('read-later--saved'));

  const readLaterBookId = $readLater.getAttribute('data-histre-read-later-book-id');
  const noteId = $wrap.getAttribute('data-histre-note-id');
  if (readLaterBookId && noteId) {
    recentBooks?.checkRecentBooks({
      bookIds: [readLaterBookId],
      itemId: noteId,
    });
  }
}

function removeReadLaterActive() {
  $readLater.classList.remove(prefix('read-later--saved'));

  const readLaterBookId = $readLater.getAttribute('data-histre-read-later-book-id');
  if (readLaterBookId) {
    recentBooks?.uncheckRecentBooks([readLaterBookId]);
  }
}

//----- Youtube watch pages -----//

async function setupYoutubeWatch(tab) {
  const hasPermission = await chrome.permissions.contains({
    origins: ['https://www.youtube.com/'],
  });

  if (hasPermission) {
    await chrome.scripting.executeScript({
      files: ['youtube-player-control.js'],
      target: { tabId: tab.id },
    });
  }

  if (youtubeTimestamp) {
    $editor.removeEventListener('click', youtubeTimestamp.jumpToYoutubeTimestamp);
    $editorContainer.querySelector(`#${prefix('yt-timestamp')}`)?.remove();
  }

  youtubeTimestamp = utils.createYTTimestamp(tab.id, tab.url, hasPermission);
  $editorContainer.appendChild(youtubeTimestamp.element);
  $editor.addEventListener('click', youtubeTimestamp.jumpToYoutubeTimestamp);
}

function removeYoutubeWatch() {
  if (!youtubeTimestamp) return;

  $editorContainer.querySelector(`#${prefix('yt-timestamp')}`)?.remove();
  $editor.removeEventListener('click', youtubeTimestamp.jumpToYoutubeTimestamp);
  youtubeTimestamp = null;
}

//----- Ask AI -----//

async function showAskAI() {
  if (userIsPremium) {
    const hasPermissions = await chrome.permissions.request({ origins: ['*://*/*'] });
    if (hasPermissions) {
      $mainContent.classList.add(prefix('hide'));
      askAI.showAskAI(userIsPremium);
    }
  } else {
    $mainContent.classList.add(prefix('hide'));
    askAI.showAskAI(userIsPremium);
  }
}

//----- Tags -----//

function handleUserTagUpdate(storedTags, storedTagsLastModified, updateCacheFunc) {
  fetchUserTags(
    {
      url: tagUrl,
      options: helper.setRequestParams(),
    },
    {
      storedTags,
      storedTagsLastModified,
      updateCacheFunc,
    },
  )
    .then((result) => {
      utilsRefs.tagList = convertTagsHashToList(result);
    })
    .catch((error) => {
      setMsg('Error fetching tags');
    });
}

function cacheUpdatedTags(updatedTags, latestModified) {
  chrome.storage.local.set({
    [USER_TAGS_CACHE_KEY]: updatedTags,
    [USER_LAST_TAG_MODIFIED_KEY]: latestModified,
  });
}

function updateTagList() {
  chrome.storage.local.get([USER_TAGS_CACHE_KEY, USER_LAST_TAG_MODIFIED_KEY], (result) => {
    const storedTags = result[USER_TAGS_CACHE_KEY] || {};
    const storedTagsLastModified = result[USER_LAST_TAG_MODIFIED_KEY] || null;
    handleUserTagUpdate(storedTags, storedTagsLastModified, cacheUpdatedTags);
  });
}

//----- Uncategorized Functions -----//

function setAccessLevel(accessLevel) {
  $noteAccessLevels.querySelector(`input[value=${accessLevel}]`).checked = true;

  if (accessLevel === 'public') {
    setPublishIcon();
  } else {
    removePublishIcon();
  }

  if (accessLevel === 'private') {
    setSavePrivatelyIcon();
  } else {
    removeSavePrivatelyIcon();
  }
}

function showMainContent() {
  $mainContent.classList.remove(prefix('hide'));
  focusEditor();
}

function focusEditor() {
  quill?.focus();
  quill?.setSelection(quill?.getLength());
}

function openSearch() {
  window.open(host + '/search/');
}

function openSettings() {
  chrome.tabs.create({ url: chrome.runtime.getURL('options.html') });
}

function openHistre() {
  window.open(host);
}

function removeURLFragment(url) {
  const urlObj = new URL(url);
  return urlObj.origin + urlObj.pathname + urlObj.search;
}

async function checkUrl(url) {
  if (!url) {
    return true;
  }
  const activeTab = (await chrome.tabs.query({ active: true, lastFocusedWindow: true }))[0];
  return removeURLFragment(activeTab.url) === removeURLFragment(url);
}

//----- Create UI -----//

// Note title

function handleNoteTitleClick() {
  $noteTitle.classList.add(prefix('hide'));
  $noteTitleInput.value = $noteTitleText.textContent;
  $noteTitleInputGroup.classList.remove(prefix('hide'));
  $noteTitleInput.focus();
}

function handleNoteTitleCancelBtn() {
  $noteTitleInputGroup.classList.add(prefix('hide'));
  $noteTitle.classList.remove(prefix('hide'));
  $noteTitleInput.classList.remove(prefix('input-error'));
}

async function handleNoteTitleSaveBtn() {
  if (!$noteTitleInput.value) {
    $noteTitleInput.classList.add(prefix('input-error'));
    $noteTitleInput.focus();
    return;
  }

  $noteTitleInput.classList.remove(prefix('input-error'));

  $noteTitleSaveBtn.disabled = true;
  $noteTitleCancelBtn.disabled = true;
  $noteTitleInput.disabled = true;

  await saveNote({ title: $noteTitleInput.value });

  $noteTitleInputGroup.classList.add(prefix('hide'));
  $noteTitle.classList.remove(prefix('hide'));
  $noteTitleSaveBtn.disabled = false;
  $noteTitleCancelBtn.disabled = false;
  $noteTitleInput.disabled = false;
}

function handleNoteTitleInputKeyDown(e) {
  $noteTitleInput.classList.remove(prefix('input-error'));

  if (e.key === 'Enter') {
    $noteTitleSaveBtn.click();
  }
}

const $noteTitleText = $e('span', { class: prefix('note-title-text text-truncate') }, noEvents, [
  $t('Add a note title'),
]);
const $noteTitle = $e(
  'div',
  { class: prefix('note-title text-truncate') },
  { click: handleNoteTitleClick },
  [$noteTitleText, $e('i', { class: 'fe fe-edit-2 text-muted' })],
);

const $noteTitleInput = $e('input', {
  type: 'text',
  class: prefix('form-control form-control-sm'),
  autocomplete: 'off',
});
const $noteTitleCancelBtn = $e(
  'button',
  { class: prefix('btn btn-sm btn-secondary') },
  { click: handleNoteTitleCancelBtn },
  [$e('i', { class: `fe fe-x ${prefix('title-input-btns')}` })],
);
const $noteTitleSaveBtn = $e(
  'button',
  { class: prefix('btn btn-sm btn-primary') },
  { click: handleNoteTitleSaveBtn },
  [$e('i', { class: `fe fe-check ${prefix('title-input-btns')}` })],
);
const $noteTitleInputGroup = $e(
  'div',
  { class: prefix('input-group note-title-input-group hide') },
  { keydown: handleNoteTitleInputKeyDown },
  [$noteTitleInput, $noteTitleCancelBtn, $noteTitleSaveBtn],
);

const $noteAccessLevels = $e('div', { class: prefix('access-levels') });
const $editorLabel = $e('div', { class: prefix('editor-label') }, noEvents, [
  $noteTitle,
  $noteTitleInputGroup,
  $noteAccessLevels,
]);

// Tags

const $suggestedTagsWrap = $e('div', { class: prefix('suggested-tags-wrap') });

// Editor

const $editor = $e('div', { id: prefix('editor') });
const $editorWrap = $e('div', { class: prefix('editor-wrap') }, noEvents, [
  $editor,
  $suggestedTagsWrap,
]);
const $editorContainer = $e('div', { class: prefix('editor-conteiner') }, noEvents, [
  $editorLabel,
  $editorWrap,
]);

if (location.hostname === 'www.youtube.com') {
  $editorWrap.addEventListener('keydown', (e) => {
    e.stopPropagation();
  });
}

// Data extraction

const $reExtractDataBtn = $e(
  'div',
  { class: prefix('re-extract-data') },
  {
    click: reExtractData,
  },
  [
    $e('span', { class: prefix('extracted-data-icon extracted-data-icon--refresh') }),
    $t('Re-extract Data'),
  ],
);
const $dataTableWrap = $e('div', {}, noEvents, [$reExtractDataBtn, ...dataTable.elements]);

// Recent books

const $recentBooksWrap = $e('div');

// Ask AI toggle

const $askAIToggleBtn = $e(
  'button',
  { class: prefix('btn btn-sm btn-outline-secondary') },
  { click: showAskAI },
  [$t('Ask AI a question about this page')],
);

const $askAIToggleWrap = $e('div', { class: prefix('ask-ai-toggle-btn-wrap') }, noEvents, [
  $askAIToggleBtn,
]);

// Status messages

const $spinner = $e('div', {
  class: prefix('spinner'),
  role: 'status',
  title: 'Loading...',
  'aria-label': 'Loading...',
});
const $checkmark = $e('div', {
  class: prefix('status status--checkmark'),
  role: 'status',
  title: 'Note saved',
  'aria-label': 'Note saved',
});
const $alert = $e('div', {
  class: prefix('status status--alert'),
  role: 'status',
  title: 'Error saving note',
  'aria-label': 'Error saving note',
});

const $voteUI = $e('div', { class: prefix('vote-ui') }, noEvents, [...votes.elements]);

const $savePrivately = $e(
  'div',
  {
    class: prefix('save-privately'),
    title: 'Save Privately',
    'aria-label': 'Save Privately',
    'data-histre-save-privately': '',
  },
  { click: handleSavePrivatelyBtn },
);

const $publish = $e(
  'div',
  {
    class: prefix('publish'),
    title: 'Publish',
    'aria-label': 'Publish',
    'data-histre-read-later': '',
  },
  { click: handlePublishBtn },
);

const $readLater = $e(
  'div',
  {
    class: prefix('read-later'),
    title: 'Read later',
    'aria-label': 'Read later',
  },
  { click: toggleReadLater },
);

const $openSettingsButton = $e(
  'button',
  {
    class: prefix('control-icon control-icon--settings'),
    title: 'Settings',
    'aria-label': 'Settings',
  },
  { click: openSettings },
);

const $openSearchButton = $e(
  'button',
  {
    class: prefix('control-icon control-icon--search'),
    title: 'Search',
    'aria-label': 'Search',
  },
  { click: openSearch },
);

const $savingInfoContainer = $e('div', { class: prefix('saving-info-container') }, noEvents, [
  $spinner,
  $checkmark,
  $alert,
  $savePrivately,
  $publish,
  $readLater,
  $openSearchButton,
  $openSettingsButton,
]);

const $statusMessage = $e('div', { class: prefix('status-msg') });

const $mainContent = $e('div', { class: prefix('main') }, noEvents, [
  $editorContainer,
  $dataTableWrap,
  $recentBooksWrap,
  $askAIToggleWrap,
  $statusMessage,
]);

// Footer

const $footer = $e('div', { class: prefix('footer') }, noEvents, [$savingInfoContainer]);

// Container

const $body = $e('div', { class: prefix('body hide') }, noEvents, [$mainContent, askAI.element]);

// Loading UI

const $loadingUI = $e('div', { class: prefix('loading-ui') }, noEvents, [
  $e('div', {
    class: prefix('spinner spinner--loading-ui'),
    role: 'status',
    title: 'Loading note...',
    'aria-label': 'Loading note...',
  }),
]);

const $setupErrorMsgText = $e('div', {}, noEvents, [$t('An error occurred')]);
const $setupErrorMsg = $e('div', { class: prefix('setup-error-msg hide') }, noEvents, [
  $setupErrorMsgText,
]);

// Wrap

const $wrap = $e(
  'div',
  {
    id: prefix('wrap'),
    class: prefix('wrap'),
    'data-histre-element': '',
  },
  noEvents,
  [$body, $footer, $loadingUI, $setupErrorMsg],
);

const $css = $e('link', {
  rel: 'stylesheet',
  type: 'text/css',
  href: chrome.runtime.getURL('sidebar.css'),
});

// Other elements

const $authError = document.querySelector('#auth-error');
const $permissionError = document.querySelector('#permission-error');

document.querySelector('#permission-btn').addEventListener('click', async () => {
  const granted = await chrome.permissions.request({
    origins: [`${host.replace(/:(\d+)/, '')}/` /* remove port number */],
  });

  if (granted) {
    location.reload();
  }
});

//----- Setup UI -----//

histreDOMHelper.loadFont();

chrome.storage.local.get(['askAIModel'], async (result) => {
  if (result.askAIModel) {
    askAI.setAskAIModel(result.askAIModel);
  }

  (document.head || document.documentElement).appendChild($css);
  document.body.appendChild($wrap);

  startLoadingUI();

  // This doesn't need to be awaited, because quill mention is live updated
  // as soon as tags are fetched
  updateTagList();

  const mention = utils.getMentionConfig();

  quill = new Quill(`#${prefix('editor')}`, {
    modules: {
      toolbar: false,
      keyboard: {
        bindings: {
          // "Cmd/Ctrl Enter" to save and close
          custom: {
            key: 13,
            shortKey: true,
            handler() {
              saveNote();
            },
          },
        },
      },
      mention,
      magicUrl: true,
    },
  });

  utilsRefs.quill = quill;

  quill.on('text-change', (delta, oldDelta, source) => {
    if (source === 'api') {
      // pass
    } else if (source === 'user') {
      saveNoteDebounced(true);
    }
  });

  chrome.runtime.sendMessage({ action: 'getSidebarNote', dest: 'sidebar' });
});

chrome.runtime.onMessage.addListener(onMessageHandler);
chrome.tabs.onActivated.addListener(tabActivatedHandler);
chrome.storage.onChanged.addListener(onStorageChangedHandler);
