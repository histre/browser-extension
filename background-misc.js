import { apiBase } from '/globals.js';
import { helper } from '/helper.js';
import { histreOrigins } from '/background-globals.js';

const outreachUrl = `${apiBase}/outreach/`;
const askAIUrl = `${apiBase}/gpt/ask/`;
const extractUrl = `${apiBase}/extract/`;
const userPromptsUrl = `${apiBase}/gpt/user_prompt/`;
const chatGPTConversationsUrl = `${apiBase}/chats/chatgpt/`;
const chatGPTImportConversationsUrl = `${apiBase}/chats/import/chatgpt/`;

const chatGPTImportBatchSize = 10;

function executeScript(tabId, details) {
  return new Promise((resolve) => {
    if (typeof tabId === 'number') {
      const injection = {
        ...details,
        target: { tabId },
      };

      chrome.scripting.executeScript(injection, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    } else {
      chrome.scripting.executeScript(tabId, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    }
  });
}

function addAddressableContentScript(tab) {
  chrome.permissions.contains(
    {
      origins: ['*://*/*'],
    },
    function (result) {
      if (result) {
        chrome.scripting.executeScript({
          files: ['addressable.js'],
          target: { tabId: tab.id },
        });
        if (chrome.runtime.lastError) {
        }
      } else {
        // we don't have the permission to do that
      }
    },
  );
}

function addTabsContentScript(tab) {
  chrome.permissions.contains(
    {
      origins: histreOrigins,
    },
    function (result) {
      if (result) {
        chrome.scripting.executeScript({
          files: ['histre_tabs.js'],
          target: { tabId: tab.id },
        });
        if (chrome.runtime.lastError) {
        }
      } else {
        // we don't have the permission to do that
      }
    },
  );
}

function closeInstallCardOnHomePage(tabId) {
  chrome.permissions.contains(
    {
      origins: histreOrigins,
    },
    (enabled) => {
      if (enabled) {
        chrome.scripting.executeScript({
          func: () => {
            document.getElementById('install-card-close-btn')?.click();
          },
          target: { tabId },
        });
      }
    },
  );
}

async function getAskAIPrompts() {
  try {
    const response = await fetch(userPromptsUrl, helper.setRequestParams('GET'));
    const result = await response.json();

    if (result.error !== false) {
      throw result;
    }

    return result.data.prompts;
  } catch (e) {
    helper.llog('Error getting user ask AI prompts', e);
    return [];
  }
}

async function deleteAskAIPrompt(promptId) {
  try {
    const response = await fetch(
      `${userPromptsUrl}${promptId}/`,
      helper.setRequestParams('DELETE'),
    );
    const result = await response.json();

    if (result.error !== false) {
      throw result;
    }

    return true;
  } catch (e) {
    helper.llog('Error getting user ask AI prompts', e);
    return false;
  }
}

function tabUpdatedHandler(tabId, changes, tab) {
  if (!tab || !tab.url) {
    return;
  }

  const url = new URL(tab.url);

  if (url.protocol.indexOf('http') === 0 && changes.status === 'complete') {
    if (helper.isContentAddressable(tab.url)) {
      addAddressableContentScript(tab);
    }
  }

  if (url.hostname.includes('histre.com')) {
    if (url.pathname === '/') {
      closeInstallCardOnHomePage(tabId);
    } else if (url.pathname.includes('/collections/')) {
      // Include check for /public/collections/
      addTabsContentScript(tab);
    } else {
      // do nothing
    }
  }

  if (url.hostname.includes('linkedin.com') && changes.status === 'complete') {
    chrome.permissions.contains({ origins: ['https://www.linkedin.com/'] }, async (enabled) => {
      if (enabled) {
        const result = await executeScript(tab.id, { files: ['outreach-loader.js'] });
        if (!Array.isArray(result) || result.length <= 0) {
          //? Something went wrong
          helper.llog('Error loading outreach', tab, result);
          return;
        }
        const isHighlightLoaded = result[0].result;
        if (isHighlightLoaded === false) {
          await executeScript(tab.id, { files: ['dom-helper.js'] });
          await executeScript(tab.id, { files: ['outreach.js'] });
        }
        helper.doGet(outreachUrl, {}, async (result) => {
          if (!result.error) {
            chrome.tabs.sendMessage(tab.id, {
              action: 'setupTemplates',
              templates: result.data.outreach_templates,
            });
          }
        });
      }
    });
  }

  if (url.hostname.includes('chatgpt.com') && changes.status === 'complete') {
    chrome.permissions.contains({ origins: ['https://chatgpt.com/'] }, async (enabled) => {
      if (enabled) {
        const result = await executeScript(tab.id, { files: ['chat-gpt-import-loader.js'] });
        if (!Array.isArray(result) || result.length <= 0) {
          //? Something went wrong
          helper.llog('Error loading chat GPT import', tab, result);
          return;
        }
        const isGPTImport = result[0].result;
        if (isGPTImport === false) {
          await executeScript(tab.id, { files: ['purify.min.js'] });
          await executeScript(tab.id, { files: ['dom-helper.js'] });
          await executeScript(tab.id, { files: ['chat-gpt-import.js'] });
        }
      }
    });
  }
}

function onMessageHandler(message, sender, sendResponse) {
  if (message.action === 'askAI') {
    fetch(askAIUrl, helper.setRequestParams('POST', message.payload))
      .then((response) => response.json())
      .then((result) => {
        sendResponse(result);
      });
    return true;
  }

  if (message.action === 'getAskAIPrompts') {
    getAskAIPrompts().then((prompts) => {
      sendResponse(prompts);

      if (prompts.length > 0) {
        chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
          tabs.forEach((tab) => {
            if (tab.id !== sender.tab?.id) {
              chrome.tabs.sendMessage(tab.id, {
                action: 'loadAskAIPrompts',
                data: prompts,
              });
            }
          });
        });
      }
    });
    return true;
  }

  if (message.action === 'deleteAskAIPrompt') {
    deleteAskAIPrompt(message.promptId).then((isDeleted) => {
      sendResponse(isDeleted);

      if (isDeleted) {
        chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
          tabs.forEach((tab) => {
            if (tab.id !== sender.tab.id) {
              chrome.tabs.sendMessage(tab.id, {
                action: 'loadAskAIPrompts',
                data: message.prompts,
              });
            }
          });
        });
      }
    });
    return true;
  }

  if (message.action === 'getLinkedInAttrs') {
    message.url ??= sender.tab.url;

    fetch(`${extractUrl}?url=${encodeURIComponent(message.url)}`, helper.setRequestParams('GET'))
      .then((response) => response.json())
      .then((result) => {
        sendResponse(result);
      });
    return true;
  }

  if (message.action === 'logOutreachTemplateUsed') {
    helper.sendFeatureLog('outreach-template-used');
    return;
  }

  if (message.action === 'getChatGTPConversation') {
    fetch(`${chatGPTConversationsUrl}${message.conversationId}/`, helper.setRequestParams('GET'))
      .then((response) => {
        return response.json();
      })
      .then((result) => {
        sendResponse(result);
      })
      .catch((e) => {
        sendResponse({ error: true, message: e.message });
      });
    return true;
  }

  if (message.action === 'saveChatGTPConversation') {
    (async () => {
      try {
        const batches = [];

        for (let i = 0; i < message.payload.chats.length; i += chatGPTImportBatchSize) {
          batches.push(message.payload.chats.slice(i, i + chatGPTImportBatchSize));
        }

        let data;

        for (const batch of batches) {
          const response = await fetch(
            chatGPTImportConversationsUrl,
            helper.setRequestParams('POST', { ...message.payload, chats: batch }),
          );
          const result = await response.json();
          data = result.data;
        }

        sendResponse({ error: false, data });
      } catch (e) {
        sendResponse({ error: true, message: e.message });
      }
    })();
    return true;
  }
}

function onConnectHandler(port) {
  if (port.name?.startsWith('popupAskAISwitch_')) {
    port.onDisconnect.addListener(async () => {
      const tabId = parseInt(port.name.split('_')[1]);
      const noteModified = parseInt(port.name.split('_')[2]);

      const result = await executeScript(tabId, { files: ['ask-ai-loader.js'] });
      if (!Array.isArray(result) || result.length <= 0) {
        //? Something went wrong
        return;
      }

      const isAskAILoaded = result[0].result;
      if (isAskAILoaded === false) {
        await executeScript(tabId, { files: ['dom-helper.js'] });
        await executeScript(tabId, { files: ['utils.js'] });
        await executeScript(tabId, { files: ['ask-ai.js'] });
      }

      const askAIPrompts = await getAskAIPrompts();
      chrome.tabs.sendMessage(tabId, {
        action: 'loadAskAIPrompts',
        data: askAIPrompts,
      });

      // Set timeout is to compensate for the time popup close animation is done, and input can be focused
      // on a page. While testing, I got it to work as low as a 5ms, but I've added some headroom just in case.
      setTimeout(() => {
        chrome.tabs.sendMessage(tabId, { action: 'focusAskAIInput' });
        if (noteModified) {
          chrome.tabs.sendMessage(tabId, { action: 'setAskAINoteModified', noteModified });
        }
      }, 200);
    });
  }
}

chrome.tabs.onUpdated.addListener(tabUpdatedHandler);
chrome.runtime.onConnect.addListener(onConnectHandler);
chrome.runtime.onMessage.addListener(onMessageHandler);
