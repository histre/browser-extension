(function () {
  if (window.histre_loaded_guard_contentscripts) {
    return false;
  }
  window.histre_loaded_guard_contentscripts = true;
  return true;
})();
