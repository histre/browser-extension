// do not use console.log or helper.llog here

function initialize() {
  if (!window.histre_tabs) {
    const tabOpenerWrap = document.querySelector('#tab-opener-wrap');
    if (!tabOpenerWrap) {
      return;
    }
    const tabOpenerElement = tabOpenerWrap.querySelector('#tab-opener');

    tabOpenerWrap.setAttribute('data-bs-original-title', '');
    tabOpenerWrap.removeAttribute('aria-label');
    tabOpenerElement.removeAttribute('disabled');

    window.histre_tabs = true;

    tabOpenerElement.addEventListener('click', () => {
      const items = document.querySelectorAll('.book-notes .note-title');
      const urls = [];
      for (let i = 0; i < items.length; i++) {
        if (items[i].href) {
          urls.push(items[i].href);
        }
      }
      if (chrome.runtime) {
        try {
          chrome.runtime.sendMessage({ type: 'open_tabs', urls: urls });
        } catch (e) {
          /* TODO notify user */
        }
      }
    });
  }
}

initialize();
