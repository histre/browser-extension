(function () {
  const pageElement = document.documentElement.cloneNode(true);

  const histreElements = pageElement.querySelectorAll('[data-histre-element]');

  for (const histreElement of histreElements) {
    histreElement.remove();
  }

  const payload = {
    url: location.href,
    page_source: pageElement.outerHTML,
    title: document.title,
  };

  chrome.runtime.sendMessage({ action: 'sendPageSource', payload });

  return true;
})();
