import { isDevMode, host } from '/globals.js';

const checkDomainMatch = function (link, domainsToMatch) {
  // Checks if a URL's domain matches a pattern in an array
  const url = new URL(link);
  const hostname = url.hostname;
  return domainsToMatch.some(function (domainMatch) {
    return hostname.endsWith(domainMatch);
  });
};

export const helper = {
  HttpCodes: {
    success: 200,
    badRequest: 400,
    notFound: 404,
    unauthorized: 401,
    forbidden: 403,
  },

  llog: function (...args) {
    if (isDevMode) {
      console.log(...args);
    }
  },

  uuid4: function () {
    // https://stackoverflow.com/a/2117523
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
      (c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(16),
    );
  },

  setIconWarning: function () {
    chrome.action.setBadgeBackgroundColor({ color: [190, 0, 0, 230] });
    chrome.action.setBadgeText({ text: '!' });
  },

  unsetIconWarning: function () {
    chrome.action.setBadgeText({ text: '' });
  },

  async setDefaultIcon(tabId, tabUrl, cb) {
    const tab = await chrome.tabs.get(tabId);
    if (tab?.url !== tabUrl) return;

    chrome.action.setIcon(
      {
        path: {
          128: '/assets/icon-128.png',
          16: '/assets/icon-16.png',
          48: '/assets/icon-48.png',
        },
        tabId,
      },
      () => {
        if (typeof cb === 'function') cb();
      },
    );
  },

  async setNoteIsPresentIcon(tabId, tabUrl) {
    const tab = await chrome.tabs.get(tabId);
    if (tab?.url !== tabUrl) return;

    chrome.action.setIcon({
      path: {
        128: '/assets/icon-filled-128.png',
        16: '/assets/icon-filled-16.png',
        48: '/assets/icon-filled-48.png',
      },
      tabId,
    });
  },

  strftime: function (sFormat, date) {
    // https://github.com/thdoan/strftime/blob/master/strftime.js
    if (!(date instanceof Date)) date = new Date();
    var nDay = date.getDay(),
      nDate = date.getDate(),
      nMonth = date.getMonth(),
      nYear = date.getFullYear(),
      nHour = date.getHours(),
      aDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
      aMonths = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ],
      aDayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334],
      isLeapYear = function () {
        return (nYear % 4 === 0 && nYear % 100 !== 0) || nYear % 400 === 0;
      },
      getThursday = function () {
        var target = new Date(date);
        target.setDate(nDate - ((nDay + 6) % 7) + 3);
        return target;
      },
      zeroPad = function (nNum, nPad) {
        return (Math.pow(10, nPad) + nNum + '').slice(1);
      };
    return sFormat.replace(/%[a-z]/gi, function (sMatch) {
      return (
        ({
          '%a': aDays[nDay].slice(0, 3),
          '%A': aDays[nDay],
          '%b': aMonths[nMonth].slice(0, 3),
          '%B': aMonths[nMonth],
          '%c': date.toUTCString(),
          '%C': Math.floor(nYear / 100),
          '%d': zeroPad(nDate, 2),
          '%e': nDate,
          '%F': date.toISOString().slice(0, 10),
          '%G': getThursday().getFullYear(),
          '%g': (getThursday().getFullYear() + '').slice(2),
          '%H': zeroPad(nHour, 2),
          '%I': zeroPad(((nHour + 11) % 12) + 1, 2),
          '%j': zeroPad(aDayCount[nMonth] + nDate + (nMonth > 1 && isLeapYear() ? 1 : 0), 3),
          '%k': nHour,
          '%l': zeroPad(((nHour + 11) % 12) + 1, 2) /* Modified */,
          '%m': zeroPad(nMonth + 1, 2),
          '%n': nMonth + 1,
          '%M': zeroPad(date.getMinutes(), 2),
          '%p': nHour < 12 ? 'AM' : 'PM',
          '%P': nHour < 12 ? 'am' : 'pm',
          '%s': Math.round(date.getTime() / 1000),
          '%S': zeroPad(date.getSeconds(), 2),
          '%u': nDay || 7,
          '%V': (function () {
            var target = getThursday(),
              n1stThu = target.valueOf();
            target.setMonth(0, 1);
            var nJan1 = target.getDay();
            if (nJan1 !== 4) target.setMonth(0, 1 + ((4 - nJan1 + 7) % 7));
            return zeroPad(1 + Math.ceil((n1stThu - target) / 604800000), 2);
          })(),
          '%w': nDay,
          '%x': date.toLocaleDateString(),
          '%X': date.toLocaleTimeString(),
          '%y': (nYear + '').slice(2),
          '%Y': nYear,
          '%z': date.toTimeString().replace(/.+GMT([+-]\d+).+/, '$1'),
          '%Z': date.toTimeString().replace(/.+\((.+?)\)$/, '$1'),
        }[sMatch] || '') + '' || sMatch
      );
    });
  },

  friendlyTime: function () {
    return this.strftime('%b %e, %Y %l:%M%P');
  },

  showLoginPage: function () {
    const ts = Date.now();
    chrome.storage.local.set({ lastLoginPageOpen: ts }, function () {
      const loginPageUrl = `${host}/accounts/login/?from=extn`;
      chrome.tabs.create({ url: loginPageUrl });
    });
  },

  handleAuthErrorsOnce: function (callback, data) {
    chrome.storage.local.get(['lastLoginPageOpen'], function (result) {
      let ts = result.lastLoginPageOpen;
      if (!ts) {
        helper.showLoginPage();
      } else {
        const timeLimit = 86400000; // ms in 24 hours
        ts = parseInt(ts);
        const now = Date.now();
        if (now - ts > timeLimit) {
          helper.showLoginPage();
        } else {
          if (callback) {
            callback(data);
          }
        }
      }
    });

    // Clear user info if there's auth error, because maybe user is switching
    // to an another account
    chrome.storage.local.set({
      userUsername: '',
      userExternalId: '',
    });
  },

  isAuthErrorStatus(statusCode) {
    return statusCode === this.HttpCodes.unauthorized || statusCode === this.HttpCodes.forbidden;
  },

  setRequestParams: function (method = 'GET', data = null) {
    const reqParams = {
      method: method,
      headers: {
        'Content-Type': 'application/json',
        'X-Histre-For': 'extn',
      },
      credentials: 'include',
    };
    if (method !== 'GET') {
      reqParams['body'] = JSON.stringify(data);
    }
    return reqParams;
  },

  doRequest: function (method, url, data, callback, errorHandler) {
    const params = helper.setRequestParams(method, data);
    fetch(url, params)
      .then(function (response) {
        return response.json();
      })
      .then((json) => {
        helper.llog(json);
        const { errcode } = json;
        if (typeof json.settings !== 'undefined' && typeof json.settings.history !== 'undefined') {
          chrome.storage.local.set({ userLogHistory: json.settings.history });
        }

        if (errcode == this.HttpCodes.unauthorized || errcode == this.HttpCodes.forbidden) {
          this.handleAuthErrorsOnce(callback, json);
        } else {
          if (callback) {
            callback(json);
          }
        }
      })
      .catch((error) => {
        this.llog('doRequest error:', error);
        if (typeof errorHandler === 'function') {
          errorHandler(error);
        }
      });
  },

  doGet: function (url, data, callback, errorHandler) {
    return this.doRequest('GET', url, data, callback, errorHandler);
  },

  doPost: function (url, data, callback, errorHandler) {
    return this.doRequest('POST', url, data, callback, errorHandler);
  },

  doPatch: function (url, data, callback, errorHandler) {
    return this.doRequest('PATCH', url, data, callback, errorHandler);
  },

  doDelete: function (url, data, callback, errorHandler) {
    return this.doRequest('DELETE', url, data, callback, errorHandler);
  },

  sendFeatureLog: async function (feature) {
    try {
      const response = await fetch(
        `${host}/api/v1/pulse/features/`,
        this.setRequestParams('POST', { feature }),
      );
      return await response.json();
    } catch (error) {
      return { error: true, errmsg: error.message };
    }
  },

  isContentAddressable: function (pageUrl) {
    const urlMatches = [
      '.craigslist.org',
      // add snippets of other URLs/domains here
    ];
    return checkDomainMatch(pageUrl, urlMatches);
  },

  isDataExtractable: function (pageUrl) {
    const urlMatches = [
      'twitter.com',
      'airbnb.com',
      'airbnb.ca',
      'airbnb.ie',
      'airbnb.co.uk',
      'airbnb.co.in',
      'airbnb.co.nz',
      'airbnb.co.za',
      'airbnb.com.au',
      'airbnb.com.mt',
      // add snippets of other URLs/domains here
    ];
    return checkDomainMatch(pageUrl, urlMatches);
  },

  refreshUserId: function (pageUrl) {
    this.doGet(pageUrl, {}, (response) => {
      chrome.storage.local.set({ userExternalId: '' });
    });
  },

  removeURLFragment(url, isEncoded) {
    const urlObj = new URL(isEncoded ? decodeURIComponent(url) : url);
    return urlObj.origin + urlObj.pathname + urlObj.search;
  },

  reduceURLSize(url) {
    let newUrl;
    if (url.length > this.MAX_URL_LEN) {
      newUrl = this.removeURLFragment(url);
      if (newUrl.length > this.MAX_URL_LEN) {
        const urlObj = new URL(url);
        newUrl = urlObj.origin + urlObj.pathname;
        if (newUrl.length > this.MAX_URL_LEN) {
          newUrl = false;
        }
      }
    } else {
      newUrl = url;
    }
    return newUrl;
  },

  MAX_URL_LEN: 2048,

  errmsgs: {
    urlMissing: 'url missing',
    urlTooLong: 'url too long',
  },
};
