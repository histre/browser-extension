// Function definitions for attributes extraction/handling in popup.js

const toHeaderText = (fieldName) => {
  // Convert snake-case field names to title case for header text
  if (!fieldName) {
    return '';
  }
  return fieldName.replace(/_+/g, ' ').replace(
    /\w\S*/g,
    (text) => {
      return text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
    }
  );
};

const renderExtractedData = (extractableResult) => {
  const { data, errorMsg } = extractableResult;
  if (data) {
    const dataTableWrap = document.getElementById('extracted-data-table-wrap');
    const dataTable = document.createElement('table');
    dataTable.setAttribute('id', 'data-table');

    for (let field in data) {
      let fieldValue = data[field];
      if (fieldValue === null || fieldValue === undefined || fieldValue === '') {
        continue;
      }

      if (typeof fieldValue === 'boolean') {
        fieldValue = (fieldValue) ? 'Yes' : 'No';
      }

      let row = document.createElement('tr');
      row.setAttribute('class', 'extracted-data-row');

      // Create row header
      let rowHeader = document.createElement('th');
      rowHeader.setAttribute('scope', 'row');
      rowHeader.setAttribute('class', 'extracted-data-row-header');
      rowHeader.textContent = toHeaderText(field);
      row.appendChild(rowHeader);

      // Set row value
      let rowValue = document.createElement('td');
      rowValue.setAttribute('class', 'extracted-data-row-value');
      rowValue.textContent = fieldValue;
      row.appendChild(rowValue);

      dataTable.appendChild(row);
    }

    dataTableWrap.appendChild(dataTable);
  } else {
    if (errorMsg) {
      // Currently on page with extractable data but none were found
      setMsg(errorMsg);
    } else {
      // Currently not on page with extractable data, so no need to show error msg
    }
  }
};

const extractStructuredData = (sourceUrl, tabId, cb) => {
  chrome.tabs.executeScript(
    tabId,
    {
      file: 'extractable.js',
    },
    () => {
      chrome.tabs.sendMessage(
        tabId,
        { extractable: sourceUrl },
        (msg) => {
          if (cb) { cb(msg.result); }
        },
      );
    },
  );
};

const runDataExtraction = (sourceUrl, tabId, cb) => {
  chrome.storage.local.get(['featuresEnhanceWebsites'], (result) => {
    if (result.featuresEnhanceWebsites) {
      extractStructuredData(sourceUrl, tabId, cb);
    } else {
      if (cb) { cb(); }
    }
  });
};
