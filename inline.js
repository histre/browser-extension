(function () {
  const histreDOMHelper = window.histreDOMHelper;
  const { $e, $t, noEvents } = histreDOMHelper;
  const prefix = (classNames) => histreDOMHelper.prefix('inline', classNames);
  const host = window.histreDOMHelper.getHost();

  const utils = new window.histreUtils('inline');
  const votes = utils.createVoteUI({ showPopup });

  // For drag functionality
  let mouseX = 0;
  let mouseY = 0;
  let leftOffset = '';
  let topOffset = '';

  let popupTimeout;
  let popupMouseover;

  let youtubeFullscreen = null;

  function nukeSelf() {
    window.histreLoadedInline = false;
    clearOld();
    chrome.runtime.onMessage.removeListener(onMessageHandler);
  }

  // clear elements from previous invocations
  function clearOld(cb) {
    while (document.getElementById(prefix('wrap'))) {
      document.getElementById(prefix('wrap')).remove();
    }

    while (document.querySelector(`link[href="${chrome.runtime.getURL('inline.css')}"]`)) {
      document.querySelector(`link[href="${chrome.runtime.getURL('inline.css')}"]`).remove();
    }

    if (cb) {
      cb();
    }
  }

  function openHistre() {
    window.open(host);
  }

  function openNote() {
    const link = this.getAttribute('histre-note-link');
    window.open(link);
  }

  function openSettings() {
    //? Opening settings page doesn't work with inline script
    try {
      chrome.runtime.sendMessage(
        { action: 'openOptionsPage', dest: 'inline' },
        function (response) {
          if (chrome.runtime.lastError) {
            console.log(JSON.stringify(chrome.runtime.lastError));
          }
          if (response && response.usermsg) {
            // TODO: Replace below with logic to show error message to user
            console.log(response.usermsg);
          }
        },
      );
    } catch (e) {
      // connection to background scripts lost, perhaps due to extn reload
      // clear self etc so the user reloads the page
      nukeSelf();
    }
  }

  function handleSavePrivatelyBtn() {
    if (!this.classList.contains(prefix('icon-save-privately--saved'))) {
      showPopup({ action: 'loading' });
      setSavePrivatelyIcon();
      chrome.runtime.sendMessage({ action: 'savePrivately', dest: 'inline' });
    } else {
      const noteId = $wrap.getAttribute('data-histre-note-id');
      if (!noteId) return;

      getUserInfo(({ username }) => {
        window.open(`${host}/notes/${username}/${noteId}/`);
      });
    }
  }

  function handlePublishBtn() {
    if (!this.classList.contains(prefix('icon-publish--published'))) {
      showPopup({ action: 'loading' });
      setPublishIcon();
      chrome.runtime.sendMessage({ action: 'publishNote', dest: 'inline' });
    } else {
      const noteId = $wrap.getAttribute('data-histre-note-id');
      if (!noteId) return;

      getUserInfo(({ username }) => {
        window.open(`${host}/notes/${username}/${noteId}/`);
      });
    }
  }

  function setSavePrivatelyIcon() {
    $savePrivately.classList.add(prefix('icon-save-privately--saved'));
    removePublishIcon();
  }

  function removeSavePrivatelyIcon() {
    $savePrivately.classList.remove(prefix('icon-save-privately--saved'));
  }

  function setPublishIcon() {
    $publish.classList.add(prefix('icon-publish--published'));
    removeSavePrivatelyIcon();
  }

  function removePublishIcon() {
    $publish.classList.remove(prefix('icon-publish--published'));
  }

  function toggleReadLater() {
    showPopup({ action: 'loading' });
    if (!this.classList.contains(prefix('icon-read-later--saved'))) {
      setReadLaterActive();
      chrome.runtime.sendMessage({ action: 'saveNoteToReadLater', dest: 'inline' });
    } else {
      const bookId = $wrap.getAttribute('data-histre-read-later-book-id');
      const noteId = $wrap.getAttribute('data-histre-note-id');

      if (!bookId || !noteId) return;

      removeReadLaterActive();

      chrome.runtime.sendMessage({
        action: 'removeNoteFromReadLater',
        bookId,
        noteId,
        dest: 'inline',
      });
    }
  }

  function setReadLaterActive() {
    $readLater.classList.add(prefix('icon-read-later--saved'));
  }

  function removeReadLaterActive() {
    $readLater.classList.remove(prefix('icon-read-later--saved'));
  }

  function openSidebar() {
    chrome.runtime.sendMessage({
      action: 'openSidePanel',
      dest: 'sidebar',
    });
  }

  function getUserInfo(cb) {
    chrome.storage.local.get(['userUsername', 'userExternalId'], (items) => {
      if (items.userUsername) {
        return cb({
          username: items.userUsername,
          externalId: items.userExternalId,
        });
      } else {
        chrome.runtime.sendMessage({ action: 'setUserInfo', dest: 'inline' }, {}, (res) => {
          if (res.isSet) {
            getUserInfo(cb);
          } else {
            //TODO Implement some sort of error message or figure out how to disable menu on user signout
            return;
          }
        });
      }
    });
  }

  function setNoteIsPresent(noteId) {
    if ($logoImg.getAttribute('src') !== chrome.runtime.getURL('assets/inline/logo-filled.svg')) {
      if (!noteId) {
        chrome.runtime.sendMessage({ action: 'getNoteIdAndSetNoteIsPresent', dest: 'inline' });
        return;
      }

      $logoImg.setAttribute('src', chrome.runtime.getURL('assets/inline/logo-filled.svg'));
      $logo.setAttribute('title', 'View note');
      $logo.removeEventListener('click', openHistre);
      getUserInfo(({ username }) => {
        $logo.setAttribute('histre-note-link', `${host}/notes/${username}/${noteId}`);
        $logo.addEventListener('click', openNote);
      });
    }
  }

  function removeNoteIsPresent() {
    if ($logoImg.getAttribute('src') !== chrome.runtime.getURL('assets/inline/logo.svg')) {
      $logoImg.setAttribute('src', chrome.runtime.getURL('assets/inline/logo.svg'));
      $logo.removeAttribute('title');
      $logo.removeAttribute('histre-note-link');
      $logo.removeEventListener('click', openNote);
      $logo.addEventListener('click', openHistre);
    }
  }

  function onMessageHandler(message) {
    if (message.action === 'showInline') {
      if (!checkUrl(message.url)) return;

      $wrap.classList.remove(prefix('d-none'));
      return;
    }

    if (message.action === 'hideInline') {
      if (!checkUrl(message.url)) return;

      $wrap.classList.add(prefix('d-none'));
      return;
    }

    if (message.action === 'showVote') {
      if (!checkUrl(message.url)) return;

      votes.updateVote(message.vote);

      if (message.noteModified) {
        setNoteIsPresent(message.noteId);
      }

      if (message.url.startsWith('https://www.youtube.com/watch?')) {
        setupYoutubeFullscreen();
      } else {
        removeYoutubeFullscreen();
      }
      return;
    }

    if (message.action === 'updateVoteInline') {
      if (!checkUrl(message.url)) return;

      votes.updateVote(message.vote);
      return;
    }

    if (message.action === 'resetInline') {
      if (!checkUrl(message.url)) return;

      votes.updateVote(0);
      removeNoteIsPresent();
      removeReadLaterActive();
      return;
    }

    if (message.action === 'loading') {
      return;
    }

    if (message.action === 'disableInline') {
      nukeSelf();
      return;
    }

    if (message.action === 'positionInline') {
      chrome.storage.local.get(['inlinePosTop', 'inlinePosLeft'], (result) => {
        // If it's undefined it will not be set
        $wrap.style.top = result.inlinePosTop;
        $wrap.style.left = result.inlinePosLeft;
      });
      return;
    }

    if (message.action === 'setNoteIsPresent') {
      if (!checkUrl(message.url)) return;

      setNoteIsPresent(message.noteId || null);
      return;
    }

    if (message.action === 'openSidebar') {
      openSidebar();
      return;
    }

    if (message.action === 'setSavePrivatelySaved') {
      if (!checkUrl(message.url)) return;

      if (message.noteId) {
        $wrap.setAttribute('data-histre-note-id', message.noteId);
      }

      setSavePrivatelyIcon();
    }

    if (message.action === 'removeSavePrivatelySaved') {
      if (!checkUrl(message.url)) return;

      removeSavePrivatelyIcon();
    }

    if (message.action === 'setPublishNotePublished') {
      if (!checkUrl(message.url)) return;

      if (message.noteId) {
        $wrap.setAttribute('data-histre-note-id', message.noteId);
      }

      setPublishIcon();
    }

    if (message.action === 'removePublishNotePublished') {
      if (!checkUrl(message.url)) return;

      removePublishIcon();
    }

    if (message.action === 'setInlineReadLater') {
      if (!checkUrl(message.url)) return;

      if (message.bookId) {
        $wrap.setAttribute('data-histre-read-later-book-id', message.bookId);
      }
      if (message.noteId) {
        $wrap.setAttribute('data-histre-note-id', message.noteId);
      }

      setReadLaterActive();
      return;
    }

    if (message.action === 'removeInlineReadLater') {
      if (!checkUrl(message.url)) return;

      removeReadLaterActive();
      return;
    }

    if (message.action === 'showReadLaterSavedPopup') {
      if (!checkUrl(message.url)) return;

      showPopup({ action: 'readLaterSaved', bookId: message.bookId });
      return;
    }

    if (message.action === 'showReadLaterRemovedPopup') {
      if (!checkUrl(message.url)) return;

      showPopup({ action: 'readLaterRemoved' });
      return;
    }

    if (message.action === 'showSavePrivatelySavedPopup') {
      if (!checkUrl(message.url)) return;

      showPopup({ action: 'savePrivatelySaved' });
      return;
    }

    if (message.action === 'showPublishNotePublishedPopup') {
      if (!checkUrl(message.url)) return;

      showPopup({ action: 'notePublished' });
      return;
    }

    if (message.action === 'showSidebarFirefoxCTA') {
      if (!checkUrl(message.url)) return;

      showPopup({ action: 'sidebarFirefoxCTA' });
      return;
    }

    if (message.action === 'showErrorPopup') {
      if (!checkUrl(message.url)) return;

      showPopup({ action: 'error' });
      return;
    }
  }

  function startInlineDrag(e) {
    e.preventDefault();

    mouseX = e.clientX;
    mouseY = e.clientY;

    document.addEventListener('mouseup', stopInlineDrag);
    document.addEventListener('mousemove', inlineDrag);
  }

  function inlineDrag(e) {
    e.preventDefault();

    const dragX = mouseX - e.clientX;
    const dragY = mouseY - e.clientY;

    // Refresh mouse coordinates
    mouseX = e.clientX;
    mouseY = e.clientY;

    // Prevent moving inline offscreen
    if ($wrap.offsetLeft - dragX < $wrap.clientWidth / 2) {
      leftOffset = `${$wrap.clientWidth / 2}px`;
    } else if ($wrap.offsetLeft - dragX > window.innerWidth - $wrap.clientWidth / 2) {
      leftOffset = `calc(100vw - ${$wrap.clientWidth / 2}px`;
    } else {
      leftOffset = `${(($wrap.offsetLeft - dragX) / window.innerWidth) * 100}vw`;
    }

    if ($wrap.offsetTop - dragY < 0) {
      topOffset = '0';
    } else if ($wrap.offsetTop - dragY > window.innerHeight - $wrap.clientHeight) {
      topOffset = `calc(100vh - ${$wrap.clientHeight}px)`;
    } else {
      topOffset = `${(($wrap.offsetTop - dragY) / window.innerHeight) * 100}vh`;
    }

    $wrap.style.left = leftOffset;
    $wrap.style.top = topOffset;
  }

  function stopInlineDrag() {
    document.removeEventListener('mouseup', stopInlineDrag);
    document.removeEventListener('mousemove', inlineDrag);

    chrome.storage.local.set({ inlinePosTop: topOffset, inlinePosLeft: leftOffset });
  }

  function showPopup(info) {
    switch (info.action) {
      case 'loading':
        $popup.innerText = 'Saving...';
        break;
      case 'votedUp':
        $popup.innerText = '';

        $popup.append(
          $t('Vote saved '),
          $e(
            'span',
            { class: prefix('popup-link') },
            {
              click() {
                window.open(`${host}/votes/`);
              },
            },
            [$t('here')],
          ),
        );
        break;
      case 'voteRemoved':
        $popup.innerText = 'Vote removed';
        break;
      case 'readLaterSaved':
        $popup.innerText = '';

        $popup.append(
          $t('Saved note to read later '),
          $e(
            'span',
            { class: prefix('popup-link') },
            {
              click() {
                window.open(`${host}/collections/${info.bookId}/`);
              },
            },
            [$t('here')],
          ),
        );
        break;
      case 'readLaterRemoved':
        $popup.innerText = 'Removed note from read later';
        break;
      case 'savePrivatelySaved':
        $popup.innerText = '';

        $popup.append(
          $t('Privately saved '),
          $e(
            'span',
            { class: prefix('popup-link') },
            {
              click() {
                window.open(`${host}/notes/`);
              },
            },
            [$t('here')],
          ),
        );
        break;
      case 'notePublished':
        $popup.innerText = '';

        $popup.append(
          $t('Note published '),
          $e(
            'span',
            { class: prefix('popup-link') },
            {
              click() {
                getUserInfo(({ username }) => {
                  window.open(`${host}/@${username}/notes/`);
                });
              },
            },
            [$t('here')],
          ),
        );
        break;
      case 'sidebarFirefoxCTA':
        $popup.innerText =
          'To open a sidebar in Firefox, please right click anywhere on a page, and select "Open Sidebar"';
        break;
      case 'error':
        $popup.innerText = 'An error occurred, please try again...';
        break;
      default:
        return;
    }

    clearTimeout(popupTimeout);
    $popup.classList.remove(prefix('hide'));

    if (info.action !== 'loading') {
      hidePopup();
    }
  }

  function hidePopup() {
    if (popupMouseover) return;

    popupTimeout = setTimeout(() => {
      $popup.classList.add(prefix('hide'));
    }, 3000);
  }

  function popupMouseEnterHandler() {
    popupMouseover = true;

    clearTimeout(popupTimeout);
  }

  function popupMouseLeaveHandler() {
    popupMouseover = false;

    hidePopup();
  }

  function setupYoutubeFullscreen() {
    if (youtubeFullscreen) return;

    youtubeFullscreen = new (function () {
      this.timeout = null;
      this.inlineWrap = document.querySelector('#histre-inline-wrap');
      this.autoHideInlineUI = () => {
        clearTimeout(this.timeout);
        this.inlineWrap.classList.remove('histre-inline-hide-on-yt');
        this.timeout = setTimeout(() => {
          this.inlineWrap.classList.add('histre-inline-hide-on-yt');
        }, 3000);
      };
      this.fullscreenChangeHandler = () => {
        if (document.fullscreenElement) {
          if (!this.inlineWrap) {
            // Wait until inline UI is loaded
            const interval = setInterval(() => {
              if (this.inlineWrap) {
                clearInterval(interval);
                this.fullscreenChangeHandler();
                return;
              }
              this.inlineWrap = document.querySelector('#histre-inline-wrap');
            }, 500);
            return;
          }
          this.timeout = setTimeout(() => {
            this.inlineWrap.classList.add('histre-inline-hide-on-yt');
          }, 3000);
          document.body.addEventListener('mousemove', this.autoHideInlineUI, true);
        } else {
          if (!this.inlineWrap) {
            return;
          }
          clearTimeout(this.timeout);
          this.inlineWrap.classList.remove('histre-inline-hide-on-yt');
          document.body.removeEventListener('mousemove', this.autoHideInlineUI, true);
        }
      };
    })();

    document.addEventListener('fullscreenchange', youtubeFullscreen.fullscreenChangeHandler);
    // If user has already entered fullscreen before event listener is set up
    if (document.fullscreenElement) {
      youtubeFullscreen.fullscreenChangeHandler();
    }
  }

  function removeYoutubeFullscreen() {
    if (!youtubeFullscreen) return;

    document.removeEventListener('fullscreenchange', youtubeFullscreen.fullscreenChangeHandler);
    youtubeFullscreen = null;
  }

  function removeURLFragment(url) {
    const urlObj = new URL(url);
    return urlObj.origin + urlObj.pathname + urlObj.search;
  }

  function checkUrl(url) {
    if (!url) {
      return true;
    }
    return removeURLFragment(location.href) === removeURLFragment(url);
  }

  const $css = $e('link', {
    rel: 'stylesheet',
    type: 'text/css',
    href: chrome.runtime.getURL('inline.css'),
  });

  const $drag = $e(
    'div',
    { id: prefix('drag'), class: prefix('icon icon-drag') },
    { mousedown: startInlineDrag },
  );

  const $logoImg = $e('img', {
    id: prefix('logo-img'),
    src: chrome.runtime.getURL('assets/inline/logo.svg'),
    alt: 'Histre logo',
  });
  const $logoTxt = $e('div', { class: prefix('txt') }, noEvents, [$t('histre')]);
  const $logo = $e('div', { id: prefix('logo'), class: prefix('logo') }, { click: openHistre }, [
    $logoImg,
    $logoTxt,
  ]);

  const $savePrivately = $e(
    'div',
    {
      class: prefix('icon icon-save-privately'),
      title: 'Save Privately',
      'aria-label': 'Save Privately',
      'data-histre-save-privately': '',
    },
    { click: handleSavePrivatelyBtn },
  );

  const $publish = $e(
    'div',
    {
      class: prefix('icon icon-publish'),
      title: 'Publish',
      'aria-label': 'Publish',
      'data-histre-read-later': '',
    },
    { click: handlePublishBtn },
  );

  const $readLater = $e(
    'div',
    {
      class: prefix('icon icon-read-later'),
      title: 'Read later',
      'aria-label': 'Read later',
      'data-histre-read-later': '',
    },
    { click: toggleReadLater },
  );

  const $settings = $e(
    'div',
    { id: prefix('settings'), class: prefix('icon icon-settings') },
    { click: openSettings },
  );

  const $maximize = $e(
    'div',
    { id: prefix('down'), class: prefix('icon icon-sidebar') },
    { click: openSidebar },
  );

  const $popup = $e(
    'div',
    { class: prefix('popup hide') },
    { mouseenter: popupMouseEnterHandler, mouseleave: popupMouseLeaveHandler },
    [$t('Some text')],
  );

  const $wrap = $e(
    'div',
    {
      id: prefix('wrap'),
      style: 'display: none;', // prevent some strange interactions until styles are loaded
      'data-histre-element': '',
    },
    noEvents,
    [$drag, $logo, $savePrivately, $publish, $readLater, $settings, $maximize, $popup],
  );

  histreDOMHelper.loadFont();

  clearOld(() => {
    chrome.storage.local.get(['inlinePosTop', 'inlinePosLeft', 'inlineIsShown'], (result) => {
      if (
        typeof result.inlinePosTop !== 'undefined' &&
        typeof result.inlinePosLeft !== 'undefined'
      ) {
        $wrap.style.top = result.inlinePosTop;
        $wrap.style.left = result.inlinePosLeft;
      }

      if (!result.inlineIsShown) {
        $wrap.classList.add(prefix('d-none'));
      }

      (document.head || document.documentElement).appendChild($css);
      document.body.appendChild($wrap);
      chrome.runtime.onMessage.addListener(onMessageHandler);
    });
  });

  return true;
})();
