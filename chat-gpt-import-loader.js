(() => {
  if (window.histreLoadedChatGPTImport) {
    return true;
  }
  window.histreLoadedChatGPTImport = true;

  // For Firefox
  if (typeof browser === 'object') {
    chrome = browser;
  }

  return false;
})();
