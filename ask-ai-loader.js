(() => {
  if (window.histreLoadedAskAI) {
    return true;
  }
  window.histreLoadedAskAI = true;

  // For Firefox
  if (typeof browser === 'object') {
    chrome = browser;
  }

  return false;
})();
