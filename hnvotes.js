// do not use console.log or helper.llog here

(function () {
  if (window.histre_hnvotes_has_run) {
    return true;
  }
  window.histre_hnvotes_has_run = true;

  function insertAfter(el, referenceNode) {
    referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
  }

  function showVoted(data) {
    const { upvotes, book_links } = data;

    document.querySelectorAll('.histre-voters').forEach(function (el) {
      el.remove();
    });
    Object.keys(upvotes).forEach(function (key, index) {
      const q = `a[href='item?id=${key}']`;
      const els = document.querySelectorAll(q);
      const el = els.length ? els[els.length - 1] : null;
      if (el) {
        const newEl = document.createElement('span');
        newEl.setAttribute('class', 'histre-voters histre-hi');
        newEl.setAttribute('data-histre-element', '');
        let txtPrefix = document.createElement('span');
        txtPrefix.textContent = ' | upvoted by: ';
        newEl.appendChild(txtPrefix);
        for (let i = 0; i < upvotes[key].length; i++) {
          let hnuser;
          let username = upvotes[key][i];
          if (book_links[username]) {
            hnuser = document.createElement('A');
            hnuser.setAttribute('href', `https://histre.com/collections/${book_links[username]}/`);
            hnuser.setAttribute('class', 'histre-book-link histre-hi link-space');
            hnuser.textContent = username;
          } else {
            hnuser = document.createElement('span');
            hnuser.setAttribute('class', 'histre-book-link histre-hi link-space');
            hnuser.textContent = username;
          }
          newEl.appendChild(hnuser);
        }
        insertAfter(newEl, el);

        let prev_ranks = document.getElementsByClassName('histre-hn-rank');
        while (prev_ranks[0]) {
          prev_ranks[0].classList.remove('histre-hn-rank');
        }

        try {
          el.parentNode.parentNode.previousSibling
            .querySelector('.rank')
            .classList.add('histre-hn-rank', 'histre-hi');
        } catch (e) {
          // no op
        }
      }
    });
  }

  function initialize() {
    const css =
      '' + '.histre-hi { color: #c145ff !important; }' + '.link-space { margin-right:4px }';

    const style = document.createElement('style');
    style.tyle = 'text/css';
    style.appendChild(document.createTextNode(css));
    const ref = document.querySelector('script');
    ref.parentNode.insertBefore(style, ref);
  }

  const hn_listener = function (msg, sender, sendResponse) {
    if (msg.action == 'show_upvoted') {
      showVoted(msg.data);
    }
  };

  chrome.runtime.onMessage.addListener(hn_listener);

  // ------------------------------------------------------------

  initialize();
})();
