(function () {
  const histreDOMHelper = window.histreDOMHelper;
  const { $e, $t, noEvents } = histreDOMHelper;
  const prefix = (classNames) => histreDOMHelper.prefix('cmd', classNames);
  const host = window.histreDOMHelper.getHost();

  let cmdPaletteIsShown;

  let doubleShiftKey = 0;
  let selectedItemIndex = 0;

  const commands = [
    { title: 'Add Note / Open Sidebar', handler: showNote },
    { title: 'Toggle Minibar', handler: toggleInlineUI },
  ];

  // This command is conditionally added if the user is premium
  const askAICommandIndex = 4;
  const askAICommand = {
    title: 'Ask AI a question about this page',
    handler: showAskAI,
  };

  let shownCommands = commands;

  //----- UI elements -----//

  const $logoImg = $e('img', {
    src: chrome.runtime.getURL('assets/sidebar/logo.png'),
    alt: 'Histre logo',
  });
  const $logoTxt = $e('div', {}, noEvents, [$t('histre')]);
  const $logo = $e(
    'div',
    {
      class: prefix('logo'),
      title: 'Open Histre',
      'aria-label': 'Open Histre',
    },
    { click: openHistre },
    [$logoImg, $logoTxt],
  );
  const $settings = $e('div', { class: prefix('settings') }, { click: openSettings });
  const $header = $e('div', { class: prefix('header') }, noEvents, [$logo, $settings]);

  const $input = $e(
    'input',
    {
      class: prefix('search-input'),
      placeholder: 'Type a command or search...',
      type: 'search',
      autocomplete: 'off',
    },
    { keydown: inputKeydownHandler, input: inputSearchHandler },
  );

  const $commands = $e('div', { class: prefix('commands-wrap') });

  const $content = $e('div', { class: prefix('content') }, noEvents, [$header, $input, $commands]);

  const $wrap = $e(
    'div',
    {
      id: prefix('wrap'),
      class: prefix('wrap'),
      style: 'display: none;', // prevent some strange interactions until styles are loaded
      'data-histre-element': '',
    },
    { click: handleOverlayClick },
    [$content],
  );

  const $css = $e('link', {
    rel: 'stylesheet',
    type: 'text/css',
    href: chrome.runtime.getURL('cmd-palette.css'),
  });

  //----- UI functionality -----//

  histreDOMHelper.loadFont();

  function openUI() {
    if ($input.value !== '') {
      shownCommands = commands;
      $input.value = '';
      renderCommands();
    } else if (selectedItemIndex !== 1) {
      changeSelectCmd(1, selectedItemIndex);
    }

    $wrap.classList.add(prefix('active'));
    $input.focus();

    chrome.runtime.sendMessage({
      action: 'sendCMDFeatureLog',
      dest: 'cmd',
      event: 'cmd-palette-open',
    });
  }

  function closeUI() {
    $wrap.classList.remove(prefix('active'));
  }

  function nukeSelf() {
    window.histreLoadedCmd = false;
    clearOld();
    chrome.runtime.onMessage.removeListener(onMessageHandler);
    chrome.storage.onChanged.removeListener(onStorageChangedHandler);
  }

  function clearOld(cb) {
    while (document.getElementById(prefix('wrap'))) {
      document.getElementById(prefix('wrap')).remove();
    }

    while (document.querySelector(`link[href="${chrome.runtime.getURL('cmd-palette.css')}"]`)) {
      document.querySelector(`link[href="${chrome.runtime.getURL('cmd-palette.css')}"]`).remove();
    }

    document.removeEventListener('keyup', doubleShiftListener);

    if (cb) {
      cb();
    }
  }

  function openHistre() {
    window.open(host);
  }

  function openSettings() {
    //? Opening settings page doesn't work with inline script
    chrome.runtime.sendMessage({ action: 'openOptionsPage', dest: 'cmd' });
  }

  //----- Rendering/selecting -----//

  function changeSelectCmd(index, prevIndex) {
    selectedItemIndex = index;
    if (prevIndex) {
      $wrap
        .querySelector(`[data-histre-cmd-index="${prevIndex}"]`)
        .classList.remove(prefix('selected'));
    }
    if (index === 0) {
      return;
    }
    $wrap.querySelector(`[data-histre-cmd-index="${index}"]`).classList.add(prefix('selected'));
  }

  function renderCommands() {
    $commands.textContent = '';
    if (shownCommands.length > 0) {
      shownCommands.forEach((command, index) => {
        const $cmd = $e(
          'div',
          { class: prefix('command'), 'data-histre-cmd-index': index + 1 },
          {
            click() {
              closeUI();
              command.handler();
            },
            mousemove: handleMouseHover,
          },
          [$t(command.title)],
        );
        $commands.appendChild($cmd);
      });
      changeSelectCmd(1);
    }
  }

  function executeSelectedCommand() {
    if (selectedItemIndex > shownCommands.length) {
      return;
    }
    closeUI();
    shownCommands[selectedItemIndex - 1].handler();
  }

  function addAskAICommand() {
    // ? temp disabled
    return;
    if (commands.findIndex((command) => command.title === askAICommand.title) >= 0) {
      return;
    }
    commands.splice(askAICommandIndex, 0, askAICommand);
    renderCommands();
  }

  function removeAskAICommand() {
    if (commands[askAICommandIndex].title !== askAICommand.title) {
      return;
    }
    commands.splice(askAICommandIndex, 1);
    renderCommands();
  }

  //----- Event handlers -----//

  function handleMouseHover() {
    const cmdIndex = this.getAttribute('data-histre-cmd-index');
    if (selectedItemIndex != cmdIndex) {
      changeSelectCmd(cmdIndex, selectedItemIndex);
    }
  }

  function inputSearchHandler(e) {
    e.stopPropagation();
    const searchText = this.value.toLocaleLowerCase().trim();

    shownCommands = commands.filter((cmd) => cmd.title.toLocaleLowerCase().includes(searchText));
    renderCommands();
  }

  function handleOverlayClick(e) {
    if (e.target.closest(`.${prefix('content')}`)) {
      if (
        e.target.classList.contains(prefix('command')) ||
        e.target.classList.contains(prefix('search-input'))
      ) {
        return;
      }
      $input.focus();
    } else {
      closeUI();
    }
  }

  function inputKeydownHandler(e) {
    e.stopPropagation();

    if (e.key === 'Escape') {
      closeUI();
      return;
    }

    if (e.key === 'ArrowUp') {
      if (selectedItemIndex - 1 === 0) {
        changeSelectCmd(shownCommands.length, selectedItemIndex);
      } else {
        changeSelectCmd(selectedItemIndex - 1, selectedItemIndex);
      }
      return;
    }

    if (e.key === 'ArrowDown') {
      if (selectedItemIndex + 1 > shownCommands.length) {
        changeSelectCmd(1, selectedItemIndex);
      } else {
        changeSelectCmd(selectedItemIndex + 1, selectedItemIndex);
      }
      return;
    }

    if (e.key === 'Enter') {
      executeSelectedCommand();
      return;
    }
  }

  function doubleShiftListener(e) {
    if (!cmdPaletteIsShown) {
      return;
    }
    if (e.key === 'Shift') {
      doubleShiftKey += 1;
      if (doubleShiftKey > 1) {
        openUI();
      }
      setTimeout(() => {
        doubleShiftKey = 0;
      }, 300);
    }
  }

  //----- Messages -----//

  function onMessageHandler(message) {
    if (message.action === 'disableCmd') {
      nukeSelf();
      return;
    }
  }

  function onStorageChangedHandler(changes) {
    if (changes.cmdPaletteIsShown) {
      cmdPaletteIsShown = changes.cmdPaletteIsShown.newValue;
    }

    if (changes.userIsPremium) {
      if (changes.userIsPremium.newValue === true) {
        addAskAICommand();
      } else {
        removeAskAICommand();
      }
    }
  }

  //----- Commands -----//

  function vote(voteVal) {
    chrome.runtime.sendMessage({
      action: 'saveInlineVote',
      dest: 'inline',
      inline: {
        url: location.href,
        title: document.title,
        vote: voteVal,
      },
    });
  }

  function voteUp() {
    vote(1);
    chrome.runtime.sendMessage({
      action: 'sendCMDFeatureLog',
      dest: 'cmd',
      event: 'cmd-palette-vote-up',
    });
  }

  function voteDown() {
    vote(-1);
    chrome.runtime.sendMessage({
      action: 'sendCMDFeatureLog',
      dest: 'cmd',
      event: 'cmd-palette-vote-down',
    });
  }

  function removeVote() {
    vote(0);
    chrome.runtime.sendMessage({
      action: 'sendCMDFeatureLog',
      dest: 'cmd',
      event: 'cmd-palette-remove-vote',
    });
  }

  function showNote() {
    chrome.runtime.sendMessage({ action: 'openSidePanel', dest: 'sidebar' });
    chrome.runtime.sendMessage({
      action: 'sendCMDFeatureLog',
      dest: 'cmd',
      event: 'cmd-palette-open-sidebar',
    });
  }

  function toggleInlineUI() {
    chrome.storage.local.get(['inlineIsShown'], (result) => {
      const inlineIsShown = !result.inlineIsShown;
      chrome.storage.local.set({ inlineIsShown });
    });
    chrome.runtime.sendMessage({
      action: 'sendCMDFeatureLog',
      dest: 'cmd',
      event: 'cmd-palette-toggle-minibar',
    });
  }

  // TODO update
  function showAskAI() {
    // chrome.storage.local.get(['sidebarIsOpen', 'inlineIsShown'], (result) => {
    //   chrome.storage.local.set({ inlineIsShown: true }, () => {
    //     if (result.sidebarIsOpen && result.inlineIsShown) {
    //       chrome.runtime.sendMessage({ action: 'showAskAI', dest: 'cmd' });
    //     } else {
    //       chrome.runtime.sendMessage({ action: 'openSidebar', dest: 'cmd' }, () => {
    //         chrome.runtime.sendMessage({ action: 'showAskAI', dest: 'cmd' });
    //       });
    //     }
    //   });
    // });
    // chrome.runtime.sendMessage({
    //   action: 'sendCMDFeatureLog',
    //   dest: 'cmd',
    //   event: 'cmd-palette-show-ask-ai',
    // });
  }

  //----- Initial setup -----//

  clearOld(() => {
    chrome.storage.local.get(['cmdPaletteIsShown', 'userIsPremium'], (result) => {
      cmdPaletteIsShown = result.cmdPaletteIsShown;
      if (result.userIsPremium) {
        addAskAICommand();
      }
      renderCommands();
      (document.head || document.documentElement).appendChild($css);
      document.body.appendChild($wrap);
      document.addEventListener('keyup', doubleShiftListener);
    });
  });

  chrome.runtime.onMessage.addListener(onMessageHandler);
  chrome.storage.onChanged.addListener(onStorageChangedHandler);

  return true;
})();
