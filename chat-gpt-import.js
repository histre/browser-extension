(() => {
  const histreDOMHelper = window.histreDOMHelper;
  const { $e, $t, noEvents } = histreDOMHelper;
  const prefix = (classNames) => histreDOMHelper.prefix('chat-gpt-import', classNames);
  const host = window.histreDOMHelper.getHost();

  //----- DOMPurify setup -----//

  DOMPurify.addHook('afterSanitizeAttributes', (node) => {
    // allow target="_blank"
    if ('target' in node) {
      node.setAttribute('target', '_blank');
      node.setAttribute('rel', 'noopener noreferrer');
    }
  });

  //----- Functions -----//

  async function insertUI() {
    const $profileIcon = document.querySelector('[data-testid="profile-button"]');
    if (!$profileIcon) return;

    const $profileIconWrap = $profileIcon.closest('div');

    if (!location.pathname.startsWith('/c/')) {
      if ($profileIconWrap.getAttribute('data-histre-import-btn-is-set')) {
        $profileIconWrap.removeAttribute('data-histre-import-btn-is-set');
        $profileIconWrap.removeChild($wrap);
      }
      return;
    }

    if ($profileIconWrap.hasAttribute('data-histre-import-btn-is-set')) return;

    $profileIconWrap.setAttribute('data-histre-import-btn-is-set', '');

    $importBtn.setAttribute('disabled', '');
    displaySpinner();
    removeConversationIsImported();

    $profileIconWrap.insertBefore($wrap, $profileIconWrap.firstChild);

    const result = await chrome.runtime.sendMessage({
      action: 'getChatGTPConversation',
      conversationId: location.pathname.replace('/c/', ''),
    });

    if (result.error === false && result.data.conversation_id && result.data.book_id) {
      setConversationIsImported(result.data.book_id);
    }

    $importBtn.removeAttribute('disabled');
    hideSpinner();
  }

  async function importMessages() {
    if ($wrap.hasAttribute('disabled')) return;

    $importBtn.setAttribute('disabled', '');
    displaySpinner();
    const $messages = document.querySelectorAll('[data-message-id]');

    const messages = [];

    for (const [index, $message] of $messages.entries()) {
      const author =
        $message.getAttribute('data-message-author-role') === 'user' ? 'user' : 'chatgpt';
      let $markdown = $message.querySelector('.markdown');

      // Remove unnecessary elements
      if ($markdown) {
        $markdown = $markdown.cloneNode(true);

        const $preTags = $markdown.querySelectorAll('pre');

        for (const $preTag of $preTags) {
          const $codeTag = $preTag.querySelector('code');
          $preTag.innerHTML = '';
          $preTag.appendChild($codeTag);
        }
      }

      const text = $markdown ? $markdown.innerHTML : `<p>${$message.innerText}</p>`;

      messages.push({
        message_id: $message.getAttribute('data-message-id'),
        ordering: index,
        text: DOMPurify.sanitize(text),
        author,
      });
    }

    const convId = location.pathname.replace('/c/', '');
    const $title = document.querySelector(`[href="${location.pathname}"]`);
    const title = $title ? $title.innerText : null;

    const payload = {
      conversation_id: convId,
      title,
      chats: messages,
    };

    const result = await chrome.runtime.sendMessage({ action: 'saveChatGTPConversation', payload });

    if (result.error === false) {
      displayCheckmark();
      setConversationIsImported(result.data.book_id);
    } else {
      displayAlert();
    }

    $importBtn.removeAttribute('disabled');
  }

  function handleLogoClick() {
    if ($wrap.getAttribute('data-histre-book-id')) {
      window.open(`${host}/chats/${$wrap.getAttribute('data-histre-book-id')}/`, '_blank');
    } else {
      window.open(`${host}/chats/`, '_blank');
    }
  }

  function setConversationIsImported(bookId) {
    $wrap.setAttribute('data-histre-book-id', bookId);
    $logo.setAttribute('src', chrome.runtime.getURL('assets/inline/logo-filled.svg'));
  }

  function removeConversationIsImported() {
    $wrap.removeAttribute('data-histre-book-id');
    $logo.setAttribute('src', chrome.runtime.getURL('assets/inline/logo.svg'));
  }

  //----- Status Messages -----//

  function displaySpinner() {
    $spinner.style.display = 'block';
    $alert.style.display = 'none';
    $checkmark.style.display = 'none';
  }

  function hideSpinner() {
    $spinner.style.display = 'none';
  }

  function displayAlert() {
    $spinner.style.display = 'none';
    $alert.style.display = 'block';

    setTimeout(() => {
      $alert.style.display = 'none';
    }, 2000);
  }

  function displayCheckmark() {
    $spinner.style.display = 'none';
    $checkmark.style.display = 'block';

    setTimeout(() => {
      $checkmark.style.display = 'none';
    }, 2000);
  }

  //----- UI elements -----//

  const $logo = $e(
    'img',
    {
      id: prefix('logo'),
      class: prefix('logo'),
      src: chrome.runtime.getURL('assets/inline/logo.svg'),
      alt: 'Histre logo',
    },
    { click: handleLogoClick },
  );

  const $spinner = $e('div', { class: prefix('spinner') });
  const $alert = $e('div', { class: prefix('status status--alert') });
  const $checkmark = $e('div', { class: prefix('status status--checkmark') });
  const $statusWrap = $e('div', { class: prefix('status-wrap') }, noEvents, [
    $spinner,
    $alert,
    $checkmark,
  ]);

  const $importBtn = $e(
    'button',
    { class: prefix('btn btn-outline-primary import-btn') },
    { click: importMessages },
    [$t('Import')],
  );

  const $wrap = $e('div', { id: prefix('wrap'), class: prefix('wrap') }, noEvents, [
    $statusWrap,
    $logo,
    $importBtn,
  ]);

  const $css = $e('link', {
    rel: 'stylesheet',
    type: 'text/css',
    href: chrome.runtime.getURL('chat-gpt-import.css'),
  });

  //----- Setup -----//

  setInterval(() => {
    insertUI();
  }, 1000);

  document.addEventListener('locationchange', () => {
    insertUI();
  });

  histreDOMHelper.loadFont();
  (document.head || document.documentElement).appendChild($css);
})();
