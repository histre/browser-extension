import { openDB, deleteDB, wrap, unwrap } from '/idb.min.js';
import { host } from './globals.js';

export const histreOrigins = [host + '/'];

const manifest = chrome.runtime.getManifest();
const version = manifest.version;

export const delayUnitBetweenAPICalls = 200; // ms

// It returns a promise that needs to be evaluated on every call. It can't be evaluated here
// because in mv2 blocking event listener setups has a possibility of them not being registered,
// and in mv3 every event listener is be required to be set up synchronically, so top level awaits
// won't work.
export const db = openDB('storage', 2, {
  upgrade(db, oldVersion, newVersion, transaction, event) {
    if (oldVersion < 1) {
      const store = db.createObjectStore('history', { keyPath: 'id', autoIncrement: true });
      store.createIndex('tabId', 'tabId');
      db.createObjectStore('navfrom', { keyPath: 'tabId' });
    }

    if (oldVersion < 2) {
      const store = db.createObjectStore('note', { keyPath: 'url' });
      store.createIndex('tabIds', 'tabIds', { multiEntry: true });
    }
  },
});
