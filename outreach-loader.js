(() => {
  if (window.histreLoadedOutreach) {
    return true;
  }
  window.histreLoadedOutreach = true;

  // For Firefox
  if (typeof browser === 'object') {
    chrome = browser;
  }

  return false;
})();
