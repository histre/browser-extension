import { host, apiBase } from '/globals.js';
import { helper } from '/helper.js';
import { delayUnitBetweenAPICalls } from '/background-globals.js';

const highlightUrl = `${apiBase}/highlight/`;

const domainBlacklist = ['mail.google.com', 'docs.google.com', 'calendar.google.com', 'notion.so'];

// Convert some Chrome functions to async/await -- start
function hasPermissions(permissions = {}) {
  return new Promise((resolve) => {
    chrome.permissions.contains(permissions, (enabled) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(enabled);
    });
  });
}

function executeScript(tabId, details) {
  return new Promise((resolve) => {
    if (typeof tabId === 'number') {
      const injection = {
        ...details,
        target: { tabId },
      };

      chrome.scripting.executeScript(injection, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    } else {
      chrome.scripting.executeScript(tabId, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    }
  });
}

function sendMessage(tabId, message) {
  return new Promise((resolve) => {
    chrome.tabs.sendMessage(tabId, message, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}

function getLocalStorage(keys = null) {
  return new Promise((resolve) => {
    chrome.storage.local.get(keys, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}
// Convert some Chrome functions to async/await -- end

async function isHighlightsEnabled() {
  return (await chrome.storage.local.get(['featuresHighlights'])).featuresHighlights;
}

async function addHighlightsContentScript(tab) {
  if (!(await isHighlightsEnabled())) {
    return;
  }

  // ignore chrome:// about:config etc
  if (!tab || !tab.url || !(tab.url.indexOf('http') === 0)) {
    return;
  }

  const tabUrl = new URL(tab.url);
  const tabHostname = tabUrl.hostname;

  for (const domain of domainBlacklist) {
    if (tabHostname.includes(domain)) {
      return;
    }
  }

  const enabled = await hasPermissions({ origins: ['*://*/*'] });

  if (enabled) {
    const result = await executeScript(tab.id, { files: ['highlight-loader.js'] });
    if (!Array.isArray(result) || result.length <= 0) {
      //? Something went wrong
      helper.llog('Error loading highlights', tab, result);
      return;
    }
    const isHighlightLoaded = result[0].result;
    if (isHighlightLoaded === false) {
      await executeScript(tab.id, { files: ['mark.es6.min.js'] });
      await executeScript(tab.id, { files: ['sha1.min.js'] });
      await executeScript(tab.id, { files: ['dom-helper.js'] });
      await executeScript(tab.id, { files: ['highlight.js'] });
    }

    helper.doGet(`${highlightUrl}?url=${encodeURIComponent(tab.url)}`, {}, (result) => {
      if (result.error !== true) {
        let highlightsData = result.data;

        if (highlightsData?.highlights) {
          highlightsData = highlightsData.highlights;
        }

        if (Array.isArray(highlightsData) && highlightsData.length > 0) {
          sendMessage(tab.id, { action: 'showHighlights', data: highlightsData });
        }
      } else if (helper.isAuthErrorStatus(result.status)) {
        helper.setIconWarning();
        helper.handleAuthErrorsOnce();
      }
    });
  }
}

async function removeHighlightsContent(tab) {
  if (await isHighlightsEnabled()) {
    return;
  }

  // ignore chrome:// about:config etc
  if (!tab || !tab.url || !(tab.url.indexOf('http') === 0)) {
    return;
  }

  sendMessage(tab.id, { action: 'disableHighlights' });
}

function onMessageHandler(message, sender, sendResponse) {
  if (message.action === 'saveHighlight') {
    const data = {
      extra: {
        selection: message.highlight.selection,
      },
      url: message.highlight.url,
      title: message.highlight.title,
      text: message.highlight.text,
      color: message.highlight.color,
      tweet: message.highlight.tweet,
    };
    helper.doPost(highlightUrl, data, function (result) {
      if (result?.data?.redir_oauth) {
        chrome.tabs.create({ url: `${host}${result.data.redir_oauth}` });
      }
      if (result?.errmsg === helper.errmsgs.urlTooLong) {
        result['usermsg'] = `Cannot save highlights: URL over ${helper.MAX_URL_LEN} chars.`;
      }
      sendResponse(result);
    });
    // Ref: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/onMessage
    // See: Sending an asynchronous response using sendResponse
    // "Note return true; in the listener: this tells the browser that you intend to use the sendResponse argument after the listener has returned."
    return true;
  }
  if (message.action === 'updateHighlight') {
    const data = {
      highlight_id: message.highlight.id,
      color: message.highlight.color,
    };
    if (typeof message.highlight.note === 'string') {
      data.note = message.highlight.note;
    }
    helper.doPatch(highlightUrl, data, function (result) {
      sendResponse(result);
    });
    // Ref: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/onMessage
    // See: Sending an asynchronous response using sendResponse
    // "Note return true; in the listener: this tells the browser that you intend to use the sendResponse argument after the listener has returned."
    return true;
  }
  if (message.action === 'deleteHighlight') {
    const data = {
      highlight_id: message.highlight.id,
    };
    helper.doDelete(highlightUrl, data, function (result) {
      cb(result);
    });
    // Ref: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/onMessage
    // See: Sending an asynchronous response using sendResponse
    // "Note return true; in the listener: this tells the browser that you intend to use the sendResponse argument after the listener has returned."
    return true;
  }
  if (message.action === 'setUserInfo' && message.dest === 'highlights') {
    helper.doGet(`${apiBase}/settings/`, {}, (result) => {
      if (result.data?.username && result.data?.external_id) {
        chrome.storage.local.set(
          {
            userUsername: result.data.username,
            userExternalId: result.data.external_id,
          },
          () => {
            sendResponse({ isSet: true });
          },
        );
      } else {
        sendResponse({ isSet: false });
      }
    });
    return true;
  }
}

async function onUpdatedHandler(tabId, changeInfo, tab) {
  if (!(await isHighlightsEnabled())) {
    return;
  }

  if (changeInfo.status === 'complete') {
    addHighlightsContentScript(tab);
  }
}

function onStorageChangedHandler(changes) {
  if (changes.featuresHighlights) {
    if (changes.featuresHighlights.newValue === true) {
      enableHighlights();
    } else {
      disableHighlights();
    }
  }
}

function refreshFromStorage() {
  chrome.storage.local.get(['featuresHighlights'], (result) => {
    if (typeof result.featuresHighlights !== 'undefined') {
      if (result.featuresHighlights) {
        enableHighlights();
      } else {
        disableHighlights();
      }
    }
  });
}

function enableHighlights() {
  chrome.tabs.query({ url: '*://*/*' }, function (tabs) {
    tabs.forEach((tab, i) => {
      setTimeout(() => {
        addHighlightsContentScript(tab);
      }, i * delayUnitBetweenAPICalls);
    });
  });
}

function disableHighlights() {
  chrome.tabs.query({ url: '*://*/*' }, function (tabs) {
    tabs.forEach((tab, i) => {
      setTimeout(() => {
        removeHighlightsContent(tab);
      }, i * delayUnitBetweenAPICalls);
    });
  });
}

function histreContextMenuOnClickHandler(info, tab) {
  chrome.scripting.executeScript(
    {
      files: ['range.js'],
      target: { tabId: tab.id },
    },
    function (selection) {
      var data = {
        extra: {
          selection: [selection[0].result],
        },
        url: tab.url,
        title: tab.title,
        text: info.selectionText,
      };
      helper.doPost(highlightUrl, data);
    },
  );
  if (chrome.runtime.lastError) {
  }
}

function addHistreContextMenu() {
  chrome.contextMenus.create({
    id: 'save-highlight',
    title: 'Save highlight to histre',
    contexts: ['selection'],
  });
}

chrome.contextMenus.onClicked.addListener((info, tab) => {
  if (info.menuItemId === 'save-highlight') {
    histreContextMenuOnClickHandler(info, tab);
  }
});
chrome.tabs.onUpdated.addListener(onUpdatedHandler);
chrome.runtime.onMessage.addListener(onMessageHandler);
chrome.storage.onChanged.addListener(onStorageChangedHandler);
chrome.runtime.onStartup.addListener(() => {
  addHistreContextMenu();
  refreshFromStorage();
});
chrome.runtime.onInstalled.addListener(() => {
  addHistreContextMenu();
  refreshFromStorage();
});
