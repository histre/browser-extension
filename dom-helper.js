(() => {
  if (typeof window.histreDOMHelper === 'object') {
    return;
  }

  window.histreDOMHelper = {
    noEvents: {},
    noChildren: [],
    $e(tag = 'div', props = {}, events = {}, children = []) {
      // Adapted from: https://news.ycombinator.com/item?id=23578319
      // Had to use setAttribute instead of Object.assign
      const elem = document.createElement(tag);
      for (const prop in props) {
        elem.setAttribute(prop, props[prop]);
      }
      elem.append(...children);
      for (const event in events) {
        elem.addEventListener(event, events[event]);
      }
      return elem;
    },
    $t: document.createTextNode.bind(document),
    prefix(prefix, classNames) {
      const classes = classNames.split(' ');
      for (let i in classes) {
        classes[i] = `histre-${prefix}-${classes[i]}`;
      }
      return classes.join(' ');
    },
    getHost() {
      // ENV : local | stg | prod
      const ENV = 'local';

      let host = 'https://histre.com';
      if (ENV === 'local') {
        host = 'http://local.histre.com:8080';
      } else if (ENV === 'stg') {
        host = 'https://cfgromit.histre.com';
      }

      return host;
    },
    loadFont() {
      return new Promise((resolve) => {
        const font = new FontFace(
          'Cerebri Sans',
          `url(${chrome.runtime.getURL('theme/fonts/cerebrisans/cerebrisans-regular.woff')})`,
        );

        font.load().then(
          async () => {
            await document.fonts.ready;
            const isLoaded = document.fonts.check('15px "Cerebri Sans"');
            if (!isLoaded) {
              document.fonts.add(font);
            }
            resolve({ error: false });
          },
          (error) => {
            resolve({ error: true, info: error });
          },
        );
      });
    },
  };
})();
