import { apiBase } from '/globals.js';
import { helper } from '/helper.js';
import { delayUnitBetweenAPICalls, db } from '/background-globals.js';
import { addInlineContentScript } from '/background-inline.js';

const noteUrl = `${apiBase}/note/`;
const extractUrl = `${apiBase}/extract/`;
const booksUrl = `${apiBase}/collections/`;
const removeNoteUrl = `${booksUrl}remove_note/`;
const voteUrl = `${apiBase}/votes/`;

const authErrorMessage = 'Unable to authenticate with histre.com';

// Convert some Chrome functions to async/await -- start
function hasPermissions(permissions = {}) {
  return new Promise((resolve) => {
    chrome.permissions.contains(permissions, (enabled) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(enabled);
    });
  });
}

function executeScript(tabId, details) {
  return new Promise((resolve) => {
    if (typeof tabId === 'number') {
      const injection = {
        ...details,
        target: { tabId },
      };

      chrome.scripting.executeScript(injection, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    } else {
      chrome.scripting.executeScript(tabId, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    }
  });
}

function sendMessage(tabId, message) {
  return new Promise((resolve) => {
    chrome.tabs.sendMessage(tabId, message, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}

function getLocalStorage(keys = null) {
  return new Promise((resolve) => {
    chrome.storage.local.get(keys, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}

function setLocalStorage(keys = null) {
  return new Promise((resolve) => {
    chrome.storage.local.set(keys, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}

function getTab(tabId) {
  return new Promise((resolve) => {
    chrome.tabs.get(tabId, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}
// Convert some Chrome functions to async/await -- end

function handleAuthError() {
  helper.setIconWarning();
  helper.handleAuthErrorsOnce();
}

function onMessageHandler(message, sender, sendResponse) {
  if (message.action === 'getNote') {
    (async () => {
      const { userCacheNotes } = await getLocalStorage('userCacheNotes');
      let note;

      if (userCacheNotes !== false) {
        try {
          note = await (await db).get('note', helper.removeURLFragment(message.tab.url, false));
        } catch (e) {
          // IndexedDB is disabled
        }
      }

      // Skip caching for now if url is content addressable
      if (note && !message.isAddressableUrl && userCacheNotes !== false) {
        chrome.storage.local.get(
          ['userLogHistory', 'userAccessLevels', 'userAccessDefault', 'userIsPremium'],
          (result) => {
            const data = {
              data: note,
              error: false,
              dataExtraction: note.dataExtraction,
              settings: {
                history: result.userLogHistory,
                access_default: result.userAccessDefault,
                access_levels: JSON.parse(result.userAccessLevels),
                premium_user: result.userIsPremium,
              },
            };

            sendResponse(data);
          },
        );
      } else {
        try {
          const result = await getNote(message.tab, message.isAddressableUrl);
          sendResponse(result);
        } catch (e) {
          sendResponse({ error: true });
        }
      }
    })();
    return true;
  }

  if (message.action === 'saveNote') {
    (async () => {
      const result = await saveNote(message.data);
      sendResponse(result);
    })();
    return true;
  }

  if (message.action === 'appendToNote') {
    message.data.note.url ??= sender.tab.url;
    if (message.forOutreach !== true) {
      message.data.note.title ??= sender.tab.title;
    }
    message.data.tabId ??= sender.tab.id;
    message.data.mode ??= 'true';
    message.data.attrpriority ??= 'new';

    (async () => {
      if (!message.data.note.note_modified) {
        const { userCacheNotes } = await getLocalStorage('userCacheNotes');
        if (userCacheNotes) {
          try {
            const note = await (
              await db
            ).get('note', helper.removeURLFragment(message.data.note.url, false));

            if (note?.note_modified) {
              message.data.note.note_modified = note.note_modified;
            }
          } catch (e) {
            // IndexedDB is disabled
          }
        }
      }

      const result = await appendToNote(message.data);
      sendResponse(result);
    })();

    return true;
  }

  if (message.action === 'deleteNote') {
    message.data.tabId ??= sender.tab.id;
    message.data.tabUrl ??= message.data.payload?.url ?? sender.tab.url;

    (async () => {
      const result = await deleteNote(message.data);
      sendResponse(result);
    })();
    return true;
  }

  if (message.action === 'createBook') {
    (async () => {
      const result = await createBook(message.data, message.url);
      sendResponse(result);
    })();
    return true;
  }

  if (message.action === 'removeNoteFromBooks') {
    (async () => {
      if (!message.data.url) {
        const tab = (await chrome.tabs.query({ active: true, lastFocusedWindow: true }))[0];
        message.data.url = tab.url;
      }
      const result = await removeNoteFromBooks(message.data);
      sendResponse(result);
    })();
    return true;
  }

  if (message.action === 'saveVote') {
    (async () => {
      const result = await saveVote(message.data);
      sendResponse(result);
    })();
    return true;
  }
}

function tabUpdatedHandler(tabId, changeInfo, tab) {
  // Exception for docs.google
  const tabUrl = new URL(tab.url);
  if (tabUrl.hostname === 'docs.google.com') {
    if (changeInfo.status === 'complete') {
      getNote(tab);
    }
    return;
  }

  // Only fire once, when tab is changing
  if (changeInfo.status === 'loading') {
    getNote(tab);
  }
}

async function onTabRemovedHandler(tabId) {
  const { userCacheNotes } = await getLocalStorage('userCacheNotes');

  if (userCacheNotes !== false) {
    try {
      const tx = (await db).transaction('note', 'readwrite');
      const store = tx.store;
      const oldNote = await store.index('tabIds').get(IDBKeyRange.only(tabId));
      if (oldNote) {
        oldNote.tabIds = oldNote.tabIds.filter((oldTabId) => oldTabId !== tabId);
        if (oldNote.tabIds.length <= 0) {
          await store.delete(oldNote.url);
        } else {
          await store.put(oldNote);
        }
      }
      await tx.done;
    } catch (e) {
      // IndexedDB is disabled
    }
  }
}

export async function getNote(tab, isAddressableUrl = false) {
  const { featuresInline } = await getLocalStorage('featuresInline');
  if (featuresInline) {
    await addInlineContentScript(tab);
  }

  const activeTab = (await chrome.tabs.query({ active: true, lastFocusedWindow: true }))[0];
  if (activeTab.id === tab.id) {
    chrome.runtime.sendMessage({
      action: 'loading',
      dest: 'sidePanel',
      url: tab.url,
    });
  }

  const url = `${noteUrl}?url=${encodeURIComponent(tab.url)}${
    isAddressableUrl ? '&addressable=1' : ''
  }`;

  const response = await fetch(url, helper.setRequestParams('GET'));
  const result = await response.json();

  if (result.error !== true) {
    if (featuresInline) {
      if (result.data.note_modified) {
        sendMessage(tab.id, {
          action: 'setNoteIsPresent',
          noteModified: result.data.note_modified,
          noteId: result.data.item_id,
          url: tab.url,
        });
      }
      if (result.data.read_later_book_id) {
        sendMessage(tab.id, {
          action: 'setInlineReadLater',
          bookId: result.data.read_later_book_id,
          noteId: result.data.item_id,
          url: tab.url,
        });
      }
      if (result.data.access === 'private') {
        sendMessage(tab.id, {
          action: 'setSavePrivatelySaved',
          noteId: result.data.item_id,
          url: tab.url,
        });
      }
      if (result.data.access === 'public') {
        sendMessage(tab.id, {
          action: 'setPublishNotePublished',
          noteId: result.data.item_id,
          url: tab.url,
        });
      }
    }

    if (result.data.note_modified) {
      helper.setNoteIsPresentIcon(tab.id, tab.url);
    } else {
      helper.setDefaultIcon(tab.id, tab.url);
    }

    const tabUrlParsed = new URL(tab.url);
    if (!result.data.attrs) {
      if (helper.isDataExtractable(tab.url)) {
        const hasPermission = await hasPermissions({ origins: ['*://*/*'] });
        if (hasPermission) {
          await executeScript(tab.id, { files: ['extractable.js'] });
          const dataExtraction = await sendMessage(tab.id, { extractable: tab.url });
          result.dataExtraction = dataExtraction?.result;
          result.dataExtraction.isExtractableSite = true;
        }
      } else if (
        tabUrlParsed.hostname === 'www.linkedin.com' &&
        tabUrlParsed.pathname.indexOf('/in/') === 0
      ) {
        const linkedInResponse = await fetch(
          `${extractUrl}?url=${encodeURIComponent(tab.url)}`,
          helper.setRequestParams('GET'),
        );
        const linkedInResult = await linkedInResponse.json();
        if (!linkedInResult.error) {
          result.dataExtraction = {
            data: linkedInResult.data,
          };
        }
      }
    } else {
      result.dataExtraction = {
        data: result.data.attrs,
      };
    }

    if (!result.data.page_source_hash_modified && result.settings.deep_search === true) {
      hasPermissions({ origins: [tab.url] }).then((hasPermission) => {
        if (hasPermission) {
          executeScript(tab.id, {
            files: ['extract-page-source.js'],
            // runAt: 'document_end',
          });
        }
      });
    }

    if (result.settings.cache_notes !== false) {
      try {
        const urlIndex = helper.removeURLFragment(tab.url, false);

        const tx = (await db).transaction('note', 'readwrite');
        const store = tx.store;

        const oldNote = await store.index('tabIds').get(IDBKeyRange.only(tab.id));
        if (oldNote) {
          oldNote.tabIds = oldNote.tabIds.filter((tabId) => tabId !== tab.id);
          if (oldNote.tabIds.length <= 0) {
            await store.delete(oldNote.url);
          } else {
            await store.put(oldNote);
          }
        }

        let newNote = await store.get(urlIndex);
        if (newNote) {
          newNote = {
            ...newNote,
            ...result.data,
          };
          newNote.tabIds.push(tab.id);
        } else {
          newNote = {
            ...result.data,
            url: helper.removeURLFragment(tab.url, false),
            tabIds: [tab.id],
          };
        }
        newNote.dataExtraction = result.dataExtraction;
        await store.put(newNote);
        await tx.done;
      } catch (e) {
        // IndexedDB is disabled
      }
    }

    // ? Set relevant settings here, and don't forget to load them in onMessageHandler
    await setLocalStorage({
      userLogHistory: result.settings.history,
      userAccessLevels: JSON.stringify(result.settings.access_levels),
      userAccessDefault: result.settings.access_default,
      userCacheNotes: result.settings.cache_notes,
      userIsPremium: result.settings.premium_user,
    });

    if (activeTab.id === tab.id) {
      chrome.runtime.sendMessage({
        action: 'setupNote',
        dest: 'sidePanel',
        result,
        tab,
        url: tab.url,
      });
    }
  } else if (helper.isAuthErrorStatus(result.status)) {
    handleAuthError();
    chrome.runtime.sendMessage({
      action: 'displaySetupError',
      dest: 'sidePanel',
      message: authErrorMessage,
      url: tab.url,
    });
  } else {
    chrome.runtime.sendMessage({
      action: 'displaySetupError',
      dest: 'sidePanel',
      message: 'An error occurred',
      url: tab.url,
    });
  }

  return result;
}

export async function saveNote(data = {}) {
  const { note, removeNoteFromBooks, tabId, newBook } = data;

  const response = await fetch(noteUrl, helper.setRequestParams('POST', [note])); // must be an array
  const result = await response.json();

  if (result.error === false) {
    if (result.data.conflict) {
      const senderTab = await getTab(tabId);
      const newNote = await getNote(senderTab);
      result.newNote = newNote;

      chrome.runtime.sendMessage({
        action: 'setupNote',
        dest: 'sidePanel',
        result: newNote,
        tab: senderTab,
        url: senderTab.url,
      });
    } else {
      const noteId = result.data.item_id;

      if (result.settings.cache_notes !== false) {
        try {
          const url = helper.removeURLFragment(note.url, true);
          const tx = (await db).transaction('note', 'readwrite');
          const noteEntry = await tx.store.get(url);

          noteEntry.note = note.note;
          noteEntry.item_id = noteId;
          noteEntry.note_modified = result.data.note_modified;
          noteEntry.access = note.access;
          noteEntry.attrs = note.extracted_attrs || null;

          if (!noteEntry.title) {
            noteEntry.title = note.title;
          } else {
            if (note.user_title) {
              noteEntry.title = note.user_title;
            }
          }

          if (note.book_ids?.length > 0 || removeNoteFromBooks?.length > 0) {
            let bookIdsCopy =
              Array.isArray(note.book_ids) && note.book_ids.length > 0 ? [...note.book_ids] : null;
            let deselectedBookIdsCopy =
              Array.isArray(removeNoteFromBooks) && removeNoteFromBooks?.length > 0
                ? [...removeNoteFromBooks]
                : null;

            for (let i = 0; i < noteEntry.recent_books.length; i++) {
              const bookId = noteEntry.recent_books[i].book_id;
              if (bookIdsCopy?.length > 0 && bookIdsCopy.includes(bookId)) {
                noteEntry.recent_books[i].current_url_item_id = noteId;
                bookIdsCopy = bookIdsCopy.filter((item) => item !== bookId);
              }
              if (deselectedBookIdsCopy?.length > 0 && deselectedBookIdsCopy.includes(bookId)) {
                delete noteEntry.recent_books[i].current_url_item_id;
                deselectedBookIdsCopy = deselectedBookIdsCopy.filter((item) => item !== bookId);
              }
              if (!(bookIdsCopy?.length > 0) && !(deselectedBookIdsCopy?.length > 0)) {
                break;
              }
            }
          }

          if (note.read_later && result.data.read_later_book_id) {
            noteEntry.read_later_book_id = result.data.read_later_book_id;

            for (let i = 0; i < noteEntry.recent_books.length; i++) {
              if (noteEntry.recent_books[i].book_id === result.data.read_later_book_id) {
                noteEntry.recent_books[i].current_url_item_id = noteId;
                break;
              }
            }
          }

          await tx.store.put(noteEntry);
          await tx.done;
        } catch (e) {
          // IndexedDB is disabled
        }
      }
    }

    if (result.data.read_later_book_id) {
      const { featuresInline } = await chrome.storage.local.get('featuresInline');
      if (featuresInline) {
        chrome.tabs.query({ url: helper.removeURLFragment(note.url, true) }, (tabs) => {
          tabs.forEach((tab) => {
            sendMessage(tab.id, {
              action: 'setInlineReadLater',
              noteId: result.data.item_id,
              bookId: result.data.read_later_book_id,
              url: tab.url,
            });
          });
        });
      }
    }

    if (note.access) {
      const { featuresInline } = await chrome.storage.local.get('featuresInline');

      chrome.runtime.sendMessage({
        action: 'setAccessLevel',
        dest: 'sidePanel',
        accessLevel: note.access,
        url: decodeURIComponent(note.url),
      });

      if (featuresInline) {
        chrome.tabs.query({ url: helper.removeURLFragment(note.url, true) }, (tabs) => {
          tabs.forEach((tab) => {
            if (note.access === 'public') {
              sendMessage(tab.id, {
                action: 'setPublishNotePublished',
                noteId: result.data.item_id,
                url: tab.url,
              });
            } else if (note.access === 'private') {
              sendMessage(tab.id, {
                action: 'setSavePrivatelySaved',
                noteId: result.data.item_id,
                url: tab.url,
              });
            } else {
              sendMessage(tab.id, {
                action: 'removeSavePrivatelySaved',
                url: tab.url,
              });
              sendMessage(tab.id, {
                action: 'removePublishNotePublished',
                url: tab.url,
              });
            }
          });
        });
      }
    }

    // Send back note for auto-save optimization - note log
    result.note = {
      ...note,
    };
  } else {
    if (
      result.status === helper.HttpCodes.badRequest &&
      result.errmsg === helper.errmsgs.urlTooLong
    ) {
      result.errorMessage = `Unable to save note: URL over ${helper.MAX_URL_LEN} chars.`;
    } else if (helper.isAuthErrorStatus(result.status)) {
      handleAuthError();
      result.errorMessage = authErrorMessage;
    } else {
      result.errorMessage = `An error occurred while saving note.`;
    }
  }

  return result;
}

async function appendToNote(data) {
  const { note, tabId, mode = 'true', attrpriority = 'new' } = data;

  const response = await fetch(
    `${noteUrl}?append=${mode}&attrpriority=${attrpriority}`,
    helper.setRequestParams('POST', [note]),
  ); // must be an array
  const result = await response.json();

  if (result.error === false) {
    if (result.data.conflict) {
      const senderTab = await getTab(tabId);
      const newNote = await getNote(senderTab);
      result.newNote = newNote;

      chrome.runtime.sendMessage({
        action: 'setupNote',
        dest: 'sidePanel',
        result: newNote,
        tab: senderTab,
        url: senderTab.url,
      });
    } else {
      const noteId = result.data.item_id;

      if (result.settings.cache_notes !== false && result.data.appended !== false) {
        try {
          const url = helper.removeURLFragment(note.url, true);
          const tx = (await db).transaction('note', 'readwrite');
          const noteEntry = await tx.store.get(url);

          if (noteEntry.note && noteEntry.note !== '<p><br></p>') {
            noteEntry.note = `${noteEntry.note}<p><br></p><p><br></p>${note.note}`;
          } else {
            noteEntry.note = `${note.note}`;
          }
          noteEntry.item_id = noteId;
          noteEntry.note_modified = result.data.note_modified;

          await tx.store.put(noteEntry);
          await tx.done;
        } catch (e) {
          // IndexedDB is disabled
        }
      }

      chrome.storage.local.get('featuresInline', async (storageResult) => {
        chrome.tabs.query({ url: helper.removeURLFragment(note.url, false) }, (tabs) => {
          tabs.forEach((tab) => {
            helper.setNoteIsPresentIcon(tab.id, tab.url);

            if (storageResult.featuresInline) {
              sendMessage(tab.id, { action: 'setNoteIsPresent', noteId });
            }
          });
        });
      });

      chrome.runtime.sendMessage({
        action: 'appendTextToNote',
        dest: 'sidePanel',
        data: {
          note: result.data.appended === false ? '' : note.noteText,
          noteModified: result.data.note_modified,
          noteId,
        },
        url: note.url,
      });
    }
  } else {
    if (
      result.status === helper.HttpCodes.badRequest &&
      result.errmsg === helper.errmsgs.urlTooLong
    ) {
      result.errorMessage = `Unable to save note: URL over ${helper.MAX_URL_LEN} chars.`;
    } else if (helper.isAuthErrorStatus(result.status)) {
      handleAuthError();
      result.errorMessage = authErrorMessage;
    } else {
      result.errorMessage = `An error occurred while saving note.`;
    }
  }

  return result;
}

async function deleteNote(data) {
  const { payload, tabUrl, tabId } = data;

  const response = await fetch(noteUrl, helper.setRequestParams('DELETE', payload));
  const result = await response.json();
  if (result.error === false) {
    if (result.settings.cache_notes !== false) {
      try {
        const url = helper.removeURLFragment(tabUrl, false);
        await (await db).delete('note', url);
      } catch (e) {
        // IndexedDB is disabled
      }
    }

    chrome.storage.local.get(['featuresInline'], (result) => {
      if (result.featuresInline) {
        chrome.tabs.query({ url: helper.removeURLFragment(tabUrl, false) }, (tabs) => {
          tabs.forEach((tab) => {
            if (tabId !== tab.id) {
              helper.setDefaultIcon(tab.id, tab.url);
              sendMessage(tab.id, { action: 'resetInline', url: tab.url });
            }
          });
        });
      }
    });
  } else {
    if (helper.isAuthErrorStatus(result.status)) {
      handleAuthError();
      result.errorMessage = authErrorMessage;
    } else {
      result.errorMessage = `An error occurred while deleting a note`;
    }
  }
  return result;
}

export async function createBook(data, url) {
  const response = await fetch(booksUrl, helper.setRequestParams('POST', data));
  const result = await response.json();

  if (result.error !== false) {
    if (helper.isAuthErrorStatus(result.status)) {
      handleAuthError();
      result.errorMessage = authErrorMessage;
    } else {
      result.errorMessage = 'Error creating collection';
    }
  } else {
    const { userCacheNotes } = await getLocalStorage('userCacheNotes');
    if (userCacheNotes !== false) {
      try {
        const tx = (await db).transaction('note', 'readwrite');
        const notes = await tx.store.getAll();

        const pageUrl = helper.removeURLFragment(url, false);

        for (const note of notes) {
          if (note.url === pageUrl) {
            note.recent_books.unshift({
              access: 'private1',
              book_id: result.data.book_id,
              title: result.data.title,
              slug: result.data.slug,
              current_url_item_id: note.url === pageUrl ? 'new-book' : null,
              shared_user_usernames: [],
              team_member_usernames: [],
            });
          }
          await tx.store.put(note);
        }

        await tx.done;
      } catch (e) {
        // IndexedDB is disabled
      }
    }
  }

  return result;
}

export async function removeNoteFromBooks({ data, url, forReadLater = false }) {
  const response = await fetch(removeNoteUrl, helper.setRequestParams('POST', data));
  if (!response?.ok) {
    response.error = true;
    response.errorMessage = `Unable to remove note from collection (status ${response.status})`;

    const { userCacheNotes } = await getLocalStorage('userCacheNotes');
    if (userCacheNotes !== false) {
      try {
        const tx = (await db).transaction('note', 'readwrite');
        const noteEntry = await tx.store.get(helper.removeURLFragment(url, false));

        if (data?.book_ids?.length > 0 && data?.url_item_item_id) {
          let bookIdsCopy =
            Array.isArray(data.book_ids) && data.book_ids.length > 0 ? [...data.book_ids] : null;

          for (let i = 0; i < noteEntry.recent_books.length; i++) {
            const bookId = noteEntry.recent_books[i].book_id;
            if (bookIdsCopy?.length > 0 && bookIdsCopy.includes(bookId)) {
              noteEntry.recent_books[i].current_url_item_id = data.url_item_item_id;
              bookIdsCopy = bookIdsCopy.filter((item) => item !== bookId);
            }
            if (!(bookIdsCopy?.length > 0)) {
              break;
            }
          }
        }

        await tx.store.put(noteEntry);
        await tx.done;
      } catch (e) {
        // IndexedDB is disabled
      }
    }
  } else {
    response.error = false;

    if (forReadLater) {
      const { userCacheNotes, featuresInline } = await getLocalStorage([
        'userCacheNotes',
        'featuresInline',
      ]);
      if (userCacheNotes !== false) {
        try {
          const tx = (await db).transaction('note', 'readwrite');
          const noteEntry = await tx.store.get(helper.removeURLFragment(url, false));

          noteEntry.read_later_book_id = null;

          for (let i = 0; i < noteEntry.recent_books.length; i++) {
            if (noteEntry.recent_books[i].book_id === data.book_ids[0]) {
              delete noteEntry.recent_books[i].current_url_item_id;
              break;
            }
          }

          await tx.store.put(noteEntry);
          await tx.done;
        } catch (e) {
          // IndexedDB is disabled
        }
      }

      if (featuresInline) {
        chrome.tabs.query({ url: helper.removeURLFragment(url, false) }, (tabs) => {
          tabs.forEach((tab) => {
            chrome.tabs.sendMessage(tab.id, {
              action: 'removeInlineReadLater',
              url: tab.url,
            });
          });
        });
      }
    }
  }
  return response;
}

export async function saveVote(data) {
  const response = await fetch(voteUrl, helper.setRequestParams('POST', data));
  const result = await response.json();
  if (result.error === false) {
    if (result.settings.cache_notes !== false) {
      try {
        const url = helper.removeURLFragment(data.url, false);
        const tx = (await db).transaction('note', 'readwrite');
        const noteEntry = await tx.store.get(url);
        noteEntry.vote = data.vote;
        await tx.store.put(noteEntry);
        await tx.done;
      } catch (e) {
        // IndexedDB is disabled
      }
    }

    chrome.storage.local.get(['featuresInline'], (result) => {
      if (result.featuresInline) {
        chrome.tabs.query({ url: helper.removeURLFragment(data.url, false) }, (tabs) => {
          tabs.forEach((tab) => {
            chrome.tabs.sendMessage(tab.id, {
              action: 'updateVoteInline',
              vote: data.vote,
              url: data.url,
            });
          });
        });
      }

      chrome.runtime.sendMessage({
        action: 'updateVoteSidebar',
        dest: 'sidePanel',
        vote: data.vote,
        url: data.url,
      });
    });
  } else {
    if (helper.isAuthErrorStatus(result.status)) {
      handleAuthError();
      result.errorMessage = authErrorMessage;
    } else {
      result.errorMessage = `An error occurred while saving vote`;
    }
  }
  return result;
}

export function getNotesForAllTabs() {
  chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
    tabs.forEach((tab, i) => {
      setTimeout(() => {
        getNote(tab);
      }, i * delayUnitBetweenAPICalls);
    });
  });
}

function saveToReadLaterContext(info, tab) {
  saveNote({
    note: {
      url: info.linkUrl,
      read_later: true,
    },
  });
}

function addContextMenu() {
  chrome.contextMenus.create({
    id: 'save-to-read-later',
    title: 'Save to read later',
    contexts: ['link'],
  });
}

chrome.contextMenus.onClicked.addListener((info) => {
  if (info.menuItemId === 'save-to-read-later') {
    saveToReadLaterContext(info);
  }
});
chrome.tabs.onUpdated.addListener(tabUpdatedHandler);
chrome.tabs.onRemoved.addListener(onTabRemovedHandler);
chrome.runtime.onMessage.addListener(onMessageHandler);
chrome.runtime.onInstalled.addListener(() => {
  getNotesForAllTabs();
  addContextMenu();
});
chrome.runtime.onStartup.addListener(() => {
  getNotesForAllTabs();
  addContextMenu();
});
