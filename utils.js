(() => {
  if (typeof window.histreUtils === 'function') {
    return;
  }

  const histreDOMHelper = window.histreDOMHelper;
  const { $e, $t, noEvents } = histreDOMHelper;
  const host = window.histreDOMHelper.getHost();

  class Utils {
    constructor(dest, refs) {
      this.dest = dest;
      this.refs = refs;
      if (dest === 'popup') {
        this.isPopup = true;
        this.prefix = (classNames) => classNames;
      } else {
        this.prefix = (classNames) => histreDOMHelper.prefix(dest, classNames);
      }
    }

    //----- Vote UI -----//

    createVoteUI({ displaySpinner, showPopup }) {
      const prefix = this.prefix;
      const dest = this.dest;

      const voteUpText = 'Vote up';
      const voteDownText = 'Vote down';
      const voteRemoveText = 'Remove vote';

      const $voteUp = $e(
        'div',
        {
          class: prefix('vote-icon vote-icon--up'),
          title: voteUpText,
          'aria-label': voteUpText,
        },
        { click: voteUp },
      );

      function updateVote(voteVal) {
        if (voteVal === 1) {
          $voteUp.classList.add(prefix(`vote-icon--up-sel`));
          $voteUp.setAttribute('title', voteRemoveText);
          $voteUp.setAttribute('aria-label', voteRemoveText);
        } else {
          $voteUp.classList.remove(prefix(`vote-icon--up-sel`));
          $voteUp.setAttribute('title', voteUpText);
          $voteUp.setAttribute('aria-label', voteUpText);
        }
      }

      function vote(action) {
        if (action !== 'up') {
          return;
        }

        const elem = $voteUp;

        if (elem.classList.contains(prefix(`vote-icon--${action}-sel`))) {
          // previously voted
          updateVote(0);
          postVote(0);
        } else {
          // previously unvoted
          const voteVal = 1;
          updateVote(voteVal);
          postVote(voteVal);
        }
      }

      async function postVote(voteVal) {
        let url;

        if (dest === 'sidebar') {
          const activeTab = (await chrome.tabs.query({ active: true, lastFocusedWindow: true }))[0];
          url = activeTab.url;
        } else {
          url = location.href;
        }

        const data = {
          action: 'saveInlineVote',
          dest: 'inline',
          inline: {
            url,
            title: document.title,
            vote: voteVal,
          },
          from: dest,
        };
        if (typeof displaySpinner === 'function') {
          displaySpinner();
        }
        if (typeof showPopup === 'function') {
          showPopup({ action: 'loading' });
        }
        // persist vote on the backend
        try {
          const response = await chrome.runtime.sendMessage(data);

          if (response && response.usermsg) {
            // TODO: Replace below with logic to show error message to user
            console.log(response.usermsg);
          }
          if (response.error !== false) {
            throw response;
          }

          if (typeof showPopup === 'function') {
            if (voteVal === 1) {
              showPopup({ action: 'votedUp' });
            } else {
              showPopup({ action: 'voteRemoved' });
            }
          }
        } catch (e) {
          updateVote(voteVal === 1 ? 0 : 1);
          if (typeof showPopup === 'function') {
            showPopup({ action: 'error' });
          }
        }
      }

      function voteUp() {
        vote('up');
      }

      return {
        elements: [$voteUp],
        updateVote,
      };
    }

    //----- Access levels -----//

    createAccessLevels(accessLevels) {
      const prefix = this.prefix;
      const isPopup = this.isPopup;
      const { quill, saveNote } = this.refs;

      const $btnGroup = $e('div', {
        class: prefix('btn-group'),
        role: 'group',
        'aria-label': 'Access level',
      });

      function createAccessSelector([choice, text]) {
        const $input = $e(
          'input',
          {
            type: 'radio',
            id: prefix(`btn-access-${choice}`),
            class: prefix('btn-check'),
            name: 'access',
            value: choice,
            autocomplete: 'off',
          },
          {
            change: () => {
              saveNote();
              quill.focus();
            },
          },
        );

        let $icon;
        if (isPopup) {
          $icon = $e('i', { 'aria-label': text });
          if (choice === 'private') {
            $icon.className = 'fe fe-lock';
          } else if (choice === 'org') {
            $icon.className = 'fe fe-briefcase';
          } else {
            $icon.className = 'fe fe-globe';
          }
        } else {
          $icon = $e('span', {
            class: prefix(`access-icon access-icon--${choice}`),
            'aria-label': text,
          });
        }

        const $label = $e(
          'label',
          {
            class: prefix('btn btn-sm btn-outline-secondary'),
            for: prefix(`btn-access-${choice}`),
            title: text,
          },
          noEvents,
          [$icon],
        );

        return [$input, $label];
      }

      accessLevels.forEach((accessLevel) => {
        const [$input, $label] = createAccessSelector(accessLevel);

        $btnGroup.appendChild($input);
        $btnGroup.appendChild($label);
      });

      return $btnGroup;
    }

    //----- Tags -----//

    createSuggestedTags(tags) {
      const prefix = this.prefix;
      const { quill } = this.refs;

      const $suggestedTagsText = $e('div', { class: prefix('suggested-tags-text') }, noEvents, [
        $t('Suggested Tags'),
      ]);
      const $suggestedTags = $e('div', { class: prefix('suggested-tags') }, noEvents, [
        $suggestedTagsText,
      ]);

      function addSuggestedTagToNote() {
        const tag = this.dataset.histreTagVal;
        const selection = quill.getSelection(true);
        const pos = selection.index;
        const txt = ` #${tag} `;
        quill.insertText(pos, txt, 'user');
        quill.setSelection(pos + txt.length, 0, 'user');
      }

      function adjustTag(tag) {
        const maxl = 12;
        const tail = 4;
        if (!tag) {
          return tag;
        }
        const l = tag.length;
        if (l <= maxl) {
          return tag;
        } else {
          return (
            tag.slice(0, Math.min(maxl - (tail + 3), l - tail)) + '...' + tag.slice(l - tail, l)
          );
        }
      }

      tags.forEach((tag) => {
        const tagHtml = $e(
          'div',
          { class: prefix('suggested-tag'), 'data-histre-tag-val': tag },
          { click: addSuggestedTagToNote },
          [$t(`#${adjustTag(tag)}`)],
        );
        $suggestedTags.appendChild(tagHtml);
      });

      return $suggestedTags;
    }

    //----- Data extraction -----//

    createExtractedDataTable() {
      const prefix = this.prefix;
      const isPopup = this.isPopup;
      const refs = this.refs;

      const $dataTable = $e('table', {
        id: prefix('extracted-data-table'),
        class: prefix('extracted-data-table'),
      });
      const $newRow = $e(
        'div',
        {
          class: prefix('extracted-data-new'),
          tabindex: '0',
          disabled: '',
        },
        {
          click: addNewRow,
          keydown: addNewRowKeydown,
        },
        [
          isPopup
            ? $e('i', { class: 'fe fe-plus' })
            : $e('span', { class: prefix('extracted-data-icon extracted-data-icon--plus') }),
          $t('Add Attribute'),
        ],
      );

      function renderExtractedData(data) {
        if ($newRow.hasAttribute('disabled')) {
          $newRow.removeAttribute('disabled');
        }

        $dataTable.textContent = '';
        if (data) {
          for (let field in data) {
            let fieldValue = data[field];

            if (typeof fieldValue === 'boolean') {
              fieldValue = fieldValue ? 'Yes' : 'No';
            }

            $dataTable.appendChild(createRow(field, fieldValue));
          }
        }
      }

      function getTableData() {
        const fields = [...$dataTable.querySelectorAll('tr > th')].map(
          (field) => field.textContent,
        );
        if (fields.length > 0) {
          const values = [...$dataTable.querySelectorAll('tr > td')].map(
            (vaule) => vaule.textContent,
          );
          const attrs = {};
          for (let i = 0; i < fields.length; i++) {
            attrs[fields[i]] = values[i];
          }
          return attrs;
        } else {
          return null;
        }
      }

      function createRow(field, value) {
        const $rowHeader = $e(
          'th',
          {
            class: prefix('extracted-data-row-header'),
            scope: 'row',
            contenteditable: 'true',
          },
          { input: saveTable },
          field ? [$t(toHeaderText(field))] : [],
        );
        const $rowValue = $e(
          'td',
          {
            class: prefix('extracted-data-row-value'),
            contenteditable: 'true',
          },
          { input: saveTable },
          value ? [$t(value)] : [],
        );
        const $removeRow = $e(
          'span',
          {
            class: prefix('extracted-data-remove'),
            tabindex: '0',
            title: 'Delete row',
            'aria-label': 'Delete row',
          },
          {
            click: removeRow,
            keydown: removeRowKeydown,
          },
          [
            isPopup
              ? $e('i', { class: 'fe fe-x' })
              : $e('span', { class: prefix('extracted-data-icon extracted-data-icon--x') }),
          ],
        );
        const $row = $e('tr', { class: prefix('extracted-data-row') }, noEvents, [
          $rowHeader,
          $rowValue,
          $removeRow,
        ]);

        function removeRow() {
          const container = isPopup ? document : document.querySelector(`#${prefix('wrap')}`);

          function confirmDelete(e) {
            if (e.target.closest('tr > span') !== $removeRow) {
              $row.removeAttribute('data-histre-delete-row');
              container.removeEventListener('click', confirmDelete);
              $removeRow.setAttribute('title', 'Delete row');
              $removeRow.setAttribute('aria-label', 'Delete row');
            }
          }

          if ($row.hasAttribute('data-histre-delete-row')) {
            $row.remove();
            refs.saveNote();
            container.removeEventListener('click', confirmDelete);
          } else {
            $row.setAttribute('data-histre-delete-row', '');
            container.addEventListener('click', confirmDelete);
            this.setAttribute('title', 'Click again to delete row');
            this.setAttribute('aria-label', 'Click again to delete row');
          }
        }

        function removeRowKeydown(e) {
          if (e.key === 'Enter' || e.key === ' ') {
            e.preventDefault();
            removeRow();
          }
        }

        function saveTable() {
          if ($rowHeader.textContent === '') {
            function showFieldError() {
              $rowHeader.removeAttribute('data-histre-table-field-error');
              $rowHeader.removeAttribute('title');
              $rowHeader.removeAttribute('aria-label');
              $rowHeader.removeEventListener('input', showFieldError);
            }

            $rowHeader.setAttribute('data-histre-table-field-error', '');
            $rowHeader.setAttribute('title', 'Please enter this field');
            $rowHeader.setAttribute('aria-label', 'Please enter this field');
            $rowHeader.addEventListener('input', showFieldError);
          } else {
            refs.saveNoteDebounced();
          }
        }

        return $row;
      }

      function addNewRow() {
        if (this.hasAttribute('disabled')) {
          return;
        }
        const $row = createRow();
        const $field = $row.querySelector('th');
        $dataTable.appendChild($row);
        $field.focus();
      }

      function addNewRowKeydown(e) {
        if (e.key === 'Enter' || e.key === ' ') {
          e.preventDefault();
          addNewRow.call(this);
        }
      }

      function toHeaderText(fieldName) {
        // Convert snake-case field names to title case for header text
        if (!fieldName) {
          return '';
        }
        return fieldName.replace(/_+/g, ' ').replace(/\w\S*/g, (text) => {
          return text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
        });
      }

      return {
        elements: [$dataTable, $newRow],
        renderExtractedData,
        getTableData,
      };
    }

    //----- Recent books : Begin -----//

    createRecentBooks(initialRecentBooks) {
      const dest = this.dest;
      const prefix = this.prefix;
      const isPopup = this.isPopup;
      const { quill, saveNote, saveNoteDebounced } = this.refs;

      let recentBooks = initialRecentBooks;
      let shownRecentBooks = recentBooks;
      const maxBooksDisplayed = isPopup ? 3 : 4;

      //--- Header ---//
      const $recentBooksToggleIcon = isPopup
        ? $e('i', { class: 'fe fe-chevron-up' })
        : $e('div', { class: prefix('book-toggle-icon book-toggle-icon--hide') });
      const $recentBooksToggle = $e(
        'button',
        { class: prefix('btn btn-sm btn-outline-secondary') },
        noEvents,
        [$recentBooksToggleIcon],
      );
      const $recentBooksHeaderText = $e(
        'div',
        { class: prefix('recent-books-toggle-text') },
        noEvents,
        [$t('Add this note to collection(s) '), $e('span', {}, noEvents, [$t('(optional)')])],
      );
      // Search
      const $recentBooksSearchToggle = $e(
        'button',
        {
          class: prefix('btn btn-sm btn-outline-secondary recent-books-search-toggle'),
          'data-histre-add-to-book-for': 'search',
        },
        { click: toggleRecentBooksSearch },
        [
          isPopup
            ? $e('i', { class: 'fe fe-search' })
            : $e('div', { class: prefix('book-toggle-icon book-toggle-icon--search') }),
        ],
      );
      // Create
      const $recentBooksCreateToggle = $e(
        'button',
        {
          class: prefix('btn btn-sm btn-outline-secondary'),
          'data-histre-add-to-book-for': 'create',
        },
        { click: toggleRecentBooksCreate },
        [
          isPopup
            ? $e('i', { class: prefix('fe fe-plus') })
            : $e('div', { class: prefix('book-toggle-icon book-toggle-icon--create') }),
        ],
      );
      // Header element
      const $recentBooksHeader = $e(
        'div',
        {
          class: prefix('recent-books-header'),
          role: 'toggle',
          'aria-label': 'Show Collections List',
        },
        { click: toggleRecentBooks },
        [
          $recentBooksCreateToggle,
          $recentBooksSearchToggle,
          $recentBooksHeaderText,
          $recentBooksToggle,
        ],
      );

      //--- Body ---//
      // Search
      const $recentBooksSearchInput = $e(
        'input',
        {
          class: prefix('form-control form-control-sm recent-books-search-input'),
          type: 'text',
          placeholder: 'Search Collections',
          'data-histre-book-input': '',
        },
        { input: handleRecentBooksSearch, keydown: handleSearchKeydown },
      );
      const $recentBooksSearchText = $e(
        'div',
        { class: prefix('recent-books-search-text') },
        noEvents,
        [$t('Type a collection title or keywords')],
      );
      const $recentBooksSearch = $e('div', { class: prefix('recent-books-search') }, noEvents, [
        $recentBooksSearchInput,
        $recentBooksSearchText,
      ]);
      // Create
      const $recentBooksCreateCheck = isPopup
        ? $e('i', { class: prefix('fe fe-circle') })
        : $e('div', { class: prefix('book-icon book-icon--circle') });
      const $recentBooksCreateIcon = isPopup
        ? $e('i', { class: prefix('book-access fe fe-lock') })
        : $e('div', { class: prefix('book-access book-icon book-icon--private') });
      const $recentBooksCreateInput = $e(
        'input',
        {
          class: prefix('form-control form-control-sm'),
          type: 'text',
          placeholder: 'Title of the new collection',
          'data-histre-book-input': '',
        },
        { keydown: handleBookCreateKeydown },
      );
      const $recentBooksCreateAccept = $e(
        isPopup ? 'i' : 'div',
        {
          class: isPopup ? 'fe fe-check' : prefix('book-icon book-icon--create-check'),
          'aria-label': 'Create new collection',
          title: 'Create new collection',
        },
        { click: saveNote },
      );
      const $recentBooksCreateCancel = $e(
        isPopup ? 'i' : 'div',
        {
          class: isPopup ? 'fe fe-x' : prefix('book-icon book-icon--create-cancel'),
          'aria-label': 'Cancel collection creation',
          title: 'Cancel collection creation',
        },
        { click: toggleRecentBooksCreate },
      );
      const $recentBooksCreate = $e(
        'div',
        { class: prefix('book-check-wrap book-check-wrap--create') },
        noEvents,
        [
          $recentBooksCreateCheck,
          $recentBooksCreateIcon,
          $recentBooksCreateInput,
          $recentBooksCreateAccept,
          $recentBooksCreateCancel,
        ],
      );
      // Books
      const $recentBooksBooks = $e('div', { class: prefix('recent-books-books') });
      // Show more / count
      const $recentBooksCount = $e('span');
      const $recentBooksShowMoreChevrons = isPopup
        ? $e('i', { class: prefix('fe fe-chevrons-down recent-books-show-more-chevrons') })
        : $e('div', { class: prefix('book-icon book-icon--chevrons') });
      const $recentBooksShowMore = $e(
        'button',
        {
          class: isPopup ? 'btn btn-link p-0' : prefix('btn btn-link btn-link--no-padding'),
        },
        { click: showAllBooks },
        [$recentBooksCount, $t(' selected. Show more'), $recentBooksShowMoreChevrons],
      );
      const $recentBooksShowMoreWithoutCount = $e(
        'button',
        {
          class: isPopup ? 'btn btn-link p-0' : prefix('btn btn-link btn-link--no-padding'),
        },
        { click: showAllBooks },
        [$t('Show more'), $recentBooksShowMoreChevrons.cloneNode(true)],
      );
      // Body element
      const $recentBooksBody = $e('div', { class: prefix('recent-books-body') }, noEvents, [
        $recentBooksSearch,
        $recentBooksCreate,
        $recentBooksBooks,
        $recentBooksShowMore,
        $recentBooksShowMoreWithoutCount,
      ]);

      //--- Wrap ---//
      const $recentBooks = $e('div', { class: prefix('recent-books') }, noEvents, [
        $recentBooksHeader,
        $recentBooksBody,
      ]);

      function setupRecentBooks(isSearch = false) {
        $recentBooksBooks.textContent = '';
        $recentBooksBooks.classList.remove(prefix('recent-books-books--overflown'));
        $recentBooksShowMore.style.display = 'none';
        $recentBooksShowMoreWithoutCount.style.display = 'none';
        if (!shownRecentBooks) {
          return;
        }

        const orgAccessLabel = document.querySelector(
          `label[for=${prefix('btn-access-org')}]`,
        )?.title;

        shownRecentBooks.forEach((book, bookIndex) => {
          const isAdded = book.current_url_item_id != null;

          const $bookItemWrap = $e('div', {
            id: prefix(`book-${book.book_id}`),
            class: prefix('book-wrap'),
          });
          if (bookIndex >= maxBooksDisplayed) {
            $bookItemWrap.style.display = 'none';
          }

          const $bookCheckWrap = $e(
            'div',
            {
              class: prefix('book-check-wrap'),
              role: 'checkbox',
              'aria-checked': isAdded ? 'true' : 'false',
              tabindex: 0,
            },
            { click: handleBookSelection },
          );

          const $bookCheck = $e(isPopup ? 'i' : 'div', {
            class: isPopup
              ? `fe ${isAdded ? 'fe-check-circle' : 'fe-circle'}`
              : prefix(`book-icon ${isAdded ? 'book-icon--checked' : 'book-icon--circle'}`),
            'data-histre-book-id': book.book_id,
            'data-histre-book-added': isAdded ? 'added' : 'not-added',
          });
          $bookCheckWrap.appendChild($bookCheck);

          const { access } = book;
          let bookIconClass, info;
          if (access === 'public') {
            bookIconClass = isPopup ? 'globe' : 'public';
            info = 'Visible to public';
          } else if (book.shared_user_usernames.length > 0) {
            bookIconClass = isPopup ? 'users' : 'limited';
            info = `Shared with ${book.shared_user_usernames.length} ${
              book.shared_user_usernames.length === 1 ? 'person' : 'people'
            }`;
          } else if (access === 'org') {
            const orgText = orgAccessLabel ? orgAccessLabel.toLowerCase() : 'your org';
            bookIconClass = isPopup ? 'briefcase' : 'org';
            info = `Visible to ${orgText}`;
          } else {
            bookIconClass = isPopup ? 'lock' : 'private';
            info = `Visible to only you`;
          }

          const $bookIcon = $e('div', {
            class: prefix(
              `book-access ${
                isPopup ? `fe fe-${bookIconClass}` : `book-icon book-icon--${bookIconClass}`
              }`,
            ),
            'aria-label': info,
            title: info,
          });
          $bookCheckWrap.appendChild($bookIcon);

          const $bookName = $e('div', { class: prefix('book-name') }, noEvents, [$t(book.title)]);
          $bookCheckWrap.appendChild($bookName);

          if (book.shareable_link) {
            const $bookShareableLink = $e(
              'div',
              {
                class: prefix('book-icon book-icon--copy'),
                role: 'button',
                tabindex: 0,
                'aria-label': 'Copy collection link to clipboard',
                title: 'Copy collection link to clipboard',
              },
              { click: copyLinkToClipboard },
            );
            $bookCheckWrap.appendChild($bookShareableLink);
          }

          const $bookOpenLink = $e(
            isPopup ? 'i' : 'div',
            {
              class: isPopup ? 'fe fe-link' : prefix('book-icon book-icon--view'),
              role: 'button',
              tabindex: 0,
              'aria-label': 'View collection details',
              title: 'View collection details',
            },
            { click: openBookLink },
          );
          $bookCheckWrap.appendChild($bookOpenLink);

          $bookItemWrap.appendChild($bookCheckWrap);
          $recentBooksBooks.appendChild($bookItemWrap);

          function handleBookSelection(e) {
            if (
              e.target.classList.contains(prefix('book-icon--copy')) ||
              e.target.classList.contains(prefix('book-icon--copy-check')) ||
              e.target.classList.contains(isPopup ? 'fe fe-link' : prefix('book-icon--view'))
            ) {
              return;
            }

            const updatedCheckedState =
              $bookCheckWrap.getAttribute('aria-checked') === 'false' ? 'true' : 'false';
            $bookCheckWrap.setAttribute('aria-checked', updatedCheckedState);

            $bookCheck.classList.toggle(isPopup ? 'fe-circle' : prefix('book-icon--circle'));
            $bookCheck.classList.toggle(isPopup ? 'fe-check-circle' : prefix('book-icon--checked'));

            if ($bookCheck.dataset.histreBookAdded === 'not-added') {
              $bookCheck.dataset.histreBookAdded = 'added';
            }

            if ($recentBooksShowMore.style.display !== 'none') {
              countSelectedBooks();
            }

            saveNoteDebounced();
            quill.focus();
          }

          function copyLinkToClipboard() {
            $recentBooksBooks
              .querySelectorAll(`.${prefix('book-icon--copy-check')}`)
              .forEach((icon) => {
                icon.classList.remove(prefix('book-icon--copy-check'));
                icon.title = 'Copy collection link to clipboard';
                icon.setAttribute('aria-label', 'Copy collection link to clipboard');
                icon.classList.add(prefix('book-icon--copy'));
              });

            navigator.clipboard.writeText(book.shareable_link);
            this.classList.remove(prefix('book-icon--copy'));
            this.classList.add(prefix('book-icon--copy-check'));
            this.title = 'Link copied to clipboard';
            this.setAttribute('aria-label', 'Link copied to clipboard');
          }

          function openBookLink() {
            const bookLink = `${host}/collections/${book.book_id}/`;
            window.open(bookLink, '_blank');
          }
        });

        if (shownRecentBooks.length > maxBooksDisplayed) {
          if (isSearch) {
            $recentBooksShowMoreWithoutCount.style.display = 'block';
          } else {
            $recentBooksShowMore.style.display = 'block';
            countSelectedBooks();
          }
        }

        if (isPopup) {
          chrome.storage.local.get(['popupShowRecentBooks'], (result) => {
            if (result.popupShowRecentBooks) {
              showRecentBooks();
            } else {
              hideRecentBooks();
            }

            $recentBooks.style.display = 'block';
          });
        } else if (dest === 'sidebar') {
          chrome.storage.local.get(['sidebarShowRecentBooks'], (result) => {
            if (result.sidebarShowRecentBooks) {
              showRecentBooks();
            } else {
              hideRecentBooks();
            }

            $recentBooks.style.display = 'block';
          });
        } else {
          showRecentBooks();
          $recentBooks.style.display = 'block';
        }
      }

      function countSelectedBooks() {
        const result = $recentBooksBody.querySelectorAll(
          `.${isPopup ? 'fe-check-circle' : prefix('book-icon--checked')}`,
        );
        $recentBooksCount.textContent = result.length;
      }

      function showRecentBooks() {
        $recentBooksBody.style.display = 'block';
        $recentBooksToggleIcon.classList.remove(
          isPopup ? 'fe-chevron-down' : prefix('book-toggle-icon--show'),
        );
        $recentBooksToggleIcon.classList.add(
          isPopup ? 'fe-chevron-up' : prefix('book-toggle-icon--hide'),
        );
        $recentBooksSearchToggle.style.display = 'block';
        $recentBooksCreateToggle.style.display = 'block';
      }

      function hideRecentBooks() {
        $recentBooksBody.style.display = 'none';
        $recentBooksToggleIcon.classList.remove(
          isPopup ? 'fe-chevron-up' : prefix('book-toggle-icon--hide'),
        );
        $recentBooksToggleIcon.classList.add(
          isPopup ? 'fe-chevron-down' : prefix('book-toggle-icon--show'),
        );
        $recentBooksSearchToggle.style.display = 'none';
        $recentBooksCreateToggle.style.display = 'none';
      }

      function toggleRecentBooks(e) {
        // Do not toggle collapse if user is clicking on create book, search books, etc. buttons
        if (e.target.closest('[data-histre-add-to-book-for]') !== null) {
          return;
        }

        if ($recentBooksBody.style.display === 'block' || $recentBooksBody.style.display === '') {
          hideRecentBooks();
          if (isPopup) {
            chrome.storage.local.set({ popupShowRecentBooks: false });
          } else if (dest === 'sidebar') {
            chrome.storage.local.set({ sidebarShowRecentBooks: false });
            chrome.runtime.sendMessage({ action: 'closeRecentBooks', dest: 'sidePanel' });
          }
        } else {
          showRecentBooks();
          if (isPopup) {
            chrome.storage.local.set({ popupShowRecentBooks: true });
          } else if (dest === 'sidebar') {
            chrome.storage.local.set({ sidebarShowRecentBooks: true });
            chrome.runtime.sendMessage({ action: 'showRecentBooks', dest: 'sidePanel' });
          }
        }
      }

      function toggleRecentBooksSearch() {
        if ($recentBooksSearch.style.display === 'block') {
          $recentBooksSearch.style.display = 'none';

          if (JSON.stringify(shownRecentBooks) !== JSON.stringify(recentBooks)) {
            shownRecentBooks = recentBooks;
            $recentBooksSearchInput.value = '';
            $recentBooksSearchText.textContent = 'Type a collection title or keywords';
            setupRecentBooks();
          }

          quill.focus();
        } else {
          $recentBooksSearch.style.display = 'block';
          $recentBooksSearchInput.focus();
        }
      }

      function toggleRecentBooksCreate() {
        if ($recentBooksCreate.style.display === 'grid') {
          $recentBooksCreate.style.display = 'none';
          $recentBooksCreateCheck.classList.remove(
            prefix(isPopup ? 'fe-check-circle' : 'book-icon--checked'),
          );
          $recentBooksCreateCheck.classList.add(
            prefix(isPopup ? 'fe-circle' : 'book-icon--circle'),
          );
          quill.focus();
          if ($recentBooksShowMore.style.display !== 'none') {
            countSelectedBooks();
          }
        } else {
          $recentBooksCreate.style.display = 'grid';
          $recentBooksCreateCheck.classList.remove(
            isPopup ? 'fe-circle' : prefix('book-icon--circle'),
          );
          $recentBooksCreateCheck.classList.add(
            isPopup ? 'fe-check-circle' : prefix('book-icon--checked'),
          );
          $recentBooksCreateInput.focus();
          if ($recentBooksShowMore.style.display !== 'none') {
            countSelectedBooks();
          }
        }
      }

      function handleBookCreateKeydown(e) {
        if (e.key === 'Escape') {
          toggleRecentBooksCreate();
        } else if (e.key === 'Enter') {
          saveNote();
        }
      }

      function handleRecentBooksSearch() {
        const search = this.value;
        const prevShownBooks = shownRecentBooks;

        if (search === '') {
          shownRecentBooks = recentBooks;
          setupRecentBooks();
          $recentBooksSearchText.textContent = 'Type a collection title or keywords';
          return;
        }

        $recentBooksShowMore.style.display = 'none';

        shownRecentBooks = recentBooks.filter((book) => {
          return book.title.toLowerCase().includes(search.toLowerCase());
        });

        if (JSON.stringify(prevShownBooks) !== JSON.stringify(shownRecentBooks)) {
          setupRecentBooks(true); // rerender
        }

        if (shownRecentBooks.length <= 0) {
          $recentBooksSearchText.textContent = 'No collections found for your search';
        } else {
          $recentBooksSearchText.textContent = `Found ${shownRecentBooks.length} collection${
            shownRecentBooks.length !== 1 ? 's' : ''
          } for your search`;
        }
      }

      function handleSearchKeydown(e) {
        if (e.key === 'Escape') {
          toggleRecentBooksSearch();
        }
      }

      function getNewBookTitle() {
        if (
          $recentBooksCreate.style.display !== 'grid' ||
          $recentBooksCreateInput.disabled === true
        ) {
          return null;
        }
        disableNewBookActions();
        if ($recentBooksCreateInput.value === '') {
          // Throw error
          enableNewBookActions();
          return null;
        } else {
          return $recentBooksCreateInput.value.trim();
        }
      }

      function disableNewBookActions() {
        $recentBooksCreateInput.disabled = true;
        $recentBooksCreateAccept.style.visibility = 'hidden';
        $recentBooksCreateCancel.style.visibility = 'hidden';
      }

      function enableNewBookActions() {
        $recentBooksCreateInput.disabled = false;
        $recentBooksCreateAccept.style.visibility = 'visible';
        $recentBooksCreateCancel.style.visibility = 'visible';
        quill.focus();
      }

      function enableNewBook(newBookId) {
        recentBooks.unshift({
          access: 'private1',
          book_id: newBookId,
          title: $recentBooksCreateInput.value,
          shared_user_usernames: [],
          team_member_usernames: [],
          current_url_item_id: 'new-book',
        });

        shownRecentBooks = recentBooks;
        setupRecentBooks();

        enableNewBookActions();
        $recentBooksCreateInput.value = '';
        toggleRecentBooksCreate();

        quill.focus();
      }

      function getBookIdsForAddNote() {
        const bookIdsForAddNote = [];
        const checkBookItems = $recentBooks.querySelectorAll(
          `.${isPopup ? 'fe-check-circle' : prefix('book-icon--checked')}`,
        );
        Array.from(checkBookItems).forEach((checkedBookItem) => {
          const bookId = checkedBookItem.dataset.histreBookId;
          if (bookId && bookId.length > 0) {
            bookIdsForAddNote.push(bookId);
          }
        });
        return bookIdsForAddNote;
      }

      function getDeselectedBookIds() {
        let deselectedBookIds = [];
        const uncheckedBooks = $recentBooksBooks.querySelectorAll(
          `.${isPopup ? 'fe-circle' : prefix('book-icon--circle')}`,
        );
        Array.from(uncheckedBooks).forEach((book) => {
          if (book.dataset.histreBookAdded !== 'not-added') {
            deselectedBookIds.push(book.dataset.histreBookId);
          }
        });

        deselectedBookIds.forEach((bookId) => {
          for (const book of recentBooks) {
            if (book.book_id === bookId) {
              book.current_url_item_id = null;
              break;
            }
          }
        });

        return deselectedBookIds;
      }

      function reCheckBooks(bookIds) {
        bookIds.forEach((bookId) => {
          const bookCheck = $recentBooksBooks.querySelector(
            `.${
              isPopup ? 'fe-circle' : prefix('book-icon--circle')
            }[data-histre-book-id="${bookId}"]`,
          );
          const bookCheckWrap = bookCheck.closest(`.${prefix('book-check-wrap')}`);

          const updatedCheckedState =
            bookCheckWrap.getAttribute('aria-checked') === 'false' ? 'true' : 'false';
          bookCheckWrap.setAttribute('aria-checked', updatedCheckedState);

          bookCheck.classList.toggle(isPopup ? 'fe-circle' : prefix('book-icon--circle'));
          bookCheck.classList.toggle(isPopup ? 'fe-check-circle' : prefix('book-icon--checked'));
        });

        bookIds.forEach((bookId) => {
          for (const book of recentBooks) {
            if (book.book_id === bookId) {
              book.current_url_item_id = 'added';
              break;
            }
          }
        });

        if ($recentBooksShowMore.style.display !== 'none') {
          countSelectedBooks();
        }
      }

      function showAllBooks() {
        [...$recentBooksBooks.children].forEach((book) => {
          book.style.display = 'block';
        });
        this.style.display = 'none';
        quill.focus();
        $recentBooksBooks.classList.add(prefix('recent-books-books--overflown'));
      }

      function renderRecentBooks(newRecentBooks) {
        const prevRecentBooks = recentBooks;
        recentBooks = newRecentBooks;
        shownRecentBooks = recentBooks;
        if (JSON.stringify(prevRecentBooks) !== JSON.stringify(recentBooks)) {
          setupRecentBooks(); // rerender
        }
      }

      function updateRecentBooks({ checkedIds, noteId, newBook }) {
        if (newBook?.id && newBook?.title && noteId) {
          recentBooks.unshift({
            access: 'private1',
            book_id: newBook.id,
            title: newBook.title,
            current_url_item_id: noteId,
            shared_user_usernames: [],
            team_member_usernames: [],
          });
        }

        if (checkedIds?.length > 0 && noteId) {
          for (const book of recentBooks) {
            if (checkedIds.includes(book.book_id)) {
              book.current_url_item_id = noteId;
            } else {
              if (book.current_url_item_id) {
                delete book.current_url_item_id;
              }
            }
          }
        }

        setupRecentBooks(); // rerender
      }

      function checkRecentBooks({ bookIds, itemId, moveToFront = false }) {
        if (!Array.isArray(bookIds) || !itemId) return;

        for (const bookId of bookIds) {
          let bookIndex = -1;

          for (let i = 0; i < recentBooks.length; i++) {
            if (recentBooks[i].book_id === bookId) {
              recentBooks[i].current_url_item_id = itemId;
              bookIndex = i;
              break;
            }
          }

          if (moveToFront && bookIndex > -1) {
            recentBooks.unshift(...recentBooks.splice(bookIndex, 1));
          }
        }

        if (moveToFront) {
          shownRecentBooks = recentBooks;
        }
        setupRecentBooks(); // rerender
      }

      function uncheckRecentBooks(bookIds) {
        if (Array.isArray(bookIds)) {
          for (const bookId of bookIds) {
            for (const book of recentBooks) {
              if (book.book_id === bookId) {
                if (book.current_url_item_id) {
                  delete book.current_url_item_id;
                }
                break;
              }
            }
          }
        } else {
          for (const book of recentBooks) {
            if (book.current_url_item_id) {
              delete book.current_url_item_id;
            }
          }
        }

        setupRecentBooks(); // rerender
      }

      setupRecentBooks();

      return {
        element: $recentBooks,
        renderRecentBooks,
        updateRecentBooks,
        checkRecentBooks,
        uncheckRecentBooks,
        showRecentBooks,
        hideRecentBooks,
        getBookIdsForAddNote,
        getDeselectedBookIds,
        reCheckBooks,
        getNewBookTitle,
        enableNewBookActions,
        enableNewBook,
      };
    }

    //----- Recent books : End -----//

    getMentionConfig() {
      return {
        allowedChars: /^[a-z0-9]*$/i,
        mentionDenotationChars: ['#'],
        isolateCharacter: true,
        minChars: 1,
        source: (searchTerm, renderList, mentionChar) => {
          const values = Array.isArray(this.refs.tagList) ? this.refs.tagList : [];

          if (searchTerm.length === 0) {
            renderList(values, searchTerm);
          } else {
            const startMatches = [];
            const anyMatches = [];
            let srch, itemval, loc;
            for (let i = 0; i < values.length; i++) {
              srch = searchTerm.toLowerCase();
              itemval = values[i].value.toLowerCase();
              loc = itemval.indexOf(srch);
              if (loc === 0) {
                startMatches.push(values[i]);
              } else if (loc > -1) {
                anyMatches.push(values[i]);
              } else {
                // not matched. skip.
              }
            }
            const matches = startMatches.concat(anyMatches);
            const maxMatches = 6;
            renderList(matches.slice(0, maxMatches), searchTerm);
          }
        },
        onSelect: (item, insertItem) => {
          const selection = this.refs.quill.getSelection(true);
          const pos = selection.index;
          let start = pos;
          let char;
          let found = false;
          while (start >= 0) {
            char = this.refs.quill.getText(start, 1);
            if (char === item.denotationChar) {
              found = true;
              break;
            }
            start -= 1;
          }
          if (found) {
            this.refs.quill.deleteText(start + 1, pos - start);
            this.refs.quill.insertText(start + 1, item.value + ' ', 'user');
            this.refs.quill.setSelection(start + item.value.length + 2, 0, 'user');
          }
        },
      };
    }

    //----- Ask AI -----//

    createAskAI({ showMainContent, closeStandaloneUI } = {}) {
      const prefix = this.prefix;
      const isStandalone = this.dest === 'ask-ai';

      let allPrompts = [];
      const displayedPromptsLimit = 5;

      const $askAIBack = $e(
        'button',
        { class: prefix('btn btn-sm btn-outline-secondary ask-ai-tab-close') },
        { click: hideAskAI },
        [$t('Go back')],
      );

      const $askAIInput = $e(
        'textarea',
        {
          class: prefix('form-control'),
          placeholder: 'Ask AI a question about this page',
        },
        { keydown: handleAIInputKeydown },
      );

      const $askAIModelText = $e('span', { class: prefix('dropdown-toggle-text') }, noEvents, [
        $t('gpt-4'),
      ]);
      const $askAIModelBtn = $e(
        'button',
        {
          class: prefix('btn btn-sm btn-outline-secondary dropdown-toggle-btn'),
          'aria-expanded': false,
        },
        { click: toggleAIModelDropdown },
        [
          $askAIModelText,
          $e('span', { class: prefix('dropdown-toggle-icon dropdown-toggle-icon--down') }),
        ],
      );
      const $askAIModelList = $e('div', { class: prefix('dropdown-list') }, noEvents, [
        $e(
          'div',
          { class: prefix('dropdown-list-item'), 'data-histre-dropdown-item': 'gpt3' },
          { click: selectAskAIModel },
          [$t('gpt-3')],
        ),
        $e(
          'div',
          { class: prefix('dropdown-list-item'), 'data-histre-dropdown-item': 'gpt3.5' },
          { click: selectAskAIModel },
          [$t('gpt-3.5')],
        ),
        $e(
          'div',
          { class: prefix('dropdown-list-item'), 'data-histre-dropdown-item': 'gpt4' },
          { click: selectAskAIModel },
          [$t('gpt-4')],
        ),
      ]);
      const $askAIModelBtnGroup = $e('div', { class: prefix('btn-group') }, noEvents, [
        $askAIModelBtn,
        $askAIModelList,
      ]);
      const $askAIModelWrap = $e(
        'div',
        { class: prefix('dropdown-wrap'), 'data-histre-dropdown-value': 'gpt4' },
        noEvents,
        [$askAIModelBtnGroup, $askAIModelList],
      );

      const $askAIPromptsText = $e('span', { class: prefix('dropdown-toggle-text') }, noEvents, [
        $t('Prompts'),
      ]);
      const $askAIPromptsToggle = $e(
        'button',
        { class: prefix('btn btn-sm btn-outline-secondary dropdown-toggle-btn'), disabled: '' },
        { click: togglePromptsDropdown },
        [
          $askAIPromptsText,
          $e('div', { class: prefix('dropdown-toggle-icon dropdown-toggle-icon--down') }),
        ],
      );
      const $askAIPromptsList = $e('div', {
        class: prefix(`dropdown-list ${isStandalone ? 'prompt-list' : 'ask-ai-prompt-list'}`),
      });
      const $askAIPromptsWrap = $e('div', { class: prefix('dropdown-wrap') }, noEvents, [
        $askAIPromptsToggle,
        $askAIPromptsList,
      ]);

      const $askAISubmitBtn = $e(
        'button',
        { class: prefix('btn btn-sm btn-outline-primary') },
        { click: submitQuestion },
        [$t('Submit')],
      );

      const $askAIControls = $e(
        'div',
        { class: isStandalone ? prefix('controls') : prefix('ask-ai-controls') },
        noEvents,
        [$askAIModelWrap, $askAIPromptsWrap, $askAISubmitBtn],
      );

      const $askAIPromptDeleteErrorMsg = $e(
        'div',
        {
          class: prefix(
            isStandalone ? 'prompt-delete-error-msg' : 'ask-ai-prompt-delete-error-msg',
          ),
        },
        noEvents,
        [$t('An error occurred while deleting an prompt, please reload a page and try again')],
      );

      const $askAIResult = $e('div', {
        class: isStandalone ? prefix('result hide') : prefix('ask-ai-result hide'),
      });

      const $askAICopyResultBtnIcon = $e('div', {
        class: isStandalone ? prefix('copy-result-icon') : prefix('ask-ai-copy-result-icon'),
      });

      const $askAICopyResultBtn = $e(
        'div',
        {
          class: isStandalone ? prefix('copy-result-btn') : prefix('ask-ai-copy-result-btn'),
          title: 'Copy result to clipboard',
          'aria-label': 'Copy result to clipboard',
        },
        { click: copyResultToClipboard },
        [$askAICopyResultBtnIcon],
      );

      const $askAISaveResultToNoteBtn = $e(
        'button',
        { class: prefix('btn btn-sm btn-outline-primary') },
        {
          click: () => {
            saveResultToNote('dedupe');
          },
        },
        [$t('Add to note')],
      );

      const $spinner = $e('div', {
        class: isStandalone ? prefix('spinner hide') : prefix('ask-ai-spinner hide'),
        role: 'status',
        title: 'Loading...',
        'aria-label': 'Loading...',
      });
      const $checkmark = $e('div', {
        class: isStandalone
          ? prefix('status status--checkmark hide')
          : prefix('ask-ai-status ask-ai-status--checkmark hide'),
        role: 'status',
        title: 'Saved to note',
        'aria-label': 'Saved to note',
      });
      const $alert = $e('div', {
        class: isStandalone
          ? prefix('status status--alert hide')
          : prefix('ask-ai-status ask-ai-status--alert hide'),
        role: 'status',
        title: 'Error saving note',
        'aria-label': 'Error saving note',
      });

      const $askAIResultControls = $e(
        'div',
        {
          class: isStandalone
            ? prefix('result-controls hide')
            : prefix('ask-ai-result-controls hide'),
        },
        noEvents,
        [$spinner, $checkmark, $alert, $askAICopyResultBtn, $askAISaveResultToNoteBtn],
      );

      const $errorMessage = $e('div', {
        class: isStandalone ? prefix('error-msg hide') : prefix('ask-ai-error-msg hide'),
      });

      const $askAIMain = $e(
        'div',
        { class: isStandalone ? prefix('main') : prefix('ai-ask-main hide') },
        noEvents,
        [
          $askAIInput,
          $askAIControls,
          $askAIPromptDeleteErrorMsg,
          $askAIResult,
          $askAIResultControls,
          $errorMessage,
        ],
      );

      const $askAICta = $e('div', { class: prefix('ask-ai-cta hide') }, noEvents, [
        ...this.createAskAICta(),
      ]);

      const $askAIContainer = $e('div', {
        class: isStandalone ? prefix('container') : prefix('ask-ai-container hide'),
      });
      if (!isStandalone) {
        $askAIContainer.append($askAIBack, $askAICta);
      }
      $askAIContainer.append($askAIMain);

      function showAskAI(userIsPremium) {
        $askAIContainer.classList.remove(prefix('hide'));
        if (userIsPremium !== false) {
          $askAIMain.classList.remove(prefix('hide'));
          focusInput();
          getUserPrompts();
        } else {
          $askAICta.classList.remove(prefix('hide'));
        }
      }

      function hideAskAI() {
        $askAIContainer.classList.add(prefix('hide'));
        if (typeof showMainContent === 'function') {
          showMainContent();
        }
        $askAIMain.classList.add(prefix('hide'));
        $askAICta.classList.add(prefix('hide'));
      }

      function hideAndResetAskAI() {
        hideAskAI();
        $askAIInput.value = '';
        $askAISubmitBtn.removeAttribute('disabled');
        $askAIResult.innerText = '';
        $askAIResult.classList.add(prefix('hide'));
        $askAIResultControls.classList.add(prefix('hide'));
        $spinner.classList.add(prefix('hide'));
        $checkmark.classList.add(prefix('hide'));
        $alert.classList.add(prefix('hide'));
        $errorMessage.classList.add(prefix('hide'));
        $errorMessage.textContent = '';
        $askAICopyResultBtnIcon.classList.remove(
          isStandalone
            ? prefix('copy-result-icon--check')
            : prefix('ask-ai-copy-result-icon--check'),
        );
        $askAICopyResultBtn.setAttribute('title', 'Copy result to clipboard');
        $askAICopyResultBtn.setAttribute('aria-label', 'Copy result to clipboard');
      }

      function focusInput() {
        $askAIInput.focus();
      }

      async function getUserPrompts() {
        $askAIPromptsToggle.setAttribute('disabled', '');
        const prompts = await chrome.runtime.sendMessage({ action: 'getAskAIPrompts' });
        loadPrompts(prompts);
      }

      function loadPrompts(prompts) {
        if (!Array.isArray(prompts) && prompts.length <= 0) {
          return;
        }

        allPrompts = prompts;
        $askAIPromptsList.innerText = '';

        const displayedPrompts = allPrompts.slice(0, displayedPromptsLimit);

        for (let i = 0; i < displayedPrompts.length; i++) {
          const prompt = displayedPrompts[i];

          async function deletePrompt(e) {
            e.stopPropagation();

            $promptListItem.setAttribute('data-histre-is-removed', '');

            allPrompts.splice(i, 1);

            try {
              const isDeleted = await chrome.runtime.sendMessage({
                action: 'deleteAskAIPrompt',
                promptId: prompt.external_id,
                prompts: allPrompts,
              });

              if (!isDeleted) throw new Error('An error occurred');
            } catch (e) {
              $askAIPromptDeleteErrorMsg.style.display = 'block';
            }
          }

          const $promptListItem = $e(
            'div',
            {
              class: prefix(
                `dropdown-list-item ${
                  isStandalone ? 'prompt-list-item' : 'ask-ai-prompt-list-item'
                }`,
              ),
            },
            { click: usePrompt },
            [
              $e('div', { class: prefix('truncate') }, noEvents, [$t(prompt.text)]),
              $e(
                'div',
                {
                  class: isStandalone
                    ? prefix('delete-prompt-btn')
                    : prefix('ask-ai-delete-prompt-btn'),
                },
                { click: deletePrompt },
              ),
            ],
          );

          $askAIPromptsList.appendChild($promptListItem);
        }
        $askAIPromptsToggle.removeAttribute('disabled');
      }

      function usePrompt(e) {
        if (this.hasAttribute('data-histre-is-removed')) {
          e.stopPropagation();
          return;
        }

        $askAIInput.focus();
        $askAIInput.value = this.innerText;
        $askAIInput.setSelectionRange(this.innerText.length, this.innerText.length);
      }

      function displaySpinner() {
        $errorMessage.classList.add(prefix('hide'));
        $checkmark.classList.add(prefix('hide'));
        $alert.classList.add(prefix('hide'));
        $spinner.classList.remove(prefix('hide'));
      }

      function displayCheck() {
        $spinner.classList.add(prefix('hide'));
        $checkmark.classList.remove(prefix('hide'));
      }

      function displayAlert(message) {
        $spinner.classList.add(prefix('hide'));
        $alert.classList.remove(prefix('hide'));
        $errorMessage.textContent = message || 'An error occurred';
        $errorMessage.classList.remove(prefix('hide'));
      }

      function displayAppendAlert() {
        $spinner.classList.add(prefix('hide'));
        $alert.classList.remove(prefix('hide'));
        $errorMessage.textContent = '';
        $errorMessage.append(
          $e('div', {}, noEvents, [$t('Result is already present in a note...')]),
          $e(
            'a',
            {},
            {
              click: () => {
                saveResultToNote('always');
              },
            },
            [$t('Add anyway')],
          ),
        );
        $errorMessage.classList.remove(prefix('hide'));
      }

      async function submitQuestion() {
        focusInput();
        if ($askAISubmitBtn.hasAttribute('disabled')) {
          return;
        }
        const question = $askAIInput.value;

        $askAISubmitBtn.setAttribute('disabled', '');
        $askAIResult.innerText = 'Processing...';
        $askAIResult.classList.remove(prefix('hide'));
        $askAIResultControls.classList.add(prefix('hide'));
        $spinner.classList.add(prefix('hide'));
        $checkmark.classList.add(prefix('hide'));
        $alert.classList.add(prefix('hide'));
        $errorMessage.classList.add(prefix('hide'));
        $errorMessage.textContent = '';
        $askAICopyResultBtnIcon.classList.remove(
          isStandalone
            ? prefix('copy-result-icon--check')
            : prefix('ask-ai-copy-result-icon--check'),
        );
        $askAICopyResultBtn.setAttribute('title', 'Copy result to clipboard');
        $askAICopyResultBtn.setAttribute('aria-label', 'Copy result to clipboard');

        if (question) {
          let pageSource;

          if (!isStandalone) {
            const tab = (await chrome.tabs.query({ active: true, lastFocusedWindow: true }))[0];
            const result = await chrome.scripting.executeScript({
              func: () => {
                return document.documentElement.outerHTML;
              },
              target: { tabId: tab.id },
            });
            if (!Array.isArray(result) || result.length <= 0) {
              //? Something went wrong
              throw result;
            }
            pageSource = result[0].result;
          } else {
            pageSource = document.documentElement.outerHTML;
          }

          const payload = {
            url: location.href,
            page_source: pageSource,
            question,
            gpt_model: $askAIModelWrap.getAttribute('data-histre-dropdown-value'),
          };

          chrome.runtime.sendMessage({ action: 'askAI', payload }, async (result) => {
            $askAISubmitBtn.removeAttribute('disabled');

            if (result.error === false) {
              $askAIResult.innerText = result.data.gpt_output;
              $askAIResultControls.classList.remove(prefix('hide'));

              const newPrompts = await chrome.runtime.sendMessage({ action: 'getAskAIPrompts' });
              loadPrompts(newPrompts);
            } else {
              let errorMessage = 'An error occurred, please try again...';

              if (
                result.errcode === 'unexpected_error' ||
                result.errcode === 'openai_not_integrated' ||
                result.errcode === 'question_too_long' ||
                result.errcode === 'page_source_too_large' ||
                result.errcode === 'openai_authentication_error' ||
                result.errcode === 'temporary_openai_error'
              ) {
                errorMessage = result.errmsg;
              }

              $askAIResult.innerText = '';
              $askAIResult.appendChild(
                $e('div', { class: prefix('ask-ai-error-msg') }, noEvents, [$t(errorMessage)]),
              );

              if (result.errcode === 'openai_not_integrated') {
                $askAIResult.appendChild(
                  $e(
                    'div',
                    {
                      style: 'margin-top: 1em',
                    },
                    noEvents,
                    [
                      $e(
                        'a',
                        {
                          href: `${host}/integrations/openai/`,
                          target: '_blank',
                          rel: 'noopener',
                        },
                        noEvents,
                        [$t('Click here to connect your OpenAI account to histre')],
                      ),
                    ],
                  ),
                );
              }
            }
          });
        } else {
          $askAIResult.innerText = 'Please enter your question...';
          $askAISubmitBtn.removeAttribute('disabled');
        }
      }

      function handleAIInputKeydown(e) {
        if (e.ctrlKey && e.key === 'Enter') {
          e.preventDefault();
          submitQuestion();
          return;
        }

        if (e.key === 'Escape') {
          e.preventDefault();
          if (typeof closeStandaloneUI === 'function') {
            closeStandaloneUI();
          } else {
            hideAskAI();
          }
          return;
        }
      }

      function toggleAIModelDropdown() {
        if (this.getAttribute('aria-expanded') === 'true') {
          return;
        }

        const hideDropdown = () => {
          $askAIModelList.style.display = '';
          this.classList.remove(prefix('active'));
          this.setAttribute('aria-expanded', 'false');
          document.removeEventListener('click', hideDropdown);
        };

        this.classList.add(prefix('active'));
        this.setAttribute('aria-expanded', 'true');
        $askAIModelList.style.display = 'block';
        $askAIModelList.style.bottom = `${0 - $askAIModelList.clientHeight - 4}px`;
        $askAIModelList.style.left = `0`;
        setTimeout(() => {
          document.addEventListener('click', hideDropdown);
        }, 1);
      }

      function togglePromptsDropdown() {
        if (this.getAttribute('aria-expanded') === 'true') {
          return;
        }

        const hideDropdown = () => {
          $askAIPromptsList.style.display = '';
          this.classList.remove(prefix('active'));
          this.setAttribute('aria-expanded', 'false');
          document.removeEventListener('click', hideDropdown);

          if ($askAIPromptsList.querySelector('[data-histre-is-removed]')) {
            loadPrompts(allPrompts);
          }
        };

        this.classList.add(prefix('active'));
        this.setAttribute('aria-expanded', 'true');
        $askAIPromptsList.style.display = 'flex';
        $askAIPromptsList.style.bottom = `${0 - $askAIPromptsList.clientHeight - 4}px`;
        $askAIPromptsList.style.right = `0`;
        setTimeout(() => {
          document.addEventListener('click', hideDropdown);
        }, 1);
      }

      function selectAskAIModel() {
        focusInput();
        const value = this.getAttribute('data-histre-dropdown-item');
        $askAIModelWrap.setAttribute('data-histre-dropdown-value', value);
        $askAIModelText.innerText = this.innerText;
        chrome.storage.local.set({ askAIModel: value });
      }

      function setAskAIModel(value) {
        if (value === 'gpt3') {
          $askAIModelWrap.setAttribute('data-histre-dropdown-value', value);
          $askAIModelText.innerText = 'gpt-3';
        } else if (value === 'gpt3.5') {
          $askAIModelWrap.setAttribute('data-histre-dropdown-value', value);
          $askAIModelText.innerText = 'gpt-3.5';
        } else {
          $askAIModelWrap.setAttribute('data-histre-dropdown-value', 'gpt4');
          $askAIModelText.innerText = 'gpt-4';
        }
      }

      function copyResultToClipboard() {
        focusInput();
        navigator.clipboard.writeText($askAIResult.innerText);
        $askAICopyResultBtnIcon.classList.add(
          isStandalone
            ? prefix('copy-result-icon--check')
            : prefix('ask-ai-copy-result-icon--check'),
        );
        $askAICopyResultBtn.setAttribute('title', 'Result copied to clipboard');
        $askAICopyResultBtn.setAttribute('aria-label', 'Result copied to clipboard');
      }

      async function saveResultToNote(mode = 'true') {
        focusInput();
        displaySpinner();
        $askAISaveResultToNoteBtn.disabled = true;

        const activeTab = (await chrome.tabs.query({ active: true, lastFocusedWindow: true }))[0];

        const noteText = $askAIResult.innerText;
        const noteHTML = `<p>${noteText}</p>`;
        const noteModified =
          document.querySelector('#histre-sidebar-wrap')?.dataset.histreNoteModified ??
          document.querySelector('#histre-ask-ai-wrap')?.dataset.histreNoteModified;

        chrome.runtime.sendMessage(
          {
            action: 'appendToNote',
            data: {
              note: {
                note: noteHTML,
                noteText,
                url: activeTab.url,
                title: activeTab.title,
                note_modified: noteModified,
              },
              tabId: activeTab.id,
              mode,
            },
          },
          (result) => {
            $askAISaveResultToNoteBtn.disabled = false;

            if (result.error !== false) {
              displayAlert('An error occurred while saving response to note');
            } else if (result.data.appended === false) {
              displayAppendAlert();
            } else {
              displayCheck();
            }
          },
        );
      }

      return {
        element: $askAIContainer,
        showAskAI,
        hideAskAI,
        hideAndResetAskAI,
        focusInput,
        setAskAIModel,
        loadPrompts,
      };
    }

    createAskAICta() {
      const isPopup = this.isPopup;
      const prefix = this.prefix;

      const $askAICtaText = $e(
        isPopup ? 'p' : 'div',
        { class: isPopup ? 'mt-2' : prefix('ask-ai-cta-text') },
        noEvents,
        [$t('Upgrade to unlock this AI feature')],
      );
      const $askAICtaLink = $e(
        'a',
        { href: `${host}/pricing/`, target: '_blank', rel: 'noopener' },
        noEvents,
        [$t('Click here to see our plans and pricing')],
      );

      return [$askAICtaText, $askAICtaLink];
    }

    //----- YouTube Timestamp -----//

    createYTTimestamp(tabId, tabUrl, initHasPermission = true) {
      const isPopup = this.isPopup;
      const prefix = this.prefix;
      const refs = this.refs;
      let hasPermission = initHasPermission;

      const $youtubeTimestamp = $e(
        'div',
        {
          id: prefix('yt-timestamp'),
          class: prefix('yt-timestamp'),
        },
        { click: insertYoutubeTimestamp },
        [
          isPopup
            ? $e('i', { class: 'fe fe-youtube' })
            : $e('span', { class: prefix('yt-timeline-icon') }),
          $t('Save current time'),
        ],
      );

      function formatTime(duration) {
        // ~~ is substitution for Math.floor
        const hrs = ~~(duration / 3600);
        const mins = ~~((duration % 3600) / 60);
        const secs = ~~duration % 60;

        // Output like "1:01" or "4:03:59" or "123:03:59"
        let time = '';

        if (hrs > 0) {
          time += '' + hrs + ':' + (mins < 10 ? '0' : '');
        }

        time += '' + mins + ':' + (secs < 10 ? '0' : '');
        time += '' + secs;
        return time;
      }

      async function askPermission() {
        const enabled = chrome.permissions.request({ origins: ['https://www.youtube.com/'] });
        if (enabled) {
          await chrome.scripting.executeScript({
            files: ['youtube-player-control.js'],
            target: { tabId: tabId },
          });

          hasPermission = true;
          return true;
        }

        return false;
      }

      async function jumpToYoutubeTimestamp(e) {
        if (!hasPermission) {
          const granted = await askPermission();

          if (!granted) return;
        }

        if (e.target.getAttribute('href')?.includes('https://www.youtube.com/watch?')) {
          e.preventDefault();
          const url = new URL(e.target.getAttribute('href'));
          const time = url.searchParams.get('t');

          chrome.tabs.sendMessage(tabId, { goToYoutubeTimestamp: time });
        }
      }

      async function insertYoutubeTimestamp() {
        if (!hasPermission) {
          const granted = await askPermission();

          if (!granted) return;
        }

        const time = await chrome.tabs.sendMessage(tabId, { getYoutubeTimestamp: true });
        if (typeof time !== 'number') {
          return;
        }
        const formatedTime = formatTime(time);
        const url = new URL(tabUrl);
        url.searchParams.set('t', time);
        const link = `<p><a href="${url}">${formatedTime}</a></p><p><br></p>`;
        let note = DOMPurify.sanitize(refs.quill.root.innerHTML);
        if (note === '<p><br></p>') {
          note = link;
        } else {
          note += link;
        }
        refs.quill.setText('', 'silent');
        const noteDelta = refs.quill.clipboard.convert(DOMPurify.sanitize(note));
        refs.quill.setContents(noteDelta, 'user');
      }

      return {
        element: $youtubeTimestamp,
        jumpToYoutubeTimestamp,
      };
    }
  }

  window.histreUtils = Utils;
})();
