// do not use console.log or helper.llog here

// Content script with functions/utilities to prepare page content for addressable lookup/insert

// Handlers for each specified website/page

(function () {
  if (window.histre_addressable_has_run) {
    return true;
  }
  window.histre_addressable_has_run = true;

  // Returns entire HTML content of page
  const addressableCraigslist = () => document.documentElement.innerHTML;

  // Extension message event listener

  chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    if (!msg.addressable) {
      return;
    }

    const url = new URL(msg.addressable);
    const hostname = url.hostname;
    if (hostname.endsWith('.craigslist.org')) {
      const content = addressableCraigslist();
      sendResponse({ result: content });
    }
    // Add condition to handle other sites/domains that require urlcanon here

    return true;
  });
})();
