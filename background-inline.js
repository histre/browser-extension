import { host, apiBase, isFirefox } from '/globals.js';
import { helper } from '/helper.js';
import { db } from '/background-globals.js';
import {
  getNote,
  getNotesForAllTabs,
  saveNote,
  createBook,
  removeNoteFromBooks,
  saveVote,
} from '/background-note.js';

const noteUrl = `${apiBase}/note/`;
const eventUrl = `${apiBase}/event/`;

const domainBlacklist = ['mail.google.com', 'calendar.google.com', 'slack.com'];

// Convert some Chrome functions to async/await -- start
function hasPermissions(permissions = {}) {
  return new Promise((resolve) => {
    chrome.permissions.contains(permissions, (enabled) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(enabled);
    });
  });
}

function executeScript(tabId, details) {
  return new Promise((resolve) => {
    if (typeof tabId === 'number') {
      const injection = {
        ...details,
        target: { tabId },
      };

      chrome.scripting.executeScript(injection, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    } else {
      chrome.scripting.executeScript(tabId, (result) => {
        if (chrome.runtime.lastError) {
          // TODO reject error
        }
        resolve(result);
      });
    }
  });
}

function sendMessage(tabId, message) {
  return new Promise((resolve) => {
    chrome.tabs.sendMessage(tabId, message, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}

function getLocalStorage(keys = null) {
  return new Promise((resolve) => {
    chrome.storage.local.get(keys, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}

function setLocalStorage(keys = null) {
  return new Promise((resolve) => {
    chrome.storage.local.set(keys, (result) => {
      if (chrome.runtime.lastError) {
        // TODO reject error
      }
      resolve(result);
    });
  });
}
// Convert some Chrome functions to async/await -- end

async function isInlineEnabled() {
  return (await chrome.storage.local.get(['featuresInline'])).featuresInline;
}

export async function addInlineContentScript(tab) {
  if (!(await isInlineEnabled())) {
    return;
  }

  // ignore chrome:// about:config etc
  if (!tab || !tab.url || !(tab.url.indexOf('http') === 0)) {
    return;
  }

  const tabUrl = new URL(tab.url);
  const tabHostname = tabUrl.hostname;

  for (const domain of domainBlacklist) {
    if (tabHostname.includes(domain)) {
      return;
    }
  }

  const enabled = await hasPermissions({ origins: ['*://*/*'] });

  if (enabled) {
    async function loadInline() {
      const result = await executeScript(tab.id, { files: ['inline-loader.js'] });
      if (!Array.isArray(result) || result.length <= 0) {
        //? Something went wrong
        throw result;
      }
      const isInlineLoaded = result[0].result;
      if (isInlineLoaded === false) {
        await executeScript(tab.id, { files: ['dom-helper.js'] });
        await executeScript(tab.id, { files: ['utils.js'] });
        await executeScript(tab.id, { files: ['inline.js'] });
      }
    }

    async function loadCmd() {
      const result = await executeScript(tab.id, { files: ['cmd-palette-loader.js'] });
      if (!Array.isArray(result) || result.length <= 0) {
        //? Something went wrong
        throw result;
      }
      const isCmdLoaded = result[0].result;
      if (isCmdLoaded === false) {
        await executeScript(tab.id, { files: ['dom-helper.js'] });
        await executeScript(tab.id, { files: ['utils.js'] });
        await executeScript(tab.id, { files: ['cmd-palette.js'] });
      }
    }

    try {
      await loadInline();
      await loadCmd();

      helper.unsetIconWarning();
    } catch (e) {
      helper.llog('Error loading inline/sidebar', tab, e);
    }
  }
}

async function removeInlineContent(tab) {
  if (await isInlineEnabled()) {
    return;
  }

  // ignore chrome:// about:config etc
  if (!tab || !tab.url || !(tab.url.indexOf('http') === 0)) {
    return;
  }

  sendMessage(tab.id, { action: 'disableInline' });
  sendMessage(tab.id, { action: 'disableCmd' });
}

function onMessageHandler(message, sender, sendResponse) {
  // Inline
  if (message.dest === 'inline') {
    if (message.action === 'saveInlineVote') {
      const data = {
        url: message.inline.url,
        title: message.inline.title,
        vote: message.inline.vote,
      };

      (async () => {
        try {
          const result = await saveVote(data);
          helper.llog('vote saved:', result);

          if (result && result.data && result.data.redir_oauth) {
            chrome.tabs.create({ url: `${host}${result.data.redir_oauth}` });
          }
          if (result && result.errmsg === helper.errmsgs.urlTooLong) {
            result['usermsg'] = `Cannot save vote: URL over ${helper.MAX_URL_LEN} chars.`;
          }

          if (result.error !== false) {
            throw result;
          }

          if (message.from === 'sidebar') {
            chrome.runtime.sendMessage({
              action: 'displayCheck',
              dest: 'sidePanel',
              url: message.inline.url,
            });
          } else {
            chrome.runtime.sendMessage({
              action: 'updateVoteSidebar',
              dest: 'sidePanel',
              vote: data.vote,
              url: data.url,
            });
          }

          sendResponse(result);
        } catch (e) {
          if (message.from === 'sidebar') {
            chrome.runtime.sendMessage({
              action: 'displayError',
              dest: 'sidePanel',
              message: e.message ? e.message : 'An error occurred',
              url: data.url,
            });
          }
          if (e.error) {
            sendResponse(e);
          } else {
            sendResponse({ error: true, errmsg: e });
          }
        }
      })();
      // Ref: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/onMessage
      // See: Sending an asynchronous response using sendResponse
      // "Note return true; in the listener: this tells the browser that you intend to use the sendResponse argument after the listener has returned."
      return true;
    }

    if (message.action === 'openOptionsPage') {
      chrome.tabs.create({ url: chrome.runtime.getURL('options.html') + '#votes' });
      return;
    }

    if (message.action === 'setUserInfo') {
      helper.doGet(`${apiBase}/settings/`, {}, (response) => {
        if (response.data?.username && response.data?.external_id) {
          chrome.storage.local.set(
            {
              userUsername: response.data.username,
              userExternalId: response.data.external_id,
            },
            () => {
              sendResponse({ isSet: true });
            },
          );
        } else {
          sendResponse({ isSet: false });
        }
      });
      return true;
    }

    if (message.action === 'setNoteIsPresent') {
      chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
        tabs.forEach((tab) => {
          chrome.tabs.sendMessage(tab.id, { action: 'setNoteIsPresent', url: tab.url });
        });
      });
      return;
    }

    if (message.action === 'getNoteIdAndSetNoteIsPresent') {
      helper.doGet(`${noteUrl}?url=${encodeURIComponent(sender.tab.url)}`, {}, (result) => {
        const { data, status } = result;

        if (status === helper.HttpCodes.success) {
          if (data.item_id) {
            chrome.tabs.sendMessage(sender.tab.id, {
              action: 'setNoteIsPresent',
              noteId: data.item_id,
              url: sender.tab.url,
            });
          }
        }
      });
      return;
    }

    if (message.action === 'savePrivately') {
      (async () => {
        try {
          const result = await saveNote({
            note: {
              url: encodeURIComponent(sender.tab.url),
              access: 'private',
            },
          });

          if (result.error !== false) {
            throw error;
          }

          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'showSavePrivatelySavedPopup',
            url: sender.tab.url,
          });
          helper.sendFeatureLog('minibar-save-privately');
        } catch (e) {
          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'removeSavePrivatelySaved',
            url: sender.tab.url,
          });
          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'showErrorPopup',
            url: sender.tab.url,
          });
        }
      })();
      return;
    }

    if (message.action === 'publishNote') {
      (async () => {
        try {
          const result = await saveNote({
            note: {
              url: encodeURIComponent(sender.tab.url),
              access: 'public',
            },
          });

          if (result.error !== false) {
            throw error;
          }

          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'showPublishNotePublishedPopup',
            url: sender.tab.url,
          });
          helper.sendFeatureLog('minibar-publish');
        } catch (e) {
          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'removePublishNotePublished',
            url: sender.tab.url,
          });
          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'showErrorPopup',
            url: sender.tab.url,
          });
        }
      })();
      return;
    }

    if (message.action === 'saveNoteToReadLater') {
      (async () => {
        try {
          chrome.runtime.sendMessage({
            action: 'setSidebarReadLater',
            dest: 'sidePanel',
            url: sender.tab.url,
          });
          const result = await saveNote({
            note: {
              url: encodeURIComponent(sender.tab.url),
              read_later: true,
            },
          });

          if (result.error !== false) {
            throw result;
          }
          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'setInlineReadLater',
            bookId: result.data.read_later_book_id,
            noteId: result.data.item_id,
            url: sender.tab.url,
          });
          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'showReadLaterSavedPopup',
            bookId: result.data.read_later_book_id,
            url: sender.tab.url,
          });
          chrome.runtime.sendMessage({
            action: 'setSidebarReadLater',
            dest: 'sidePanel',
            bookId: result.data.read_later_book_id,
            url: sender.tab.url,
          });

          chrome.storage.local.get('featuresInline', async (storageResult) => {
            chrome.tabs.query({ url: helper.removeURLFragment(sender.tab.url, false) }, (tabs) => {
              tabs.forEach((tab) => {
                helper.setNoteIsPresentIcon(tab.id, tab.url);

                if (storageResult.featuresInline) {
                  sendMessage(tab.id, {
                    action: 'setNoteIsPresent',
                    noteId: result.data.item_id,
                  });
                }
              });
            });
          });
        } catch (e) {
          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'removeInlineReadLater',
            url: sender.tab.url,
          });
          chrome.runtime.sendMessage({
            action: 'removeReadLaterActive',
            dest: 'sidePanel',
            url: sender.tab.url,
          });
          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'showErrorPopup',
            url: sender.tab.url,
          });
        }
      })();
      return;
    }

    if (message.action === 'removeNoteFromReadLater') {
      (async () => {
        try {
          chrome.runtime.sendMessage({
            action: 'removeReadLaterActive',
            dest: 'sidePanel',
            url: sender.tab.url,
          });

          const result = await removeNoteFromBooks({
            data: {
              book_ids: [message.bookId],
              url_item_item_id: message.noteId,
            },
            url: sender.tab.url,
            forReadLater: true,
          });

          if (result.error !== false) {
            throw error;
          }

          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'showReadLaterRemovedPopup',
            url: sender.tab.url,
          });
        } catch (e) {
          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'setInlineReadLater',
            url: sender.tab.url,
          });
          chrome.tabs.sendMessage(sender.tab.id, {
            action: 'showErrorPopup',
            url: sender.tab.url,
          });
          chrome.runtime.sendMessage({
            action: 'setSidebarReadLater',
            dest: 'sidePanel',
            url: sender.tab.url,
          });
        }
      })();
      return;
    }
  }

  // Sidebar
  if (message.dest === 'sidebar') {
    if (message.action === 'openSidePanel') {
      if (isFirefox) {
        chrome.tabs.sendMessage(sender.tab.id, { action: 'showSidebarFirefoxCTA' });
      } else {
        openSidebar(sender.tab.windowId);
      }
      return;
    }

    if (message.action === 'getSidebarNote') {
      (async () => {
        const tab = (await chrome.tabs.query({ active: true, lastFocusedWindow: true }))[0];

        const { userCacheNotes } = await chrome.storage.local.get('userCacheNotes');
        let note;

        if (userCacheNotes !== false) {
          try {
            note = await (await db).get('note', helper.removeURLFragment(tab.url, false));
          } catch (e) {
            // IndexedDB is disabled
          }
        }

        // Skip caching for now if url is content addressable
        if (note && !message.isAddressableUrl && userCacheNotes !== false) {
          chrome.storage.local.get(
            ['userLogHistory', 'userAccessLevels', 'userAccessDefault', 'userIsPremium'],
            (result) => {
              const data = {
                data: note,
                error: false,
                dataExtraction: note.dataExtraction,
                settings: {
                  history: result.userLogHistory,
                  access_default: result.userAccessDefault,
                  access_levels: JSON.parse(result.userAccessLevels),
                  premium_user: result.userIsPremium,
                },
              };

              chrome.runtime.sendMessage({
                action: 'setupNote',
                dest: 'sidePanel',
                result: data,
                tab,
                url: tab.url,
              });
            },
          );
        } else {
          try {
            await getNote(tab);
          } catch (e) {
            chrome.runtime.sendMessage({
              action: 'displaySetupError',
              dest: 'sidePanel',
              message: 'An error occurred',
              url: tab.url,
            });
          }
        }
      })();
      return;
    }

    if (message.action === 'saveSidebarNote' && message.data) {
      // Ref: https://stackoverflow.com/questions/20435528/chrome-extension-sendresponse-does-not-work
      // onMessageHandler function can't be "natively" async because sendResponse won't work,
      // as it depends for return value to be boolean, and async function returns a Promise.
      (async () => {
        const tab = (await chrome.tabs.query({ active: true, lastFocusedWindow: true }))[0];

        try {
          let newBookId;
          if (message.createBook) {
            const result = await createBook(message.createBook, tab.url);
            if (result.error === false) {
              newBookId = result.data.book_id;
              if (Array.isArray(message.data.book_ids)) {
                message.data.book_ids.push(newBookId);
              } else {
                message.data.book_ids = [newBookId];
              }
              chrome.runtime.sendMessage({
                action: 'enableNewBook',
                dest: 'sidePanel',
                newBookId,
                url: tab.url,
              });
            } else {
              if (!helper.isAuthErrorStatus(result.status)) {
                chrome.runtime.sendMessage({
                  action: 'enableNewBookActions',
                  dest: 'sidePanel',
                  url: tab.url,
                });
              }
              throw new Error(result.errorMessage ?? 'Error creating collection');
            }
          }

          const result = await saveNote({
            note: message.data,
            removeNoteFromBooks: message.removeNoteFromBooks,
            tabId: tab.id,
            newBook: {
              id: newBookId,
              title: message.createBook?.title,
            },
          });

          if (result.error === false) {
            if (result.newNote) {
              // Break here, note update is done in background-note.js
              return;
            } else {
              chrome.runtime.sendMessage({
                action: 'noteSaved',
                dest: 'sidePanel',
                noteId: result.data.item_id,
                note: result.note,
                noteModified: result.data.note_modified,
                readLaterBook: result.data.read_later_book_id,
                url: tab.url,
              });
            }
          } else {
            throw new Error(result.errorMessage ?? 'An error occurred while saving note');
          }

          if (
            message.removeNoteFromBooks?.length > 0 &&
            (message.noteId || result?.data?.item_id)
          ) {
            const response = await removeNoteFromBooks({
              data: {
                book_ids: message.removeNoteFromBooks,
                url_item_item_id: message.noteId || result?.data?.item_id,
              },
              url: tab.url,
              tabId: tab.id,
            });
            if (response?.error !== false || !response?.ok) {
              chrome.runtime.sendMessage({
                action: 'reCheckBooks',
                dest: 'sidePanel',
                bookIds: message.removeNoteFromBooks,
                url: tab.url,
              });
              throw new Error(response.errorMessage ?? 'Unable to remove note from collection');
            }
          }

          chrome.tabs.query({ url: helper.removeURLFragment(tab.url, false) }, (tabs) => {
            tabs.forEach((queriedTab) => {
              sendMessage(queriedTab.id, {
                action: 'setNoteIsPresent',
                noteId: result.data.item_id,
                url: queriedTab.url,
              });
              helper.setNoteIsPresentIcon(queriedTab.id, queriedTab.url);
            });
          });

          chrome.runtime.sendMessage({
            action: 'displayCheck',
            dest: 'sidePanel',
            url: tab.url,
          });

          sendResponse(result);
        } catch (e) {
          chrome.runtime.sendMessage({
            action: 'displayError',
            dest: 'sidePanel',
            message: e.message ? e.message : 'An error occurred',
            url: tab.url,
          });
          if (message.data.read_later) {
            chrome.runtime.sendMessage({
              action: 'removeReadLaterActive',
              dest: 'sidePanel',
              url: tab.url,
            });
          }

          sendResponse({ error: true, errmsg: e.message ? e.message : 'An error occurred' });
        }
      })();
      return true;
    }

    if (message.action === 'reExtractData') {
      (async () => {
        const dataExtraction = await sendMessage(sender.tab.id, { extractable: sender.tab.url });
        chrome.runtime.sendMessage({
          action: 'renderExtractedData',
          dest: 'sidePanel',
          dataExtraction: dataExtraction?.result,
          url: sender.tab.url,
        });
      })();
      return;
    }

    if (message.action === 'handleAuthError') {
      getNote(sender.tab);
      return;
    }

    if (message.action === 'sendEvent') {
      helper.doPost(eventUrl, message.data, (result) => {
        if (result.status !== 200) {
          helper.llog('Error posting an sidebar event. Info:', result);
        }
      });
      return;
    }
  }

  // Command palette
  if (message.dest === 'cmd') {
    if (message.action === 'openSidebar') {
      sendMessage(sender.tab.id, { action: 'openSidebar' });
      return;
    }

    if (message.action === 'openInline') {
      sendMessage(sender.tab.id, { action: 'openInline' });
      return;
    }

    if (message.action === 'openOptionsPage') {
      chrome.tabs.create({ url: chrome.runtime.getURL('options.html') + '#cmd-palette' });
      return;
    }

    if (message.action === 'showAskAI') {
      sendMessage(sender.tab.id, { action: 'showAskAI' });
      return;
    }

    if (message.action === 'sendCMDFeatureLog') {
      helper.sendFeatureLog(message.event);
      return;
    }
  }
}

function focusChangedHandler(windowId) {
  if (windowId < 0) {
    return;
  }

  chrome.runtime.sendMessage({ action: 'getUpdatedSidebarNote', dest: 'sidePanel' });
}

function onStorageChangedHandler(changes) {
  if (changes.featuresInline) {
    if (changes.featuresInline.newValue === true) {
      enableInline();
    } else {
      disableInline();
    }
  }

  if (changes.inlinePosTop && changes.inlinePosLeft) {
    updateInlinePosition(changes.inlinePosTop.newValue, changes.inlinePosLeft.newValue);
  } else if (changes.inlinePosTop || changes.inlinePosLeft) {
    chrome.storage.local.get(['inlinePosTop', 'inlinePosLeft'], (result) => {
      updateInlinePosition(result.inlinePosTop, result.inlinePosLeft);
    });
  }

  if (changes.inlineIsShown) {
    chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
      tabs.forEach((tab) => {
        chrome.tabs.sendMessage(tab.id, {
          action: changes.inlineIsShown.newValue ? 'showInline' : 'hideInline',
          url: tab.url,
        });
      });
    });
  }
}

function refreshFromStorage() {
  chrome.storage.local.get(['featuresInline', 'inlinePosTop', 'inlinePosLeft'], (result) => {
    if (typeof result.inlinePosTop !== 'undefined' && typeof result.inlinePosLeft !== 'undefined') {
      updateInlinePosition(result.inlinePosTop, result.inlinePosLeft);
    }
  });
}

function enableInline() {
  getNotesForAllTabs();
}

function disableInline() {
  chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
    tabs.forEach((tab) => {
      removeInlineContent(tab);
    });
  });
}

function updateInlinePosition(topInlineOffset, leftInlineOffset) {
  if (!topInlineOffset || !leftInlineOffset) {
    return;
  }

  chrome.tabs.query({ url: ['http://*/*', 'https://*/*'] }, (tabs) => {
    tabs.forEach((tab) => {
      sendMessage(tab.id, { action: 'positionInline' });
    });
  });
}

function handleAuthError() {
  helper.setIconWarning();
  helper.handleAuthErrorsOnce();
}

async function openSidebar(windowId) {
  if (typeof browser === 'object' && typeof browser.sidebarAction === 'object') {
    try {
      await browser.sidebarAction.open();
      helper.sendFeatureLog('open-sidebar');
    } catch {
      chrome.tabs.create({ url: chrome.runtime.getURL('side-panel-opening-error.html') });
    }
    return;
  }

  try {
    chrome.sidePanel.setOptions({ path: 'side-panel.html' });
    await chrome.sidePanel.open({ windowId: windowId });
    helper.sendFeatureLog('open-sidebar');
  } catch {
    chrome.tabs.create({ url: chrome.runtime.getURL('side-panel-opening-error.html') });
  }
}

function addContextMenu() {
  chrome.contextMenus.create({
    id: 'open-sidebar',
    title: 'Open Sidebar',
    contexts: ['all'],
  });
}

chrome.contextMenus.onClicked.addListener((info, tab) => {
  if (info.menuItemId === 'open-sidebar') {
    openSidebar(tab.windowId);
  }
});

chrome.runtime.onMessage.addListener(onMessageHandler);
chrome.storage.onChanged.addListener(onStorageChangedHandler);
chrome.windows.onFocusChanged.addListener(focusChangedHandler);
chrome.runtime.onStartup.addListener(() => {
  refreshFromStorage();
});
chrome.runtime.onInstalled.addListener(() => {
  refreshFromStorage();
  addContextMenu();
});
