(() => {
  if (window.histreLoadedCmd) {
    return true;
  }
  window.histreLoadedCmd = true;

  // For Firefox
  if (typeof browser === 'object') {
    chrome = browser;
  }

  return false;
})();
