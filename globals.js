// ENV : local | stg | prod
const ENV = 'local';

export const isDevMode = ENV !== 'prod';

export let host = 'https://histre.com';
if (ENV === 'local') {
  host = 'http://local.histre.com:8080';
} else if (ENV === 'stg') {
  host = 'https://cfgromit.histre.com';
}

export const apiBase = `${host}/api/v1`;

export const isFirefox = typeof browser === 'object';
if (isFirefox) {
  chrome = browser;
}
